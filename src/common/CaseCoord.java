/**
 * 
 */
package common;

import java.io.Serializable;

import exceptions.TourTropGrandeException;

public class CaseCoord extends Case implements Serializable {

    private static final long serialVersionUID = -6770713338571545890L;

    private int               y;

    private int               x;

    /**
     * @param b
     */
    public CaseCoord(byte b, int x, int y) {
        super(b);
        this.x = x;
        this.y = y;

    }

    /**
     * @param case1
     * @throws TourTropGrandeException
     */
    public CaseCoord(Case c, int x, int y) throws TourTropGrandeException {
        super(c);
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CaseCoord other = (CaseCoord) obj;
        if (x != other.x) {
            return false;
        }
        if (y != other.y) {
            return false;
        }
        return true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
