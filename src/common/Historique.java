package common;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.ListIterator;

public class Historique implements Serializable {

    private static final long            serialVersionUID = 678462794810819567L;

    private transient ListIterator<Coup> it;
    private LinkedList<Coup>             listCoupJoues;

    /* Constructeurs */

    /**
     * Constructeur par d�faut
     */
    public Historique() {
        this(null);
    }

    public Historique(LinkedList<Coup> listCoupJoues) {
        if (listCoupJoues == null) {
            this.listCoupJoues = new LinkedList<>();
        } else {
            this.listCoupJoues = listCoupJoues;
        }

        it = this.listCoupJoues.listIterator();
    }

    /* Work code */

    /**
     * Ajoute un nouveau coup dans l'historique. Supprime les coups qui ne sont plus jouable de l'historique.
     * 
     * @param c
     *        coordonn�es et direction du coup qui a �t� jou�.
     */
    public void ajouterNouveauCoup(Coup c) {
        // TODO: Mettre � jour l'ui si n�cessaire (griser bouton de l'ui, si n�cessaire)
        if (c == null) {
            throw new IllegalArgumentException("Impossible d'ajouter un coup nul");
        }
        if (c.getSource() == null) {
            throw new IllegalArgumentException("Impossible d'ajouter un coup � source nulle");
        }
        if (c.getDestCoord() == null) {
            throw new IllegalArgumentException("Impossible d'ajouter un coup � destination nulle");
        }
        if (c.getSourceCoord().getX() < 0 || c.getSourceCoord().getY() < 0) {
            throw new IllegalArgumentException("Impossible d'ajouter un coup avec des coordonn�es de source n�gatives");
        }
        if (c.getDestCoord().getX() < 0 || c.getDestCoord().getY() < 0) {
            throw new IllegalArgumentException(
                                               "Impossible d'ajouter un coup avec des coordonn�es de destination n�gatives");
        }

        while (it.hasNext()) {
            it.next();
            it.remove();
        }
        it.add(c);
    }

    public void annulerCoup() throws Exception {
        if (it.hasPrevious()) {
            it.previous();
        } else {
            throw new Exception("impossible d'annuler le coup");
        }
    }

    public Coup getDernierCoup() {
        return listCoupJoues.get(it.previousIndex());
    }
    
    public Coup getProchainCoup() {
        return listCoupJoues.get(it.nextIndex());
    }
    
    
    /* Getters et Setters */

    public ListIterator<Coup> getIt() {
        return it;
    }

    public LinkedList<Coup> getListCoupJoues() {
        return listCoupJoues;
    }

    public void refaireCoup() throws Exception {
        if (it.hasNext()) {
            it.next();
        } else {
            throw new Exception("impossible de refaire le coup");
        }
    }

    public void setIt(ListIterator<Coup> it) {
        this.it = it;
    }

    public void setListCoupJoues(LinkedList<Coup> listCoupJoues) {
        this.listCoupJoues = listCoupJoues;
    }

    public boolean annulerPossible() {
        return it.nextIndex() != 0;
    }

    public boolean refairePossible() {
        return it.nextIndex() != getListCoupJoues().size();
    }
}
