package common;

import java.io.Serializable;

import exceptions.MauvaisChoixDeCouleurException;

public class Joueur implements Serializable {

    private static final long   serialVersionUID = -5411396231167349312L;
    private TypeJoueur          typeJoueur;
    private String              nom;
    private final CouleurJoueur couleur;
    private final int           nbTours;

    public Joueur(TypeJoueur typeJoueur, String nom, CouleurJoueur couleur) throws MauvaisChoixDeCouleurException {
        super();
        this.typeJoueur = typeJoueur;
        this.nom = nom;
        if (couleur.equals(CouleurJoueur.TOUS)) {
            throw new MauvaisChoixDeCouleurException("Le joueur doit avoir une couleur blanche ou noire");
        }
        this.couleur = couleur;
        nbTours = 24;
    }

    public CouleurJoueur getCouleur() {
        return couleur;
    }

    public int getNbTours() {
        return nbTours;
    }

    public String getNom() {
        return nom;
    }

    public TypeJoueur getTypeJoueur() {
        return typeJoueur;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setTypeJoueur(TypeJoueur typeJoueur) {
        this.typeJoueur = typeJoueur;
    }
}
