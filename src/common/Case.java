package common;

import java.io.Serializable;

import exceptions.TourTropGrandeException;

public class Case implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8226715167649951819L;

    private byte              valeurCase;

    final byte                MASQUE_COULEUR   = 0b1000;
    final byte                MASQUE_HAUTEUR   = 0b0111;
    final byte                MASQUE_ENTIER    = 0b1111;
    public static final byte  couleurB         = 0;

    public static final byte  couleurN         = 1;
    public static final byte  L                = 0;                       // libre

    public static final byte  C                = 8;                       // centre
    public static final byte  U                = -1;                      // undefined -> non défini
    public static final byte  B                = (Case.couleurB << 3) + 1; // Blanc de hauteur 1
    public static final byte  N                = (Case.couleurN << 3) + 1; // Noir de hauteur 1
    public final static Case  LIBRE            = new Case(Case.L);

    public final static Case  NON_DEFINI       = new Case(Case.U);
    public final static Case  CENTRE           = new Case(Case.C);

    public static boolean estSelectionnable(byte caseAJouer, byte[] casesVoisines) {
        final Case cj = new Case(caseAJouer);
        for (final byte casesVoisine : casesVoisines) {
            final Case cv = new Case(casesVoisine);
            if (cv.getHauteur() + cj.getHauteur() <= 5) {
                return true;
            }
        }
        return false;
    }

    public Case(byte b) {
        valeurCase = b;
    }

    public Case(Case case1) throws TourTropGrandeException {
        assignerValeur(case1.getValeurCase());
    }

    /**
     * Assigne une nouvelle valeur (hauteur et couleur) à la case
     * 
     * @param b
     *        valeur à assigner
     * @throws TourTropGrandeException
     */
    public void assignerValeur(byte b) throws TourTropGrandeException {
        final byte nouvelleHauteur = (byte) (b & MASQUE_HAUTEUR);

        if (nouvelleHauteur > 5) {
            throw new TourTropGrandeException();
        }

        valeurCase = (byte) (b & MASQUE_ENTIER); // Le masque sert au cas ou si les bits non utilisés sont assigné
    }

    public void augmenterHauteur(int nbPions) throws TourTropGrandeException {
        final byte couleur = getCouleur();
        final int nouvelleHauteur = getHauteur() + nbPions;

        /* La hauteur ne doit pas dépasser 5 */
        if (nouvelleHauteur > 5) {
            throw new TourTropGrandeException();
        }

        valeurCase = (byte) (couleur << 3 | (byte) nouvelleHauteur);
    }

    public void changerCouleur() {
        valeurCase = (byte) (valeurCase + 0b1000 & MASQUE_ENTIER);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Case other = (Case) obj;
        if (valeurCase != other.valeurCase) {
            return false;
        }
        return true;
    }

    public boolean estUneTour() {
        return !equals(Case.CENTRE) && !equals(Case.LIBRE) && !equals(Case.NON_DEFINI);
    }

    /**
     * getCouleur() Renvoie juste la couleur de la case
     * 
     * @return la couleur de la case 1 ou 0
     */
    public byte getCouleur() {
        return (byte) ((valeurCase & MASQUE_COULEUR) >> 3);
    }

    public byte getHauteur() {
        return (byte) (valeurCase & MASQUE_HAUTEUR);
    }

    public byte getValeurCase() {
        return valeurCase;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + valeurCase;
        return result;
    }

    @Override
    public String toString() {
        return "Case [valeurCase=" + valeurCase + ", getHauteur()=" + getHauteur() + ", getCouleur()=" + getCouleur()
            + "]";
    }

    public void viderCase() {
        valeurCase = Case.L;
    }
}
