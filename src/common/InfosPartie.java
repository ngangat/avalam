package common;

import java.io.Serializable;
import java.util.Observable;

import client.ia.NiveauDifficile;
import client.ia.NiveauFacile;
import client.ia.NiveauMoyen;
import client.ia.NiveauRapide;
import exceptions.MauvaisChoixDeCouleurException;

public class InfosPartie extends Observable implements Serializable {

    private static final long serialVersionUID = -7015261253395749211L;

    private CouleurJoueur     couleurJoueurCourant;
    private Joueur[]          joueurs;
    private TypePartie        typePartie;
    private CouleurJoueur     couleurJoueurClient;
    private Historique        historique;
    private Terrain           terrain;

    public InfosPartie() {
    }

    public InfosPartie(Joueur j1, Joueur j2, CouleurJoueur joueurCourant, TypePartie typePartie)
                                                                                                throws MauvaisChoixDeCouleurException {
        if (j1 != null && j2 != null && j1.getCouleur().equals(j2.getCouleur())) {
            throw new MauvaisChoixDeCouleurException("Les joueurs ne peuvent avoir la même couleur.");
        }
        if (joueurCourant.equals(CouleurJoueur.TOUS)) {
            throw new MauvaisChoixDeCouleurException("La couleur du joueur courant doit être BLANC ou NOIR.");
        }

        historique = new Historique();
        joueurs = new Joueur[2];
        if (j1 != null) {
            joueurs[j1.getCouleur().getValeur()] = j1;
        }
        if (j2 != null) {
            joueurs[j2.getCouleur().getValeur()] = j2;
        }

        couleurJoueurCourant = joueurCourant;

        this.typePartie = typePartie;
    }
    

    public void ajouterNouveauJoueur(Joueur j) {
        if (j == null) {
            throw new IllegalArgumentException("le joueur ne peut être null");
        }
        joueurs[j.getCouleur().getValeur()] = j;
    }

    public synchronized void changerJoueurCourant() {
        System.out.println("Joueur ayant joué : " + couleurJoueurCourant);
        couleurJoueurCourant = couleurJoueurCourant.equals(CouleurJoueur.BLANC) ? CouleurJoueur.NOIR
            : CouleurJoueur.BLANC;
        setChanged();
        notifyObservers();
        System.out.println("Joueur courant : " + couleurJoueurCourant);
    }

    public void detruireJoueur() {
        if (joueurs != null) {
            for (Joueur j : joueurs) {
                if (j != null) {
                    if (j.getTypeJoueur().equals(TypeJoueur.Humain)) {
                        j = null;
                    } else {
                        if (j instanceof NiveauDifficile) {
                            NiveauDifficile jb = (NiveauDifficile) j;
                            deleteObserver(jb);
                            jb.seSuicider();
                            jb = null;
                        } else if (j instanceof NiveauRapide) {
                            NiveauRapide jr = (NiveauRapide) j;
                            deleteObserver(jr);
                            jr.seSuicider();
                            jr = null;
                        } else if (j instanceof NiveauFacile) {
                        	NiveauFacile jf = (NiveauFacile) j;
                        	deleteObserver(jf);
                        	jf.seSuicider();
                        	jf = null;
                        } else if (j instanceof NiveauMoyen) {
                        	NiveauMoyen jm = (NiveauMoyen) j;
                        	deleteObserver(jm);
                        	jm.seSuicider();
                        	jm = null;
                        }
                    }
                }
            }
        }
    }

    public synchronized CouleurJoueur getCouleurJoueurClient() {
        return couleurJoueurClient;
    }

    public synchronized CouleurJoueur getCouleurJoueurCourant() {
        return couleurJoueurCourant;
    }

    public synchronized Historique getHistorique() {
        return historique;
    }

    public synchronized Joueur getJoueurCourant() {
        return joueurs[couleurJoueurCourant.getValeur()];
    }

    public Joueur[] getJoueurs() {
        return joueurs;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public TypePartie getTypePartie() {
        return typePartie;
    }

    public synchronized void setCouleurJoueurClient(CouleurJoueur couleurJoueurClient) {
        this.couleurJoueurClient = couleurJoueurClient;
    }

    public synchronized void setCouleurJoueurCourant(CouleurJoueur couleurJoueurCourant, boolean notify) {
        this.couleurJoueurCourant = couleurJoueurCourant;
        if (notify) {
            setChanged();
            notifyObservers();
        }
    }

    public synchronized void setHistorique(Historique historique) {
        this.historique = historique;
    }

    public void setJoueurs(Joueur[] joueurs) {
        this.joueurs = joueurs;
    }

    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    public void setTypePartie(TypePartie typePartie) {
        this.typePartie = typePartie;
    }
}
