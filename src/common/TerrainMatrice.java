package common;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;

import common.Coup.TYPE_MVT;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public class TerrainMatrice extends Observable implements Terrain, Serializable {
    private static final long serialVersionUID = 1248335907813304056L;
    private byte[][]          cases;

    private final byte[][]    TERRAIN_INITIAL  = {
        { Case.U, Case.U, Case.U, Case.U, Case.B, Case.N, Case.U, Case.U, Case.U },
        { Case.U, Case.B, Case.N, Case.B, Case.N, Case.B, Case.U, Case.U, Case.U },
        { Case.B, Case.N, Case.B, Case.N, Case.B, Case.N, Case.B, Case.U, Case.U },
        { Case.N, Case.B, Case.N, Case.B, Case.N, Case.B, Case.N, Case.U, Case.U },
        { Case.U, Case.N, Case.B, Case.N, Case.C, Case.N, Case.B, Case.N, Case.U },
        { Case.U, Case.U, Case.N, Case.B, Case.N, Case.B, Case.N, Case.B, Case.N },
        { Case.U, Case.U, Case.B, Case.N, Case.B, Case.N, Case.B, Case.N, Case.B },
        { Case.U, Case.U, Case.U, Case.B, Case.N, Case.B, Case.N, Case.B, Case.U },
        { Case.U, Case.U, Case.U, Case.N, Case.B, Case.U, Case.U, Case.U, Case.U } };

    public TerrainMatrice() {
        reinitialiserTerrain();
    }

    public TerrainMatrice(Terrain t) {
        cases = new byte[9][9];
        cases = TERRAIN_INITIAL;
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                try {
                    cases[y][x] = t.getCase(x, y).getValeurCase();
                } catch (final HorsDuTerrainException e) { // PAS CENSÃ‰ ARRIVÃ‰
                    // e.printStackTrace();
                }
            }
        }

        setChanged();
        notifyObservers();
    }

    @Override
    public void annulerCoup(Coup coup) {

        final Point src = coup.getSourceCoord();
        final Point dest = coup.getDestCoord();
        // try {
        // Coup temp = new Coup(coup, this);
        // } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
        // | TourTropGrandeException | HorsDuTerrainException e) { // ces erreurs ne doivent pas Ãªtre catchÃ© =>
        // // utilisation avec l'ia
        // }

        try {
            setCase(src.x, src.y, coup.getPrecedenteSource());
            setCase(dest.x, dest.y, coup.getPrescedenteDest());
        } catch (final HorsDuTerrainException e) {
        }

        // System.out.println("Mise Ã  jour affichage aprÃ¨s coup annulÃ©...");

        setChanged();
        notifyObservers();
    }

    @Override
    public TerrainMatrice clone() {
        return new TerrainMatrice(this);
    }

    @Override
    public void effectuerCoup(Coup coup, boolean notify) throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {

        final Point src = coup.getSourceCoord();
        final Point dest = coup.getDestCoord();
        final Coup temp = new Coup(coup, this);

        setCase(src.x, src.y, coup.getSource());

        try {
            setCase(dest.x, dest.y, coup.getDestination());
        } catch (final HorsDuTerrainException e) {
            setCase(src.x, src.y, temp.getPrecedenteSource());
            throw e;
        }

        if (notify) {
            setChanged();
            notifyObservers(coup);
        }
    }

    @Override
    public boolean finPartie() {

        // Parcours de toutes les cases
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                for (final TYPE_MVT mvt : Coup.TYPE_MVT.values()) {
                    try {
                        new Coup(x, y, mvt, this);
                        return false;
                    } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                             | TourTropGrandeException | HorsDuTerrainException e) {
                    }
                }
            }
        }
        return true;
    }

    @Override
    public Case getCase(int x, int y) throws HorsDuTerrainException {
        if (x < 0 || x >= 9 || y < 0 || y >= 9) {
            throw new HorsDuTerrainException();
        }
        if (cases[y][x] == Case.U || cases[y][x] == Case.C) {
            throw new HorsDuTerrainException();
        }
        return new Case(cases[y][x]);
    }

    /**
     * Donne la liste des voisins jouables
     * @param x
     * @param y
     * @return
     */
    @Override
    public ArrayList<CaseCoord> getCasesVoisinesJouables(int x, int y) {
        final ArrayList<CaseCoord> casesVoisines = new ArrayList<>();
        for (final TYPE_MVT mvt : Coup.TYPE_MVT.values()) {
            try {
                final Coup v = new Coup(x, y, mvt, this);
                casesVoisines.add(new CaseCoord(v.getPrescedenteDest(), v.getDestCoord().x, v.getDestCoord().y));
            } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                     | TourTropGrandeException | HorsDuTerrainException e) {
            }
        }
        return casesVoisines;
    }

    @Override
    public short getDiffNbTours(CouleurJoueur couleurJoueur) {
        int nbCouleurJoueurCourant = 0;
        int nbCouleurJoueurAdverse = 0;
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                final Case c = new Case(cases[y][x]);
                if (c.getCouleur() == couleurJoueur.getValeur() && c.getHauteur() > 0 && c.getHauteur() <= 5) {
                    nbCouleurJoueurCourant++;
                } else if (c.getCouleur() != couleurJoueur.getValeur() && c.getHauteur() > 0 && c.getHauteur() <= 5) {
                    nbCouleurJoueurAdverse++;
                }
            }
        }
        return (short) (nbCouleurJoueurCourant - nbCouleurJoueurAdverse);
    }

    @Override
    public LinkedList<Coup> getListeCoupsPossible() {
        final LinkedList<Coup> listeCoupJouables = new LinkedList<>();
        // Parcours de toutes les cases
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                // Parcours de tous les coups possibles pour la cases courante => Ã  trier en fonction de la stratÃ©gie
                for (final TYPE_MVT mvt : Coup.TYPE_MVT.values()) {
                    try {
                        final Coup v = new Coup(x, y, mvt, this); // en cas d'impossibilitÃ© : lÃ¨ve une exception
                        if (!getCase(x, y).estUneTour()) {
                            System.err.println("Case source vide !!!!!");
                            // throw new NullPointerException();
                        } else if (!getCase(x + mvt.getX(), y + mvt.getY()).estUneTour()) {
                            System.err.println("Case destination vide !!!!!");
                            // throw new NullPointerException();
                        } else {
                            listeCoupJouables.add(v);
                            // System.out.println("liste coup possible : " + v.getPrecedenteSource().getValeurCase());
                        }
                    } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                             | TourTropGrandeException | HorsDuTerrainException e) {

                    }
                }
            }
        }

        return listeCoupJouables;
    }

    /*
     * public short getNbTours(CouleurJoueur couleurJoueur) { int nbTour=0; for (int y = 0; y < 9; y++) { for (int x =
     * 0; x < 9; x++) { Case c = new Case(cases[y][x]); if (c.getCouleur() == couleurJoueur.getValeur() &&
     * c.getHauteur() > 0 && c.getHauteur() <= 5) nbTour++; } } return (short) nbTour; }
     */
    @Override
    public int getNbTours(CouleurJoueur couleurJoueur) {
        int nbCouleurJoueur = 0;
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                final Case c = new Case(cases[y][x]);
                if (c.getCouleur() == couleurJoueur.getValeur() && c.getHauteur() > 0 && c.getHauteur() <= 5) {
                    nbCouleurJoueur++;
                }

            }
        }
        return nbCouleurJoueur;
    }

    public ArrayList<Point> getPointsVoisinsJouables(int x, int y) {
        final ArrayList<Point> pointsVoisins = new ArrayList<>();
        for (final TYPE_MVT mvt : TYPE_MVT.values()) {
            try {
                final Coup c = new Coup(x, y, mvt, this);
                pointsVoisins.add(c.getDestCoord());
            } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                     | TourTropGrandeException | HorsDuTerrainException e) {
            }
        }
        return pointsVoisins;
    }

    @Override
    public byte[][] getTerrainBytes() {
        return cases;
    }

    public byte[][] getTerrainInitial() {
        return TERRAIN_INITIAL;
    }

    @Override
    public byte getValeurCase(int x, int y) {
        return cases[y][x];

    }

    @Override
    public void reinitialiserTerrain() {
        cases = TERRAIN_INITIAL;
        setChanged();
        notifyObservers();
    }

    @Override
    public void setCase(int x, int y, Case c) throws HorsDuTerrainException {
        if (x < 0 || x >= 9 || y < 0 || y >= 9) {
            throw new HorsDuTerrainException();
        }
        if (cases[y][x] == Case.U || cases[y][x] == Case.C) {
            throw new HorsDuTerrainException();
        }
        cases[y][x] = c.getValeurCase();
    }

    @Override
    public void setCoupAvantAnnule(Coup precedentCoup) {
        setChanged();
        notifyObservers(precedentCoup);
    }

    /**
     * Pour les tests!
     * @param terrain
     */
    @Override
    public void setNouvelleConfiguration(byte[][] terrain) {
        cases = terrain;
    }

    @Override
    public String toString() {
        String s = "";
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                switch (cases[y][x]) {
                    case Case.U:
                    case Case.C:
                        s += "  ";
                        break;
                    case Case.B:
                        s += "â˜— ";
                        break;
                    case Case.N:
                        s += "â˜– ";
                        break;

                    default:
                        s += "â˜¹ ";
                        break;
                }
            }
            s += "\n";
        }
        return s;
    }
}
