package common.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import common.Case;
import common.Coup;
import common.Coup.TYPE_MVT;
import common.Terrain;
import common.TerrainMatrice;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public class TestCoup {
    private Terrain terrain;

    @Before
    public void setUp() throws Exception {
        terrain = new TerrainMatrice();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = HorsDuTerrainException.class)
    public void testCaseDestinationInexistante_Constr2() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException, HorsDuTerrainException, CasesNonAdjacentesException {
        final int srcX = 4;
        final int srcY = 0;

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.GAUCHE, terrain);
        terrain.effectuerCoup(coup,true);
    }

    @Test
    public void testCaseDestinationSommeeApresCoup_Constr2() throws HorsDuTerrainException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException {
        final int srcX = 4;
        final int srcY = 1;

        final Case source = terrain.getCase(srcX, srcY);
        Case destination = terrain.getCase(srcX + 1, srcY);
        final int sommeAttendue = source.getHauteur() + destination.getHauteur();

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.DROIT, terrain);
        terrain.effectuerCoup(coup,true);
        destination = terrain.getCase(srcX + 1, srcY);
        assert destination.getHauteur() == sommeAttendue;
    }

    @Test(expected = AjoutSurCaseVideException.class)
    public void testCaseDestinationVide_Constr2() throws HorsDuTerrainException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException {
        final int srcX = 4;
        final int srcY = 0;

        final Case destination = terrain.getCase(srcX + 1, srcY);
        destination.viderCase();
        terrain.setCase(srcX + 1, srcY, destination);

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.DROIT, terrain);
        terrain.effectuerCoup(coup,true);
    }

    @Test(expected = CasesNonAdjacentesException.class)
    public void testCasesNonAdjacentes() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final int srcX = 1;
        final int srcY = 1;
        final int destX = 3;
        final int destY = 3;

        final Coup coup = new Coup(srcX, srcY, destX, destY, terrain);
        terrain.effectuerCoup(coup,true);
    }

    @Test(expected = HorsDuTerrainException.class)
    public void testCaseSourceInexistante_Constr2() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException, HorsDuTerrainException, CasesNonAdjacentesException {
        final int srcX = 0;
        final int srcY = 0;

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.DROIT, terrain);
        terrain.effectuerCoup(coup,true);
    }

    @Test(expected = CaseSourceVideException.class)
    public void testCaseSourceVide_Constr2() throws HorsDuTerrainException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException {
        final int srcX = 4;
        final int srcY = 0;

        final Case source = terrain.getCase(srcX, srcY);
        source.viderCase();
        terrain.setCase(srcX, srcY, source);

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.DROIT, terrain);
        terrain.effectuerCoup(coup,true);
    }

    @Test
    public void testCaseSourceVideApresCoup_Constr2() throws HorsDuTerrainException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException {
        final int srcX = 4;
        final int srcY = 0;

        Case source = terrain.getCase(srcX, srcY);
        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.DROIT, terrain);
        terrain.effectuerCoup(coup,true);
        source = terrain.getCase(srcX, srcY);
        assert source.getHauteur() == 0;
    }

    public void testConstructionCoupSourceVide() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final int srcX = 4;
        final int srcY = 4;
        final int destX = 3;
        final int destY = 5;

        final Coup coup = new Coup(srcX, srcY, destX, destY, terrain);
        terrain.effectuerCoup(coup,true);

        assert coup.getDestCoord().x == destX && coup.getDestCoord().y == destY && coup.getSourceCoord().x == srcX
            && coup.getSourceCoord().y == srcY;
    }

    @Test(expected = HorsDuTerrainException.class)
    public void testCoordsCorrespondentApresCoup_Constr2() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final int srcX = 4;
        final int srcY = 4;
        final int destX = 3;
        final int destY = 5;

        final Coup coup = new Coup(srcX, srcY, destX, destY, terrain);
        terrain.effectuerCoup(coup,true);
        assert coup.getDestCoord().x == destX && coup.getDestCoord().y == destY && coup.getSourceCoord().x == srcX
            && coup.getSourceCoord().y == srcY;
    }

    @Test
    public void testCoordsCorrespondentApresCoup_Constr3() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final int srcX = 5;
        final int srcY = 4;
        final int destX = 4;
        final int destY = 5;

        final Coup coup = new Coup(srcX, srcY, destX, destY, terrain);
        terrain.effectuerCoup(coup,true);
        assert coup.getDestCoord().x == destX && coup.getDestCoord().y == destY && coup.getSourceCoord().x == srcX
            && coup.getSourceCoord().y == srcY;
    }

    @Test
    public void testCouleurCaseDestinationValideApresCoup_Constr2() throws TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, HorsDuTerrainException, CasesNonAdjacentesException {
        final int srcX = 4;
        final int srcY = 0;

        final Case source = terrain.getCase(srcX, srcY);
        final byte couleurAttendue = source.getCouleur();

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.DROIT, terrain);

        final Case destination = terrain.getCase(srcX + 1, srcY);
        terrain.effectuerCoup(coup,true);
        assert destination.getCouleur() == couleurAttendue;
    }

    @Test(expected = HorsDuTerrainException.class)
    public void testCoupEnDehors_Constr1() throws HorsDuTerrainException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException {
        final int srcX = -5;
        final int srcY = -5;
        final int destX = -4;
        final int destY = -4;

        final Coup coup = new Coup(srcX, srcY, destX, destY, terrain);
        terrain.effectuerCoup(coup,true);
    }

    @Test(expected = HorsDuTerrainException.class)
    public void testCoupEnDehors_Constr2() throws HorsDuTerrainException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException {
        final int srcX = 0;
        final int srcY = 0;

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.HAUT_GAUCHE, terrain);
        terrain.effectuerCoup(coup,true);
    }

    @Test(expected = NullPointerException.class)
    public void testTerrainNull_Constr1() throws NullPointerException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final Coup coup = new Coup(5, 5, 4, 4, null);
        terrain.effectuerCoup(coup,true);
    }

    @Test(expected = NullPointerException.class)
    public void testTerrainNull_Constr2() throws NullPointerException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final Coup coup = new Coup(5, 5, TYPE_MVT.BAS, null);
        terrain.effectuerCoup(coup,true);
    }

    @Test(expected = TourTropGrandeException.class)
    public void testTourTropGrande_Constr2() throws HorsDuTerrainException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException {
        final int srcX = 4;
        final int srcY = 0;
        final int destX = srcX + 1;
        final int destY = 0;

        /* Rendre la case destination pleine */
        final Case destination = terrain.getCase(destX, destY);
        destination.assignerValeur((byte) 0b0000_0111);
        terrain.setCase(destX, destY, destination);

        final Coup coup = new Coup(srcX, srcY, TYPE_MVT.DROIT, terrain);
        terrain.effectuerCoup(coup,true);
    }

}
