package common.tests;

import org.junit.Assert;
import org.junit.Test;

import common.Case;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.TourTropGrandeException;

public class TestCase {
    public Case c;

    // Test assignerValeur()
    @Test
    public void testassignerByte() throws TourTropGrandeException {
        final byte b = 0b0001;
        c = new Case(b);
        c.assignerValeur(b);
        Assert.assertEquals(c.getValeurCase(), b);
    }

    @Test(expected = TourTropGrandeException.class)
    public void testassignerByteErr() throws TourTropGrandeException {
        final byte b = 0b0111;
        c = new Case(b);
        c.assignerValeur(b);
    }

    @Test(expected = TourTropGrandeException.class)
    public void testassignerByteErr2() throws TourTropGrandeException {
        final byte b = 0b1111;
        c = new Case(b);
        c.assignerValeur(b);
    }

    @Test
    public void testAugmenterHauteurDeux() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException {
        final byte b = 0b0010;
        c = new Case(b);
        final byte a = 2;
        c.augmenterHauteur(a);
        Assert.assertEquals(c.getHauteur(), 4);

    }

    @Test(expected = TourTropGrandeException.class)
    public void testAugmenterHauteurErr1() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException {
        final byte b = 0b0111;
        c = new Case(b);
        final byte a = 1;
        c.augmenterHauteur(a);

    }

    @Test(expected = TourTropGrandeException.class)
    public void testAugmenterHauteurErr2() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException {
        final byte b = 0b1101;
        c = new Case(b);
        final byte a = 1;
        c.augmenterHauteur(a);

    }

    // Tests augmenterHauteur(byte b)
    @Test
    public void testAugmenterHauteurUn() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException {
        final byte b = 0b0000;
        c = new Case(b);
        final byte a = 1;
        c.augmenterHauteur(a);
        Assert.assertEquals(c.getHauteur(), 1);

    }

    @Test
    public void testChangerCouleurUn() {
        final byte b = 0b1010;
        c = new Case(b);
        c.changerCouleur();
        Assert.assertEquals(c.getCouleur(), 0);

    }

    // Test de changerCouleur()
    @Test
    public void testChangerCouleurZero() {
        final byte b = 0b0001;
        c = new Case(b);
        c.changerCouleur();
        Assert.assertEquals(c.getCouleur(), 1);

    }

    @Test
    public void testGetCouleurU() {
        final byte f = 0b1010;
        c = new Case(f);
        Assert.assertEquals(c.getCouleur(), 1);
    }

    @Test
    public void testGetCouleurZ() {
        final byte f = 0b0011;
        c = new Case(f);
        Assert.assertEquals(c.getCouleur(), 1);
    }

    @Test
    public void testGetHauteurUn_Cinq() {
        final byte f = 0b1101;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 5);
    }

    @Test
    public void testGetHauteurUn_Deux() {
        final byte f = 0b1010;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 2);
    }

    @Test
    public void testGetHauteurUn_Un() {
        final byte f = 0b1001;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 1);
    }

    // Test de getHauteur()
    @Test
    public void testGetHauteurUn_Zero() {
        final byte f = 0b1000;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 0);
    }

    @Test
    public void testGetHauteurZero_Cinq() {
        final byte f = 0b0101;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 5);
    }

    @Test
    public void testGetHauteurZero_Deux() {
        final byte f = 0b0010;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 2);
    }

    @Test
    public void testGetHauteurZero_Un() {
        final byte f = 0b0001;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 1);
    }

    @Test
    public void testGetHauteurZero_Zero() {
        final byte f = 0b0000;
        c = new Case(f);
        Assert.assertEquals(c.getHauteur(), 0);
    }
}
