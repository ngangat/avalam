/**
 * 
 */
package common.tests;

import java.util.ArrayList;
import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import common.Case;
import common.CaseCoord;
import common.CouleurJoueur;
import common.Coup;
import common.TerrainMatrice;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TerrainNonChargeException;
// import exceptions.TerrainNonChargeException;
import exceptions.TourTropGrandeException;

/**
 * @author issa
 *
 */
public class TestTerrainMatrice {

    private TerrainMatrice t;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        t = new TerrainMatrice();

    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link common.TerrainMatrice#afficherTerrainInitialConsole()}.
     */
    @Test
    public void testAfficherTerrainInitialConsole() throws TerrainNonChargeException {
        final byte[][] res = t.getTerrainInitial();
        System.out.println(t.toString());
        final TerrainMatrice test = new TerrainMatrice();
        assert test.getTerrainInitial() == res;
    }

    /**
     * Test method for {@link common.TerrainMatrice#annulerCoup(common.Coup)}.
     * @throws HorsDuTerrainException 
     * @throws TourTropGrandeException 
     * @throws AjoutSurCaseVideException 
     * @throws CaseSourceVideException 
     * @throws CasesNonAdjacentesException 
     */
    @Test
    public void testAnnulerCoup() throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {
        final int srcX = 5;
        final int srcY = 6;
        final int destX = 4;
        final int destY = 7;

        final Coup coup = new Coup(srcX, srcY, destX, destY, t);
        t.annulerCoup(coup);
        assert coup.getSourceCoord().x == srcX && coup.getSourceCoord().y == srcY;
    }

    @Test(expected = HorsDuTerrainException.class)
    public void testEffectuerCoupHorsTerrain() throws TourTropGrandeException, CasesNonAdjacentesException,
        HorsDuTerrainException, CaseSourceVideException, AjoutSurCaseVideException {
        final int srcX = -5;
        final int srcY = -5;
        final int destX = -4;
        final int destY = -4;

        final Coup coup = new Coup(srcX, srcY, destX, destY, t);
        t.effectuerCoup(coup, true);
    }

    /**
     * Test method for {@link common.TerrainMatrice#effectuerCoup(common.Coup)}.
     * @throws AjoutSurCaseVideException 
     * @throws CaseSourceVideException 
     */
    @Test(expected = CasesNonAdjacentesException.class)
    public void testEffectuerCoupNonAdjad() throws TourTropGrandeException, CasesNonAdjacentesException,
        HorsDuTerrainException, CaseSourceVideException, AjoutSurCaseVideException {
        final int srcX = 1;
        final int srcY = 1;
        final int destX = 3;
        final int destY = 3;

        final Coup coup = new Coup(srcX, srcY, destX, destY, t);
        t.effectuerCoup(coup, true);
    }

    /**
     * Test method for {@link common.TerrainMatrice#getCasesVoisines(int, int)}.
     * @throws HorsDuTerrainException 
     * @throws TourTropGrandeException 
     * @throws AjoutSurCaseVideException 
     * @throws CaseSourceVideException 
     */
    @Test
    public void testGetCasesVoisines() throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {
        final int srcX = 5;
        final int srcY = 6;
        final int destX = 4;
        final int destY = 7;

        final Coup coup = new Coup(srcX, srcY, destX, destY, t);
        final ArrayList<CaseCoord> casesVoisines = t.getCasesVoisinesJouables(srcX, srcY);
        final int taille = casesVoisines.size();
        assert casesVoisines.size() == 0;
        assert casesVoisines.get(taille) == coup.getDestination();

    }

    /**
     * Test method for {@link common.TerrainMatrice#getDiffNbTours(common.CouleurJoueur)}.
     * @throws HorsDuTerrainException 
     */
    @Test
    public void testGetDiffNbTours() throws HorsDuTerrainException {
        CouleurJoueur j1, j2;
        j1 = CouleurJoueur.BLANC;
        j2 = CouleurJoueur.NOIR;

        final short res = t.getDiffNbTours(j1);
        final int tourJ1 = t.getNbTours(j1);
        final int tourJ2 = t.getNbTours(j2);

        assert res == tourJ1 - tourJ2;

    }

    /**
     * Test method for {@link common.TerrainMatrice#getListeCoupsPossible()}.
     */
    @Test
    public void testGetListeCoupsPossibleVide() throws TourTropGrandeException, CasesNonAdjacentesException,
        HorsDuTerrainException {
        final LinkedList<Coup> listeCoupJouables = t.getListeCoupsPossible();
        assert listeCoupJouables.size() == 0;
    }

    /**
     * Test method for {@link common.TerrainMatrice#getValeurCase(int, int)}.
     */
    @Test
    public void testGetValeurCase() throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {
        final int destX = 3;
        final int destY = 4;

        final byte b = t.getValeurCase(destX, destY);
        final Case c = t.getCase(destX, destY);
        assert c.getValeurCase() == b;

    }

}
