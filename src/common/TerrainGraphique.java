package common;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.Timer;

import client.ChargeurDeRessources;
import client.window.FenetrePrincipale;
import exceptions.HorsDuTerrainException;

public class TerrainGraphique extends JPanel implements Observer, ActionListener {
    /**
     * 
     */
    private static final long serialVersionUID = 7422986743904388598L;

    private final Terrain     terrain;

    private Point             sourceMouvement  = null;
    private Coup.TYPE_MVT     typeMouvement;

    private boolean           selected;
    private Point             sourceSelected;
    private Point             positionSouris;

    private boolean           isAnim           = false;
    private boolean           animModeReverse;
    private Timer             timerAnim;
    private Point             currentPos;
    private Point             destPos;
    private Image             pionAnim;
    private Image             pionOld;
    
    private boolean           isConseil        = false;
    
    private BufferedImage     calquePlateau;
    private BufferedImage     calqueSelection;
    private BufferedImage     calqueAnim;
    private BufferedImage     calqueConseil;
    
    private long              tstart;

    public TerrainGraphique(Terrain t) {
        terrain = t;
        sourceSelected = null;
        selected = false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
        
        int depl = animModeReverse?-2:2;
        
        currentPos.x += typeMouvement.getX() * depl;
        currentPos.y += typeMouvement.getY() * depl;
        
        if (currentPos.equals(destPos)) {
            isAnim = false;
            timerAnim.stop();
            System.out.println("Durée animation : " + (double) ((System.nanoTime() - tstart) / 1000000.0));
        }
    }

    public synchronized boolean isAnim() {
        return isAnim;
    }

    public void animerCoup(Coup coup, boolean modeReverse) {
        animModeReverse = modeReverse;
        sourceMouvement = modeReverse?coup.getDestCoord():coup.getSourceCoord();
        typeMouvement = coup.getTypeMouvement();

        currentPos = new Point(sourceMouvement.x * getLargeurCase() + getLargeurCase() / 2
                               , sourceMouvement.y * getHauteurCase() + getHauteurCase() / 2);

        destPos = new Point((modeReverse?coup.getSourceCoord():coup.getDestCoord()).x * getLargeurCase() + getLargeurCase() / 2
                            , (modeReverse?coup.getSourceCoord():coup.getDestCoord()).y * getHauteurCase() + getHauteurCase() / 2);

        pionAnim = ChargeurDeRessources.getPion(coup.getPrecedenteSource().getCouleur()
                                                , coup.getPrecedenteSource().getHauteur()
                                                , false);
        
        if (modeReverse)
            pionOld = ChargeurDeRessources.getPion(coup.getPrescedenteDest().getCouleur()
                                                    , coup.getPrescedenteDest().getHauteur()
                                                    , false);
        
        tstart = System.nanoTime();
        isAnim = true;
        timerAnim = new Timer(10, this);
        timerAnim.start();
    }

    private void dessinerPion(Graphics2D drawpad, Image pion, int x, int y) {
        drawpad.drawImage(pion, x - pion.getWidth(null) / 2, y - pion.getHeight(null) / 2, null);
    }

    public int getHauteurCase() {
        return getHeight() / 9;
    }

    public int getLargeurCase() {
        return getWidth() / 9;
    }

    @Override
    public void paintComponent(Graphics g) {
        redessinerCalquePlateau();

        g.drawImage(calquePlateau, 0, 0, null);

        if (selected) {
            redessinerCalqueSelection();
            g.drawImage(calqueSelection, 0, 0, null);
        }

        if (isAnim) {
            redessinerCalqueAnim();
            g.drawImage(calqueAnim, 0, 0, null);
        }
        
        if (isConseil) {
            g.drawImage(calqueConseil, 0, 0, null);
            isConseil = false;
        }
    }

    private void redessinerCalqueAnim() {
        calqueAnim = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
        final Graphics2D drawpad = calqueAnim.createGraphics();

        if (animModeReverse) {
            dessinerPion(drawpad, ChargeurDeRessources.getMouvement(typeMouvement), destPos.x, destPos.y);
            dessinerPion(drawpad
                        , pionOld
                        , sourceMouvement.x * getLargeurCase() + getLargeurCase() / 2
                        , sourceMouvement.y * getHauteurCase() + getHauteurCase() / 2);
        }
        dessinerPion(drawpad, pionAnim, currentPos.x, currentPos.y);
    }

    private void redessinerCalquePlateau() {
        if (terrain == null) {
            throw new NullPointerException("Le terrain doit exister");
        }

        calquePlateau = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);

        final Graphics2D drawpad = calquePlateau.createGraphics();

        drawpad.setPaint(Color.black);
        drawpad.fillRect(0, 0, getWidth(), getHeight());
        drawpad.drawImage(ChargeurDeRessources.getPlateau(), 0, 0, null);

        if (sourceMouvement != null) {
            dessinerPion(drawpad, ChargeurDeRessources.getMouvement(typeMouvement), sourceMouvement.x
                * getLargeurCase() + getLargeurCase() / 2, sourceMouvement.y * getHauteurCase() + getHauteurCase() / 2);
        }

        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < 9; i++) {
                if (isAnim && sourceMouvement.x == i && sourceMouvement.y == j) {
                    continue;
                }

                try {
                    final Case caseCourante = terrain.getCase(i, j);

                    if (caseCourante.getValeurCase() == Case.U || caseCourante.getValeurCase() == Case.C) {
                        continue;
                    }

                    if (caseCourante.getHauteur() == 0) {
                        continue;
                    }

                    dessinerPion(drawpad,
                                 ChargeurDeRessources.getPion(caseCourante.getCouleur(), caseCourante.getHauteur(),
                                                              terrain.getCasesVoisinesJouables(i, j).size() == 0), i
                                     * getLargeurCase() + getLargeurCase() / 2, j * getHauteurCase() + getHauteurCase()
                                     / 2);

                } catch (final HorsDuTerrainException ex) {
                }
            }
        }

    }

    private void redessinerCalqueSelection() {
        if (!selected) {
            return;
        }

        calqueSelection = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
        final ArrayList<CaseCoord> voisins = terrain.getCasesVoisinesJouables(sourceSelected.x, sourceSelected.y);

        try {
            final Case caseSelectionnee = terrain.getCase(sourceSelected.x, sourceSelected.y);
            final Graphics2D drawpad = calqueSelection.createGraphics();

            dessinerPion(drawpad, ChargeurDeRessources.getMasquePionSource(), sourceSelected.x * getLargeurCase()
                + getLargeurCase() / 2, sourceSelected.y * getHauteurCase() + getHauteurCase() / 2);

            if (FenetrePrincipale.getInstance().aideVisuellesActivees()) {
                for (final CaseCoord caseCoord : voisins) {
                    dessinerPion(drawpad, ChargeurDeRessources.getPionOverlay(), caseCoord.getX() * getLargeurCase()
                        + getLargeurCase() / 2, caseCoord.getY() * getHauteurCase() + getHauteurCase() / 2);
                }
            }

            dessinerPion(drawpad, ChargeurDeRessources.getPion(caseSelectionnee.getCouleur(),
                                                               caseSelectionnee.getHauteur(), false), positionSouris.x,
                         positionSouris.y);
        } catch (final HorsDuTerrainException ex) {
        }
    }

    private void setMouvement(Coup coup) {
        sourceMouvement = coup.getSourceCoord();
        typeMouvement = coup.getTypeMouvement();
    }

    public void setPositionSouris(Point p) {
        positionSouris = p;
    }

    public void setSelected(boolean s) {
        selected = s;
    }

    public void setSource(Point p) {
        sourceSelected = p;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Coup) {
            setMouvement((Coup) arg);
        }
        repaint();
    }

    public void montrerConseil(Coup conseil) {
        isConseil = true;
        redessinerCalqueConseil(conseil);
        repaint();
    }

    private void redessinerCalqueConseil(Coup conseil) {
        calqueConseil = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D drawpad = calqueConseil.createGraphics();
        dessinerPion(drawpad
                    , ChargeurDeRessources.getConseil(conseil.getTypeMouvement())
                    , conseil.getSourceCoord().x * getLargeurCase() + getLargeurCase()/2 + conseil.getTypeMouvement().getX()*getLargeurCase()/2
                    , conseil.getSourceCoord().y * getHauteurCase() + getHauteurCase()/2 + conseil.getTypeMouvement().getY()*getHauteurCase()/2);
    }
}
