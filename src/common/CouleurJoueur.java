package common;

import java.io.Serializable;

public enum CouleurJoueur implements Serializable {
    BLANC(0), // joueur 1
    NOIR(1), // joueur 2
    TOUS(2); // les deux joueurs
    private int valeur;

    private CouleurJoueur(int valeur) {
        this.valeur = valeur;

    }

    public CouleurJoueur getCouleurAdversaire() throws IllegalAccessException {
        if (equals(TOUS)) {
            throw new IllegalAccessException(
                                             "la couleur doit être fixé à BLANC ou NOIR avant d'utiliser cette fonction");
        }
        return equals(BLANC) ? NOIR : BLANC;
    }

    public int getValeur() {
        return valeur;
    }
}
