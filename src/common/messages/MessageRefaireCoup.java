package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;

public class MessageRefaireCoup extends Message implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6282607832054480615L;

    public MessageRefaireCoup() {
        super();
        codage();
    }

    @Override
    protected void codage() {
        setCodeMessage(CodeMessage.DEMANDE_REFAIRE_COUP);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 0) {
            throw new MalformedLinkException("La taille du message est invalide");
        }
        return getCorps();
    }
}
