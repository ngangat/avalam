package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;

public class MessageBienvenue extends Message implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -3240803559697234643L;
    private static final String messageBienvenue = "Bienvenue !";

    public MessageBienvenue() {
        super();
        codage();
    }

    @Override
    public void codage() {
        super.setCodeMessage(CodeMessage.BIENVENUE);
        super.getCorps().add(MessageBienvenue.messageBienvenue);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (super.getCorps().size() != 1) {
            throw new MalformedLinkException("le message de bienvenue n'a pas la taille attendue");
        }
        if (!(super.getCorps().get(0) instanceof String)) {
            throw new MalformedLinkException("Le corps du message n'est pas de type string");
        }
        return super.getCorps();
    }

    public String getMessageBienvenue() throws MalformedLinkException {
        return (String) decodage().getFirst();
    }
}
