package common.messages;

import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.Coup;

public class MessageBroadcastRefaireCoup extends Message {

    /**
     * 
     */
    private static final long serialVersionUID = 2878341795156129262L;
    Coup                      coup;
    Integer                   nbRefaireRestant;

    public MessageBroadcastRefaireCoup(Coup c, int nb) {
        super();
        coup = c;
        nbRefaireRestant = nb;
        codage();
    }

    @Override
    protected void codage() {
        setCodeMessage(CodeMessage.BROADCAST_REFAIRE_COUP);
        getCorps().add(coup);
        getCorps().add(nbRefaireRestant);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 2) {
            throw new MalformedLinkException("La taille du message est invalide");
        }
        if (!(getCorps().get(0) instanceof Coup)) {
            throw new MalformedLinkException("Le premier élément attendu dans le message doit être un Coup");
        }
        if (!(getCorps().get(1) instanceof Integer)) {
            throw new MalformedLinkException("Le second élément attendu dans le message doit être un Integer");
        }

        return getCorps();
    }

    public Coup getCoup() throws MalformedLinkException {
        return (Coup) decodage().get(0);
    }

    public int getNbRefaireRestant() throws MalformedLinkException {
        return ((Integer) decodage().get(1)).intValue();
    }

}
