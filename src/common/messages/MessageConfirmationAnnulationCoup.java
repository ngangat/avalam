package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.Coup;

public class MessageConfirmationAnnulationCoup extends Message implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1135746337917791679L;
    private final Coup        coupAAnnuler;
    private final Boolean     okPourAnnulation;

    public MessageConfirmationAnnulationCoup(Coup c, boolean isOk) {
        super();
        coupAAnnuler = c;
        okPourAnnulation = isOk;
        codage();
    }

    @Override
    protected void codage() {
        setCodeMessage(CodeMessage.CONFIRMATION_ANNULATION);
        getCorps().add(coupAAnnuler);
        getCorps().add(okPourAnnulation);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 2) {
            throw new MalformedLinkException("La taille du message est invalide");
        }
        if (!(getCorps().get(0) instanceof Coup)) {
            throw new MalformedLinkException("Le premier élément du message est invalide, un Coup est attendu");
        }
        if (!(getCorps().get(1) instanceof Boolean)) {
            throw new MalformedLinkException("Le second élément du message est invalide, un Boolean est attendu");
        }

        return getCorps();
    }

    public Coup getCoup() throws MalformedLinkException {
        return (Coup) decodage().get(0);
    }

    public boolean isOkPourAnnulation() throws MalformedLinkException {
        return ((Boolean) decodage().get(1)).booleanValue();
    }

}
