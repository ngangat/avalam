package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.Coup;

public class MessageDemandeConfirmationAnnulationCoup extends Message implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4996921967130568674L;
    Coup                      coup;

    public MessageDemandeConfirmationAnnulationCoup(Coup c) {
        super();
        coup = c;
        codage();
    }

    @Override
    protected void codage() {
        setCodeMessage(CodeMessage.DEMANDE_CONFIRMATION_ANNULATION);
        getCorps().add(coup);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 1) {
            throw new MalformedLinkException("La taille du message est invalide");
        }
        if (!(getCorps().getFirst() instanceof Coup)) {
            throw new MalformedLinkException("Le contenu du message est invalide");
        }

        return getCorps();
    }

    public Coup getCoup() throws MalformedLinkException {
        return (Coup) decodage().getFirst();
    }

}
