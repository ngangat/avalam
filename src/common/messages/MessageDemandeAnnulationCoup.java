package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.TypePartie;

public class MessageDemandeAnnulationCoup extends Message implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8263376783679081325L;
    TypePartie                typePartie;

    public MessageDemandeAnnulationCoup(TypePartie t) {
        super();
        typePartie = t;
        codage();
    }

    @Override
    protected void codage() {
        setCodeMessage(CodeMessage.DEMANDE_ANNULATION_COUP);
        getCorps().add(typePartie);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 1) {
            throw new MalformedLinkException("La taille du message est invalide");
        }
        if (!(getCorps().getFirst() instanceof TypePartie)) {
            throw new MalformedLinkException("Le contenu du message est invalide");
        }

        return getCorps();
    }

    public TypePartie getTypePartie() {
        return (TypePartie) getCorps().getFirst();
    }

}
