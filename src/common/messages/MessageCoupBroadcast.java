package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.CouleurJoueur;
import common.Coup;

public class MessageCoupBroadcast extends Message implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -5056832379436613715L;
    private final Coup          coup;
    private final CouleurJoueur prochainJoueur;

    public MessageCoupBroadcast(MessageCoup messageCoupRecu, CouleurJoueur couleur) throws MalformedLinkException {
        super();
        coup = messageCoupRecu.getCoup();
        prochainJoueur = couleur;
        codage();
    }

    @Override
    protected void codage() {
        setCodeMessage(CodeMessage.BROADCAST_COUP);

        getCorps().add(coup);
        getCorps().add(prochainJoueur);

        /*
         * TODO
         * codage throws si mal construit
         */
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 2) {
            throw new MalformedLinkException("Le message de broadcast de coup n'a pas la forme attendue");
        }

        if (!(getCorps().get(0) instanceof Coup)) {
            throw new MalformedLinkException("Le corps message ne contient pas un Coup dans son premier champ");
        }
        if (!(getCorps().get(1) instanceof CouleurJoueur)) {
            throw new MalformedLinkException(
                                             "Le corps du message ne contient pas un CouleurJoueur dans son deuxième champ");
        }

        return getCorps();
    }

    public CouleurJoueur getCouleurProchainJoueur() throws MalformedLinkException {
        return (CouleurJoueur) decodage().get(1);
    }

    public Coup getCoup() throws MalformedLinkException {
        return (Coup) decodage().get(0);
    }

}
