package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.Coup;

public class MessageCoup extends Message implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -9157615604748871955L;
    private final Coup        coup;

    public MessageCoup(Coup c) {
        super();
        coup = c;
        System.out.println("Coup à envoyer : " + c.getSource().getValeurCase() + ", "
            + c.getDestination().getValeurCase());
        codage();
    }

    public MessageCoup(MessageCoup messageCoup) throws MalformedLinkException {
        this(messageCoup.getCoup());
    }

    @Override
    protected void codage() {
        super.setCodeMessage(CodeMessage.COUP);
        super.getCorps().add(coup);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (super.getCorps().size() != 1) {
            throw new MalformedLinkException("Le message du coup joué n'a pas la taille attendue");
        }

        if (!(super.getCorps().get(0) instanceof Coup)) {
            throw new MalformedLinkException("Le corps du message n'est pas du type Coup");
        }

        return super.getCorps();
    }

    public Coup getCoup() throws MalformedLinkException {
        return (Coup) decodage().get(0);
    }

    public void setCodeErreur() {
        setCodeMessage(CodeMessage.ERR_COUP);
    }
}
