package common.messages;

import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.CouleurJoueur;

public class MessageBroadcastFinPartie extends Message {
    /**
     * 
     */
    private static final long serialVersionUID = -7011400145984176488L;
    CouleurJoueur             couleurJoueurGagnant;

    public MessageBroadcastFinPartie(CouleurJoueur gagnant) {
        couleurJoueurGagnant = gagnant;
    }

    @Override
    protected void codage() throws MalformedLinkException {
        setCodeMessage(CodeMessage.BROADCAST_FIN_PARTIE);
        getCorps().add(couleurJoueurGagnant);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 1) {
            throw new MalformedLinkException("La taille du message est invalide");
        }

        if (!(getCorps().get(0) instanceof CouleurJoueur)) {
            throw new MalformedLinkException("Le contenu du message est invalide");
        }

        return getCorps();
    }

    public CouleurJoueur getCouleurGagnant() {
        return (CouleurJoueur) getCorps().get(0);
    }

}
