/**
 * 
 */
package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.Coup;
import common.InfosPartie;

/**
 * @author ngangat
 *
 */
public class MessageInfosPartie extends Message implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3585282160919090461L;
    private final InfosPartie infosPartie;

    public MessageInfosPartie(InfosPartie infosPartie) {
        super();
        super.setCodeMessage(CodeMessage.INFOS_PARTIE);
        this.infosPartie = infosPartie;
        try {
            codage();
        } catch (final MalformedLinkException e) {
            e.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     * @see common.messages.Message#codage()
     */
    @Override
    protected void codage() throws MalformedLinkException {
        if (infosPartie == null) {
            throw new MalformedLinkException("Infos partie ne peut avoir une référence nulle");
        }
        if (super.getCorps().size() != 0) {
            throw new MalformedLinkException("Le message ne peut être codé qu'une seule fois.");
        }
        super.getCorps().add(infosPartie);
    }

    /*
     * (non-Javadoc)
     * @see common.messages.Message#decodage()
     */
    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 1) {
            throw new MalformedLinkException(
                                             "Le message doit contenir les informations de la partie, et uniquement les infos de la partie");
        }

        if (!(getCorps().get(0) instanceof Coup)) {
            throw new MalformedLinkException("Le corps message ne contient pas un Coup dans son premier champ");
        }
        return getCorps();

    }

    public InfosPartie getInfosPartie() {
        return infosPartie;
    }

}
