package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;

public class MessageNom extends Message implements Serializable {

    private static final long serialVersionUID = 8416078352395517671L;
    private String            nomJoueur;

    public MessageNom(String j1) throws MalformedLinkException {
        super();
        super.setCodeMessage(CodeMessage.NOMJOUEUR);
        setNomJoueur(j1);
        codage();
    }

    @Override
    protected void codage() throws MalformedLinkException {

        if (nomJoueur == null || nomJoueur.length() < 1 || nomJoueur.length() > 15) {
            throw new MalformedLinkException("Le nom du joueur doit avoir une taille compris entre 1 et 15");
        }

        if (super.getCorps().isEmpty()) {
            super.getCorps().add(nomJoueur);
        } else {
            return;
        }
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (!super.getCodeMessage().equals(CodeMessage.NOMJOUEUR)
            || !super.getCodeMessage().equals(CodeMessage.NOM_DEJA_UTILISE)) {
            throw new MalformedLinkException("Le code message ne correspond pas à un MessageNom.");
        }
        if (super.getCorps().isEmpty()) {
            throw new MalformedLinkException("Le message n'a pas la taille attendu");
        }
        if (!(super.getCorps().get(0) instanceof String)) {
            throw new MalformedLinkException("Le message n'a pas le format attendu");
        }
        if (!(((String) super.getCorps().get(0)).length() >= 1 && ((String) super.getCorps().get(0)).length() <= 15)) {
            throw new MalformedLinkException("Le nom n'a pas une taille compris entre 1 et 15");
        }
        return super.getCorps();
    }

    public String getNomJoueur() {
        return nomJoueur;
    }

    public void setCodeErreur() {
        super.setCodeMessage(CodeMessage.NOM_DEJA_UTILISE);
    }

    public void setNomJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }

}
