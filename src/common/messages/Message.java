package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;

public abstract class Message implements Serializable {

    private static final long        serialVersionUID = -3235527367410123458L;
    private CodeMessage              codeMessage;
    private final LinkedList<Object> corps;

    protected Message() {
        codeMessage = CodeMessage.NONDEFINI;
        corps = new LinkedList<>();
    }

    protected abstract void codage() throws MalformedLinkException;

    protected abstract LinkedList<Object> decodage() throws MalformedLinkException;

    public CodeMessage getCodeMessage() {
        return codeMessage;
    }

    protected LinkedList<Object> getCorps() {
        return corps;
    }

    protected void setCodeMessage(CodeMessage codeMessage) {
        this.codeMessage = codeMessage;
    }

}
