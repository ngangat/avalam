package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.Coup;

public class MessageBroadcastAnnulationCoup extends Message implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 5338667513772047938L;
    Coup                      coupAAnnuler;
    Integer                   nbAnnulationsPossibles;
    Coup                      coupPrecedent;

    public MessageBroadcastAnnulationCoup(Coup c, int nb) {
        super();
        coupAAnnuler = c;
        nbAnnulationsPossibles = nb;
        codage();
    }

    public MessageBroadcastAnnulationCoup(Coup c, int nb, Coup cPrev) {
        super();
        coupAAnnuler = c;
        coupPrecedent = cPrev;
        nbAnnulationsPossibles = nb;
        codage();
    }

    @Override
    protected void codage() {
        setCodeMessage(CodeMessage.BROADCAST_ANNULATION_COUP);
        getCorps().add(coupAAnnuler);
        getCorps().add(nbAnnulationsPossibles);
        if (nbAnnulationsPossibles > 0) {
            getCorps().add(coupPrecedent);
        }
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 2 && getCorps().size() != 3) {
            throw new MalformedLinkException("La taille du message est invalide");
        }
        if (!(getCorps().get(0) instanceof Coup)) {
            throw new MalformedLinkException("Le premier élément du message n'est pas un Coup");
        }
        if (!(getCorps().get(1) instanceof Integer)) {
            throw new MalformedLinkException("Le second élément du message n'est pas un Integer");
        }

        if ((Integer) getCorps().get(1) > 0 && !(getCorps().get(2) instanceof Coup)) {
            throw new MalformedLinkException("Le troisième élément du message n'est pas un Coup");
        }

        return getCorps();
    }

    public Coup getCoup() throws MalformedLinkException {
        return (Coup) decodage().get(0);
    }

    public int getNbAnnulationsPossibles() throws MalformedLinkException {
        return ((Integer) decodage().get(1)).intValue();
    }

    public Coup getPrecedentCoup() throws MalformedLinkException {
        if (isAnnulerPossible()) {
            return (Coup) decodage().get(2);
        }
        return null;
    }

    public boolean isAnnulerPossible() throws MalformedLinkException {
        return getNbAnnulationsPossibles() > 0;
    }

}
