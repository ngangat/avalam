package common.messages;

import java.io.Serializable;
import java.util.LinkedList;

import javax.naming.MalformedLinkException;

import common.CodeMessage;
import common.CouleurJoueur;

public class MessageAnnonceCouleur extends Message implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -8200547164559780566L;
    private final CouleurJoueur couleur;
    private final String        nom;

    public MessageAnnonceCouleur(CouleurJoueur couleurJoueur, String nomJoueur) throws MalformedLinkException {
        super();
        couleur = couleurJoueur;
        nom = nomJoueur;
        codage();
    }

    @Override
    protected void codage() throws MalformedLinkException {
        setCodeMessage(CodeMessage.ANNONCE_COULEUR);
        if (couleur == null) {
            throw new MalformedLinkException("La couleur ne peut être nulle.");
        }
        if (nom == null || nom.length() < 1 || nom.length() > 15) {
            throw new MalformedLinkException("Le nom doit être compris entre 1 et 15 caractères.");
        }

        getCorps().add(couleur);
        getCorps().add(nom);
    }

    @Override
    protected LinkedList<Object> decodage() throws MalformedLinkException {
        if (getCorps().size() != 2) {
            throw new MalformedLinkException("Ce message n'a pas la bonne taille");
        }

        if (!(getCorps().get(0) instanceof CouleurJoueur)) {
            throw new MalformedLinkException(
                                             "Ce message ne contient pas le bon type d'objet : le premier element ne correspond pas à une couleur");
        }
        if (!(getCorps().get(1) instanceof String)) {
            throw new MalformedLinkException(
                                             "Ce message ne contient pas le bon type d'objet : le deuxieme élement ne correspond pas au nom du joueur");
        }
        return getCorps();
    }

    public CouleurJoueur getCouleur() throws MalformedLinkException {
        return (CouleurJoueur) decodage().get(0);
    }

}
