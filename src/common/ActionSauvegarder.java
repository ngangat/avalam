package common;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class ActionSauvegarder { // TODO A mettre dans le moteur!!
    private final Historique  historique;
    private final InfosPartie infoJoueur1;
    private final InfosPartie infoJoueur2;

    public ActionSauvegarder(Historique h, InfosPartie j1, InfosPartie j2) {
        historique = h;
        infoJoueur1 = j1;
        infoJoueur2 = j2;
    }

    public void sauver() {
        try {
            FileOutputStream fichier = new FileOutputStream("sauvegarde_historique.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fichier);
            oos.writeObject(historique);
            oos.flush();
            oos.close();

            fichier = new FileOutputStream("sauvegarde_j1.ser");
            oos = new ObjectOutputStream(fichier);
            oos.writeObject(infoJoueur1);
            oos.flush();
            oos.close();

            fichier = new FileOutputStream("sauvegarde_j2.ser");
            oos = new ObjectOutputStream(fichier);
            oos.writeObject(infoJoueur2);
            oos.flush();
            oos.close();
        } catch (final java.io.IOException e) {
            e.printStackTrace();
        }
    }
}
