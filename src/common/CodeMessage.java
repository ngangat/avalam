package common;

public enum CodeMessage {
    NONDEFINI,
    BIENVENUE, // message de bienvenue
    ANNONCE_COULEUR, // message correspondant à une annonce de couleur lors de l'init d'une connexion
    COUP, // message correspondant à un envoi de coup
    ERR_COUP, // message correspondant à un coup erroné
    BROADCAST_COUP, // message correspondant à un broadcast de coup + joueur suivant
    NOMJOUEUR,
    NOM_DEJA_UTILISE,
    DEMANDE_ANNULATION_COUP,
    DEMANDE_CONFIRMATION_ANNULATION,
    CONFIRMATION_ANNULATION,
    BROADCAST_ANNULATION_COUP,
    DEMANDE_REFAIRE_COUP,
    BROADCAST_REFAIRE_COUP,
    BROADCAST_FIN_PARTIE,
    INFOS_PARTIE,
}
