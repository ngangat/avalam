package common;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class ActionCharger { // TODO A mettre dans le moteur!!
    private Historique  historique;
    private InfosPartie infoJoueur1;
    private InfosPartie infoJoueur2;

    public ActionCharger() {
        historique = null;
        infoJoueur1 = null;
        infoJoueur2 = null;
    }

    public void charger() {
        try {
            FileInputStream fichier = new FileInputStream("sauvegarde_historique.ser");
            ObjectInputStream ois = new ObjectInputStream(fichier);
            historique = (Historique) ois.readObject();
            fichier.close();

            fichier = new FileInputStream("sauvegarde_j1.ser");
            ois = new ObjectInputStream(fichier);
            infoJoueur1 = (InfosPartie) ois.readObject();
            fichier.close();

            fichier = new FileInputStream("sauvegarde_j2.ser");
            ois = new ObjectInputStream(fichier);
            infoJoueur2 = (InfosPartie) ois.readObject();

            fichier.close();
            ois.close();
        } catch (final java.io.IOException e) {
            e.printStackTrace();
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Historique getHistorique() {
        return historique;
    }

    public InfosPartie getInfoJoueur1() {
        return infoJoueur1;
    }

    public InfosPartie getInfoJoueur2() {
        return infoJoueur2;
    }
}
