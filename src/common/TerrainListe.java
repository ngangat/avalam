package common;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public class TerrainListe implements Terrain {

    private final HashMap<Point, ArrayList<Point>> terrain;
    private final TerrainMatrice                   terrainCopie;

    public TerrainListe(TerrainMatrice t) {
        terrainCopie = t.clone();
        terrain = new HashMap<Point, ArrayList<Point>>();

        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                try {
                    final Case courante = t.getCase(x, y);
                    if (courante.estUneTour() && courante.getHauteur() < 5) {
                        final ArrayList<Point> voisins = terrainCopie.getPointsVoisinsJouables(x, y);
                        if (voisins != null && !voisins.isEmpty()) {
                            terrain.put(new Point(x, y), voisins);
                        }
                    }
                } catch (final HorsDuTerrainException e) {
                }
            }
        }
    }

    public TerrainListe(TerrainMatrice t, HashMap<Point, ArrayList<Point>> pointsTerrain) {
        terrainCopie = t.clone();
        terrain = pointsTerrain;
        for (Point p : terrain.keySet()) {
            Case courante;
            try {
                courante = t.getCase(p.x, p.y);
                if (courante.estUneTour() && courante.getHauteur() < 5) {
                    ArrayList<Point> voisins = terrainCopie.getPointsVoisinsJouables(p.x, p.y);
                    if (voisins != null && !voisins.isEmpty()) {
                        terrain.put(p, voisins);
                    }
                }
            } catch (HorsDuTerrainException e) {
            }

        }
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                if (!terrain.containsKey(new Point(x, y))) {
                    try {
                        terrainCopie.setCase(x, y, Case.NON_DEFINI);
                    } catch (HorsDuTerrainException e) {
                    }
                }
            }
        }
    }

    public HashMap<Point, ArrayList<Point>> getTerrain() {
        return terrain;
    }

    @Override
    public void annulerCoup(Coup coup) throws HorsDuTerrainException {
        final Point src = coup.getSourceCoord();
        final Point dest = coup.getDestCoord();

        terrainCopie.annulerCoup(coup);

        terrain.put(src, new ArrayList<Point>());
        if (terrain.get(dest) == null) {
            terrain.put(dest, new ArrayList<Point>());
        }

        ArrayList<Point> adjascentsSource = terrainCopie.getPointsVoisinsJouables(src.x, src.y);
        ListIterator<Point> lit = adjascentsSource.listIterator();
        while (lit.hasNext()) {
            Point adj = lit.next();
            if (terrain.get(src) == null)
                terrain.put(src, new ArrayList<Point>());
            terrain.get(src).add(adj);
            if (terrain.get(adj) == null)
                terrain.put(adj, new ArrayList<Point>());
            terrain.get(adj).add(src);
        }

        ArrayList<Point> adjascentsDest = terrainCopie.getPointsVoisinsJouables(dest.x, dest.y);
        lit = adjascentsDest.listIterator();
        while (lit.hasNext()) {
            Point adj = lit.next();
            if (terrain.get(dest) == null)
                terrain.put(dest, new ArrayList<Point>());
            if (!terrain.get(dest).contains(adj)) {
                terrain.get(dest).add(adj);
            }
            if (terrain.get(adj) == null)
                terrain.put(adj, new ArrayList<Point>());
            if (!terrain.get(adj).contains(dest)) {
                terrain.get(adj).add(dest);
            }
        }
    }

    @Override
    public void effectuerCoup(Coup coup, boolean notify) throws TourTropGrandeException, HorsDuTerrainException,
        CasesNonAdjacentesException, CaseSourceVideException, AjoutSurCaseVideException {
        final Point src = coup.getSourceCoord();
        final Point dest = coup.getDestCoord();

        terrainCopie.effectuerCoup(coup, notify);

        ArrayList<Point> adjascentsSource = terrain.remove(src);
        for (Point adj : adjascentsSource) {
            terrain.get(adj).remove(src);
            if (terrain.get(adj).isEmpty()) {
                terrain.remove(adj);
            }
        }

        ArrayList<Point> adjascentsDest = terrain.get(dest);
        if (adjascentsDest != null) {
            ListIterator<Point> lit = adjascentsDest.listIterator();
            while (lit.hasNext()) {
                Point adj = lit.next();
                final int somme = terrainCopie.getCase(dest.x, dest.y).getHauteur()
                    + terrainCopie.getCase(adj.x, adj.y).getHauteur();
                if (somme <= 5) {
                    continue;
                }

                terrain.get(adj).remove(dest);
                lit.remove();
                if (terrain.get(adj).isEmpty()) {
                    terrain.remove(adj);
                }
            }
        }
        if (terrain.get(dest) != null && terrain.get(dest).isEmpty()) {
            terrain.remove(dest);
        }
    }

    @Override
    public boolean finPartie() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Case getCase(int x, int y) throws HorsDuTerrainException {
        return terrainCopie.getCase(x, y);
    }

    @Override
    public ArrayList<CaseCoord> getCasesVoisinesJouables(int x, int y) {
        final ArrayList<CaseCoord> casesVoisinesJouables = new ArrayList<CaseCoord>();
        final ArrayList<Point> casesVoisines = terrain.get(new Point(x, y));
        if (casesVoisines != null) {
            for (final Point p : casesVoisines) {
                try {
                    casesVoisinesJouables.add(new CaseCoord(getCase(p.x, p.y), p.x, p.y));
                } catch (TourTropGrandeException | HorsDuTerrainException e) {
                    e.printStackTrace();
                }
            }
        }
        return casesVoisinesJouables;
    }

    @Override
    public short getDiffNbTours(CouleurJoueur couleurJoueur) {
        int nbToursJoueurs = 0;
        int nbToursAdverses = 0;
        for (Point p : terrain.keySet()) {
            try {
                if (terrainCopie.getCase(p.x, p.y).estUneTour()
                    && terrainCopie.getCase(p.x, p.y).getCouleur() == couleurJoueur.getValeur())
                    nbToursJoueurs++;
                else if (terrainCopie.getCase(p.x, p.y).estUneTour()
                    && terrainCopie.getCase(p.x, p.y).getCouleur() == couleurJoueur.getCouleurAdversaire().getValeur())
                    nbToursAdverses++;
            } catch (HorsDuTerrainException | IllegalAccessException e) {

            }
        }
        //System.out.println("difference nb Tours : " + (nbToursJoueurs - nbToursAdverses));
        return (short) (nbToursJoueurs - nbToursAdverses);
    }

    @Override
    public LinkedList<Coup> getListeCoupsPossible() {
        LinkedList<Coup> listeCoupsPossibles = new LinkedList<Coup>();

        for (final Point src : terrain.keySet()) {
            for (final Point dest : terrain.get(src)) {
                try {
                    final Coup c = new Coup(src.x, src.y, dest.x, dest.y, terrainCopie);
                    listeCoupsPossibles.add(c);
                } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                         | TourTropGrandeException | HorsDuTerrainException e) {
                    /*
                     * Normalement TerrainListe ne contiendra toujours QUE des
                     * coups valides
                     */
                    e.printStackTrace();
                }
            }
        }

        return listeCoupsPossibles;
    }

    public long getNbCombinaison() {
        long nbCombinaisons = 0L;
        for (ArrayList<Point> a : terrain.values()) {
            if (a != null)
                nbCombinaisons += a.size();
        }
        return nbCombinaisons;
    }

    /*
     * Méthodes non utiles pour l'IA
     * Inutile de les implémenter
     * *************************************************************************
     */
    @Override
    public int getNbTours(CouleurJoueur couleurJoueur) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public byte[][] getTerrainBytes() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public byte getValeurCase(int x, int y) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void reinitialiserTerrain() {
        // TODO Auto-generated method stub

    }

    @Override
    public void setCase(int x, int y, Case c) throws HorsDuTerrainException {
        // TODO Auto-generated method stub

    }

    @Override
    public void setCoupAvantAnnule(Coup precedentCoup) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setNouvelleConfiguration(byte[][] terrain) {
        // TODO Auto-generated method stub

    }

}
