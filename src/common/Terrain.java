package common;

import java.util.ArrayList;
import java.util.LinkedList;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public interface Terrain {
    public void annulerCoup(Coup c) throws HorsDuTerrainException, CasesNonAdjacentesException,
        CaseSourceVideException, AjoutSurCaseVideException, TourTropGrandeException;

    public void effectuerCoup(Coup coup, boolean notify) throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException;

    public boolean finPartie();

    public Case getCase(int x, int y) throws HorsDuTerrainException;

    public ArrayList<CaseCoord> getCasesVoisinesJouables(int x, int y);

    public short getDiffNbTours(CouleurJoueur couleurJoueur);

    public LinkedList<Coup> getListeCoupsPossible();

    public int getNbTours(CouleurJoueur couleurJoueur);

    public byte[][] getTerrainBytes();

    // public short getNbTours(CouleurJoueur couleurJoueur);

    public byte getValeurCase(int x, int y);

    void reinitialiserTerrain();

    public void setCase(int x, int y, Case c) throws HorsDuTerrainException;

    public void setCoupAvantAnnule(Coup precedentCoup);

    public void setNouvelleConfiguration(byte[][] terrain);
}
