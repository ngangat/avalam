package common;

import java.awt.Point;
import java.io.Serializable;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public class Coup implements Serializable {

    /**
     * Enum représentant un mouvement. La valeur est représenté sur 4 bits signés: Les deux premiers bits de poids fort
     * représente la coordonnée x de la destination ( 01 : droite, 11 : gauche, 00 : pas de mouvement) Les deux premiers
     * bits représente la coodonnée y.
     * 
     * @author ngangat
     */
    public static enum TYPE_MVT {

        DROIT(1, 0),
        GAUCHE(-1, 0),
        HAUT(0, -1),
        BAS(0, 1),
        HAUT_DROIT(1, -1),
        BAS_GAUCHE(-1, 1),
        BAS_DROIT(1, 1),
        HAUT_GAUCHE(-1, -1);
        public static TYPE_MVT getValue(int valeurX, int valeurY) {
            for (final TYPE_MVT t : TYPE_MVT.values()) {
                if (t.getX() == valeurX && t.getY() == valeurY) {
                    return t;
                }
            }
            return null;
        }

        private int valeurX, valeurY;

        TYPE_MVT(int valeurX, int valeurY) {
            this.valeurX = valeurX;
            this.valeurY = valeurY;
        }

        public int getX() {
            return valeurX;
        }

        public int getY() {
            return valeurY;
        }
    }

    private static final long serialVersionUID = -4653611960488484911L;

    private Case              source;                                  // valeur de la source après avoir appliquer le coup

    private Case              destination;                             // valeur de la destination après avoir appliquer le coup
    private final Case        precedenteSource;                        // valeur de la source avant d'appliquer le coup
    private final Case        precedenteDest;                          // valeur de la destination avant d'appliquer le coup
    private Point             sourceCoord;                             // Coordonnée de la source
    private final Point       destCoord;                               // Coordonnée de la destination
    private final TYPE_MVT    typeMouvement;

    public Coup(Coup coup, Terrain terrain) throws CasesNonAdjacentesException, CaseSourceVideException,
                                           AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {
        this(coup.getSourceCoord().x, coup.getSourceCoord().y, coup.getDestCoord().x, coup.getDestCoord().y, terrain);
    }

    /* Constructeurs */

    public Coup(int srcX, int srcY, int destX, int destY, Terrain terrain) throws CasesNonAdjacentesException,
                                                                          CaseSourceVideException,
                                                                          AjoutSurCaseVideException,
                                                                          TourTropGrandeException,
                                                                          HorsDuTerrainException {
        final int mouvementX = destX - srcX;
        final int mouvementY = destY - srcY;

        if (terrain == null) {
            throw new NullPointerException("Le terrain doit exister");
        }

        source = new Case(terrain.getCase(srcX, srcY));
        destination = new Case(terrain.getCase(destX, destY));
        precedenteSource = new Case(terrain.getCase(srcX, srcY));
        precedenteDest = new Case(terrain.getCase(destX, destY));
        sourceCoord = new Point(srcX, srcY);
        destCoord = new Point(destX, destY);

        final boolean coordsAdjacentes = Math.abs(mouvementX) == 1 && mouvementY == 0 || mouvementX == 0
            && Math.abs(mouvementY) == 1 || Math.abs(mouvementX) == 1 && Math.abs(mouvementY) == 1;

        if (!coordsAdjacentes) {
            throw new CasesNonAdjacentesException();
        }

        if (source.getHauteur() == 0) {
            throw new CaseSourceVideException();
        }

        if (destination.getHauteur() == 0) {
            throw new AjoutSurCaseVideException();
        }

        if (source.getCouleur() != destination.getCouleur()) {
            destination.changerCouleur();
        }
        destination.augmenterHauteur(source.getHauteur());
        source.viderCase();

        typeMouvement = TYPE_MVT.getValue(mouvementX, mouvementY);
    }

    public Coup(int srcX, int srcY, TYPE_MVT mvt, Terrain terrain) throws CasesNonAdjacentesException,
                                                                  CaseSourceVideException, AjoutSurCaseVideException,
                                                                  TourTropGrandeException, HorsDuTerrainException {
        this(srcX, srcY, srcX + mvt.getX(), srcY + mvt.getY(), terrain);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Coup)) {
            return false;
        }

        final Coup coup = (Coup) o;
        return coup.source == source && coup.destination == destination && coup.sourceCoord == sourceCoord
            && coup.destCoord == destCoord && coup.precedenteSource == precedenteSource
            && coup.precedenteDest == precedenteDest;
    }

    /* Getters et setters */

    public Point getDestCoord() {
        return destCoord;
    }

    public Case getDestination() {
        return destination;
    }

    public Case getPrecedenteSource() {
        return precedenteSource;
    }

    public Case getPrescedenteDest() {
        return precedenteDest;
    }

    public Case getSource() {
        return source;
    }

    public Point getSourceCoord() {
        return sourceCoord;
    }

    public TYPE_MVT getTypeMouvement() {
        return typeMouvement;
    }

    public void setDestination(Case destination) {
        this.destination = destination;
    }

    public void setSource(Case source) {
        this.source = source;
    }

    public void setSourceCoord(Point sourceCoord) {
        this.sourceCoord = sourceCoord;
    }

    @Override
    public String toString() {
        return "Coup [source=" + source + ", destination=" + destination + ",\nprecedenteSource=" + precedenteSource
            + ", precedenteDest=" + precedenteDest + ",\nsourceCoord=" + sourceCoord + ", destCoord=" + destCoord + "]";
    }
}
