package client.window;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.ChargeurDeRessources;
import client.MoteurLeger;
import common.TypePartie;

public class PanelControle extends JPanel {

    private static final long serialVersionUID = 8108646327524674314L;

    private final JButton     btnAnnuler;
    private final JButton     btnRefaire;
    private final JButton     btnAide;

    private boolean           etatConnexion    = false;
    private final JLabel      lblEtatConnexion;
    private final JPanel      panelConnexion;

    private final MoteurLeger moteur;

    /**
     * Create the panel.
     */
    public PanelControle(MoteurLeger m) {
        setMinimumSize(new Dimension(1, 1));
        moteur = m;

        setLayout(new BorderLayout(0, 0));

        final JPanel panelHaut = new JPanel();
        add(panelHaut, BorderLayout.CENTER);
        panelHaut.setLayout(new BoxLayout(panelHaut, BoxLayout.X_AXIS));

        final Component horizontalGlue = Box.createHorizontalGlue();
        panelHaut.add(horizontalGlue);

        btnAnnuler = new JButton();
        panelHaut.add(btnAnnuler);
        btnAnnuler.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                new Thread(new Runnable() {
                    
                    @Override
                    public void run() {
                        moteur.annulerCoup();
                    }
                }).start();
            }
        });

        final Component horizontalStrut_1 = Box.createHorizontalStrut(20);
        panelHaut.add(horizontalStrut_1);

        btnAide = new JButton();
        panelHaut.add(btnAide);
        btnAide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moteur.conseillerCoup();
            }
        });

        final Component horizontalStrut = Box.createHorizontalStrut(20);
        panelHaut.add(horizontalStrut);

        btnRefaire = new JButton();
        panelHaut.add(btnRefaire);
        btnRefaire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        moteur.refaireCoup();
                    }
                }).start();
            }
        });

        final Component horizontalGlue_1 = Box.createHorizontalGlue();
        panelHaut.add(horizontalGlue_1);

        etatConnexion = false;
        lblEtatConnexion = new JLabel(ChargeurDeRessources.getServerStatus()[etatConnexion ? 1 : 0]);
        panelConnexion = new JPanel();
        panelConnexion.setLayout(new BoxLayout(panelConnexion, BoxLayout.X_AXIS));
        panelConnexion.add(Box.createHorizontalGlue());
        panelConnexion.add(lblEtatConnexion);

        //  panelHaut.setBackground(Color.black);
        //  panelConnexion.setBackground(Color.black);

        redessiner();

        activerAnnuler(false);
        activerRefaire(false);
    }

    public void activerConseil(boolean value) {
        btnAide.setEnabled(value);
    }

    public void activerAnnuler(boolean value) {
        btnAnnuler.setEnabled(value);
    }

    public void activerRefaire(boolean value) {
        btnRefaire.setEnabled(value);
    }

    private void generateImageButton(JButton button, ImageIcon[] imgButton) {
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setFocusPainted(false);

        button.setDisabledIcon(imgButton[0]);
        button.setIcon(imgButton[1]);
        button.setPressedIcon(imgButton[2]);
        button.setRolloverIcon(imgButton[3]);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(ChargeurDeRessources.getControlPanel(), 0, 0, null);
    }

    private void redessiner() {
        generateImageButton(btnAnnuler, ChargeurDeRessources.getAnnuler());
        generateImageButton(btnAide, ChargeurDeRessources.getAide());
        generateImageButton(btnRefaire, ChargeurDeRessources.getRefaire());

        setNetworkStatus(etatConnexion);
    }

    public void setMode(TypePartie type) {
        /*
         * TODO TypePartie.LOCAL: afficher panel timeline TypePartie.DISTANT: afficher panel Ã©tat connexion
         */
    }

    public void setNetworkStatus(boolean value) {
        etatConnexion = value;
        lblEtatConnexion.setIcon(ChargeurDeRessources.getServerStatus()[etatConnexion ? 1 : 0]);
    }
}
