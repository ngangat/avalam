package client.window;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import server.AvalamServeur;

public class RejoindrePartieEnLigne extends JDialog {

    public enum Mode {
        Depart,
        AttenteConnexionServeur,
        AttenteValidationNom,
        NomInvalide
    }

    private static final long serialVersionUID = 4811539040913238807L;

    private final JPanel     contentPane;
    private final JTextField txtJoueur;
    private final JTextField txtHote;
    private final JSpinner   spinnerPort;
    protected int            port;

    private final JButton    btnValider;
    private Mode             mode;
    private final JButton    btnAnnuler;

    /**
     * Create the frame.
     * @throws IOException 
     */

    public RejoindrePartieEnLigne() {
        mode = Mode.Depart;
        setAlwaysOnTop(true);
        setModal(true);
        setTitle("Rejoindre une partie en ligne");
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 520, 230);
        setLocationRelativeTo(FenetrePrincipale.getInstance());
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        final GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 };
        gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 4 };
        gbl_contentPane.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0,
            Double.MIN_VALUE };
        gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        contentPane.setLayout(gbl_contentPane);

        final JLabel lblJoueur = new JLabel("Joueur");
        lblJoueur.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
        lblJoueur.setHorizontalAlignment(SwingConstants.CENTER);
        final GridBagConstraints gbc_lblJoueur = new GridBagConstraints();
        gbc_lblJoueur.weighty = 1.0;
        gbc_lblJoueur.anchor = GridBagConstraints.NORTH;
        gbc_lblJoueur.gridheight = 2;
        gbc_lblJoueur.gridwidth = 5;
        gbc_lblJoueur.insets = new Insets(0, 0, 5, 5);
        gbc_lblJoueur.gridx = 0;
        gbc_lblJoueur.gridy = 0;
        contentPane.add(lblJoueur, gbc_lblJoueur);

        final JSeparator separator = new JSeparator();
        separator.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator = new GridBagConstraints();
        gbc_separator.fill = GridBagConstraints.VERTICAL;
        gbc_separator.gridheight = 8;
        gbc_separator.insets = new Insets(20, 0, 30, 5);
        gbc_separator.gridx = 5;
        gbc_separator.gridy = 0;
        contentPane.add(separator, gbc_separator);

        final JLabel lblServeur = new JLabel("Serveur");
        lblServeur.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
        lblServeur.setHorizontalAlignment(SwingConstants.CENTER);
        final GridBagConstraints gbc_lblServeur = new GridBagConstraints();
        gbc_lblServeur.anchor = GridBagConstraints.NORTH;
        gbc_lblServeur.gridheight = 2;
        gbc_lblServeur.gridwidth = 5;
        gbc_lblServeur.insets = new Insets(0, 0, 5, 0);
        gbc_lblServeur.gridx = 6;
        gbc_lblServeur.gridy = 0;
        contentPane.add(lblServeur, gbc_lblServeur);

        final JLabel lblNom = new JLabel("Nom");
        lblNom.setHorizontalAlignment(SwingConstants.LEFT);
        final GridBagConstraints gbc_lblNom = new GridBagConstraints();
        gbc_lblNom.fill = GridBagConstraints.HORIZONTAL;
        gbc_lblNom.anchor = GridBagConstraints.WEST;
        gbc_lblNom.insets = new Insets(0, 0, 5, 5);
        gbc_lblNom.gridx = 1;
        gbc_lblNom.gridy = 2;
        contentPane.add(lblNom, gbc_lblNom);

        txtJoueur = new JTextField();
        txtJoueur.setHorizontalAlignment(SwingConstants.LEFT);
        txtJoueur.setText("Joueur1");
        final GridBagConstraints gbc_txtJoueur = new GridBagConstraints();
        gbc_txtJoueur.anchor = GridBagConstraints.WEST;
        gbc_txtJoueur.gridwidth = 2;
        gbc_txtJoueur.insets = new Insets(0, 0, 5, 5);
        gbc_txtJoueur.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtJoueur.gridx = 2;
        gbc_txtJoueur.gridy = 2;
        contentPane.add(txtJoueur, gbc_txtJoueur);
        txtJoueur.setColumns(15);

        final JLabel lblAdresse = new JLabel("Adresse");
        lblAdresse.setHorizontalAlignment(SwingConstants.LEFT);
        final GridBagConstraints gbc_lblAdresse = new GridBagConstraints();
        gbc_lblAdresse.fill = GridBagConstraints.HORIZONTAL;
        gbc_lblAdresse.anchor = GridBagConstraints.EAST;
        gbc_lblAdresse.insets = new Insets(0, 0, 5, 5);
        gbc_lblAdresse.gridx = 7;
        gbc_lblAdresse.gridy = 2;
        contentPane.add(lblAdresse, gbc_lblAdresse);

        txtHote = new JTextField();
        txtHote.setHorizontalAlignment(SwingConstants.LEFT);
        txtHote.setText("localhost");
        final GridBagConstraints gbc_txtHote = new GridBagConstraints();
        gbc_txtHote.anchor = GridBagConstraints.WEST;
        gbc_txtHote.fill = GridBagConstraints.HORIZONTAL;
        gbc_txtHote.gridwidth = 2;
        gbc_txtHote.insets = new Insets(0, 0, 5, 5);
        gbc_txtHote.gridx = 8;
        gbc_txtHote.gridy = 2;
        contentPane.add(txtHote, gbc_txtHote);
        txtHote.setColumns(10);

        final JLabel lblPort = new JLabel("Port");
        lblPort.setHorizontalAlignment(SwingConstants.LEFT);
        final GridBagConstraints gbc_lblPort = new GridBagConstraints();
        gbc_lblPort.anchor = GridBagConstraints.EAST;
        gbc_lblPort.insets = new Insets(0, 0, 5, 5);
        gbc_lblPort.gridx = 7;
        gbc_lblPort.gridy = 3;
        contentPane.add(lblPort, gbc_lblPort);

        port = AvalamServeur.portParDefaut;
        spinnerPort = new JSpinner();
        spinnerPort.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                port = (int) spinnerPort.getValue();
            }
        });
        spinnerPort.setModel(new SpinnerNumberModel(port, 1025, 65534, 1));
        final GridBagConstraints gbc_spinnerPort = new GridBagConstraints();
        gbc_spinnerPort.gridwidth = 2;
        gbc_spinnerPort.anchor = GridBagConstraints.WEST;
        gbc_spinnerPort.insets = new Insets(0, 0, 5, 5);
        gbc_spinnerPort.gridx = 8;
        gbc_spinnerPort.gridy = 3;
        contentPane.add(spinnerPort, gbc_spinnerPort);

        btnAnnuler = new JButton("Annuler");
        btnAnnuler.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAffichage(Mode.Depart);
                dispose();
            }
        });
        final GridBagConstraints gbc_btnAnnuler = new GridBagConstraints();
        gbc_btnAnnuler.gridheight = 2;
        gbc_btnAnnuler.anchor = GridBagConstraints.SOUTHEAST;
        gbc_btnAnnuler.gridwidth = 4;
        gbc_btnAnnuler.insets = new Insets(0, 0, 0, 5);
        gbc_btnAnnuler.gridx = 0;
        gbc_btnAnnuler.gridy = 6;
        contentPane.add(btnAnnuler, gbc_btnAnnuler);

        btnValider = new JButton("Jouer");
        btnValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (mode) {
                    case Depart:
                        traitementDepart();
                        break;
                    case NomInvalide:
                        traitementNomInvalide();
                        break;
                    default:
                        break;
                }
            }

            private void traitementDepart() {
                setAffichage(Mode.AttenteConnexionServeur);
                final String j1 = txtJoueur.getText().trim();
                final String hote = txtHote.getText().trim();
                if (verifierNotifierChamp(j1, hote, port)) {
                    try {
                        FenetrePrincipale.getInstance().getMoteur().nouvellePartieDistante(hote, port, j1);
                        setAffichage(Mode.AttenteValidationNom);
                    } catch (final IOException e2) {
                        e2.printStackTrace();
                        FenetrePrincipale.getInstance().ajouterMessageNotification("Connexion impossible",
                                                                                   "VÃ©rifier l'adresse et le port.");
                        setAffichage(Mode.Depart);
                    }
                } else {
                    setAffichage(Mode.Depart);
                }
            }

            private void traitementNomInvalide() {
                setAffichage(Mode.AttenteValidationNom);
                final String j1 = txtJoueur.getText().trim();
                if (verifierNom(j1)) {
                    FenetrePrincipale.getInstance().getMoteur().envoyerNom(j1);
                } else {
                    setAffichage(Mode.NomInvalide);
                }
            }
        });
        final GridBagConstraints gbc_btnValider = new GridBagConstraints();
        gbc_btnValider.weighty = 1.0;
        gbc_btnValider.gridheight = 2;
        gbc_btnValider.anchor = GridBagConstraints.SOUTHWEST;
        gbc_btnValider.gridwidth = 4;
        gbc_btnValider.insets = new Insets(0, 0, 0, 5);
        gbc_btnValider.gridx = 7;
        gbc_btnValider.gridy = 6;
        contentPane.add(btnValider, gbc_btnValider);
    }

    public void setAffichage(Mode mode) {
        switch (mode) {
            case Depart:
                this.mode = Mode.Depart;
                setAffichageDepart();
                break;
            case AttenteConnexionServeur:
                this.mode = Mode.AttenteConnexionServeur;
                setAffichageAttenteConnexion();
                break;
            case AttenteValidationNom:
                this.mode = Mode.AttenteValidationNom;
                setAffichageAttenteValidationNom();
                break;
            case NomInvalide:
                this.mode = Mode.NomInvalide;
                setAffichageNomInvalide();
                break;
            default:
                break;
        }
    }

    private void setAffichageAttenteConnexion() {
        final ImageIcon wait = new ImageIcon("themes/default/wait.gif");
        btnValider.setIcon(wait);
        txtHote.setEnabled(false);
        txtJoueur.setEnabled(false);
        spinnerPort.setEnabled(false);
        btnValider.setEnabled(false);
        btnAnnuler.setEnabled(true);
    }

    private void setAffichageAttenteValidationNom() {
        final ImageIcon wait = new ImageIcon("themes/default/wait.gif");
        btnValider.setIcon(wait);
        txtHote.setEnabled(false);
        txtJoueur.setEnabled(false);
        spinnerPort.setEnabled(false);
        btnValider.setEnabled(false);
        btnAnnuler.setEnabled(false);
    }

    private void setAffichageDepart() {
        btnValider.setIcon(null);
        txtHote.setEnabled(true);
        txtJoueur.setEnabled(true);
        spinnerPort.setEnabled(true);
        btnValider.setEnabled(true);
        btnAnnuler.setEnabled(true);
    }

    private void setAffichageNomInvalide() {
        btnValider.setIcon(null);
        txtHote.setEnabled(false);
        txtJoueur.setEnabled(true);
        spinnerPort.setEnabled(false);
        btnValider.setEnabled(true);
        btnAnnuler.setEnabled(false);
    }

    public void setModeNomInvalide() {
        txtHote.setEnabled(false);
        txtJoueur.setEnabled(true);
        spinnerPort.setEnabled(false);
        btnValider.setEnabled(true);
    }

    private boolean verifierInfosServeur(final String hote) {
        if (hote.isEmpty()) {
            FenetrePrincipale.getInstance().ajouterMessageNotification("Adresse serveur incorrecte",
                                                                       "L'adresse du serveur ne doit pas Ãªtre vide");
            return false;
        }
        return true;
    }

    private boolean verifierNom(final String j1) {
        if (j1.isEmpty() || j1.length() < 1 || j1.length() > 15) {
            FenetrePrincipale.getInstance().ajouterMessageNotification("Nom incorrecte",
                                                                       "Votre nom doit avoir une taille compris entre "
                                                                           + 1 + " et " + 15);
            return false;
        }
        return true;
    }

    protected boolean verifierNotifierChamp(final String j1, final String hote, final int port) {
        return verifierNom(j1) && verifierInfosServeur(hote);
    }

}
