package client.window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.jachievement.Achievement;
import org.jachievement.AchievementConfig;
import org.jachievement.AchievementPosition;
import org.jachievement.AchievementQueue;

import client.ChargeurDeRessources;
import client.MoteurLeger;
import client.ReglesEtManuel;

import common.Joueur;
import common.Terrain;
import common.TypeJoueur;

public class FenetrePrincipale extends JFrame implements Observer {

    private static final long        serialVersionUID = 750152623439143451L;

    private static FenetrePrincipale instance;                              // permet de simplifier le
                                                                             // positionnement

    public static FenetrePrincipale getInstance() {
        if (FenetrePrincipale.instance == null) {
            FenetrePrincipale.instance = new FenetrePrincipale();
        }

        return FenetrePrincipale.instance;
    }

    private MoteurLeger             moteur;
    private final JPanel            contentPane;

//    private final JMenuItem         mntmEnregistrer;
    private final JMenu             mnAction;
    private final AchievementConfig notificationConfig;
    private final AchievementQueue  listeNotification;
    private final JPanel            panelPlateau;
    private final JPanel            panelControle;
    private PanelControle           realPanelControle;
    private final JPanel            panelJoueurBas;
    private final JPanel            panelJoueurHaut;
    private final JLabel            lblInfos;
    private final JCheckBoxMenuItem chckbxmntmAideVisuelle;
    private final JCheckBoxMenuItem chckbxmntmAutorisationDuConseil;
//    private final JCheckBoxMenuItem chckbxmntmMusique;
    private final JMenu             mnTheme;

    private final JMenuItem         mntmAnnuler;

    private final JMenuItem         mntmRefaire;

    private RejoindrePartieEnLigne  fenetreRejoindrePartieEnLigne;

    private String                  txt;

    private Color                   color;

    /**
     * Create the frame.
     */
    private FenetrePrincipale() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                System.exit(0);
            }
        });
        notificationConfig = new AchievementConfig();
        notificationConfig.setTitleColor(Color.BLUE);
        notificationConfig.setAchievementPosition(AchievementPosition.TOP_CENTER);
        listeNotification = new AchievementQueue();

        setResizable(false);
        setTitle("Avalam");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        final JMenu mnPartie = new JMenu("Partie");
        menuBar.add(mnPartie);

        final JMenuItem mntmPartieRapide = new JMenuItem("Jouer une partie rapide");
        mntmPartieRapide.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
        mntmPartieRapide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Clic sur partie rapide");

                FenetrePrincipale.getInstance().getMoteur()
                                 .nouvellePartieLocal("Joueur 1", "Wall-E", true, TypeJoueur.Facile);
                FenetrePrincipale.getInstance().getMoteur().getAvalamClient().setPanels();
                FenetrePrincipale.getInstance().revalidate();
            }
        });
        mnPartie.add(mntmPartieRapide);

        final JMenuItem mntmNouvellePartie = new JMenuItem("Jouer une nouvelle partie");
        mntmNouvellePartie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Clic sur nouvelle partie");
                try {
                    final JouerNouvellePartie nouvPartie = new JouerNouvellePartie();
                    nouvPartie.setLocationRelativeTo(FenetrePrincipale.getInstance());
                    nouvPartie.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                    nouvPartie.setVisible(true);
                } catch (final Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        mnPartie.add(mntmNouvellePartie);
/*
        final JMenuItem mntmCharger = new JMenuItem("Reprendre une partie");
        mntmCharger.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
        mntmCharger.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser dialogue = new JFileChooser();
                dialogue.showOpenDialog(FenetrePrincipale.getInstance());
                System.out.println("Fichier : " + dialogue.getSelectedFile());
                // Chargement du fichier
            }
        });
        mnPartie.add(mntmCharger);
*/
        final JMenuItem mntmNewMenuItem = new JMenuItem("Rejoindre une partie en ligne");
        mntmNewMenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    fenetreRejoindrePartieEnLigne = new RejoindrePartieEnLigne();
                    fenetreRejoindrePartieEnLigne.setLocationRelativeTo(FenetrePrincipale.getInstance());
                    fenetreRejoindrePartieEnLigne.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                    fenetreRejoindrePartieEnLigne.setVisible(true);
                } catch (final Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        mnPartie.add(mntmNewMenuItem);
/*
        final JSeparator separator = new JSeparator();
        mnPartie.add(separator);

        mntmEnregistrer = new JMenuItem("Enregistrer la partie");
        mntmEnregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        mntmEnregistrer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Enregistrement

                // griser bouton

            }
        });
        mnPartie.add(mntmEnregistrer);

        final JMenuItem mntmEnregistrerSous = new JMenuItem("Enregistrer la partie sous...");
        mntmEnregistrerSous.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser dialogue = new JFileChooser();
                dialogue.showOpenDialog(FenetrePrincipale.getInstance());
                System.out.println("Fichier : " + dialogue.getSelectedFile());
                // Enregistrement du fichier
            }
        });
        mnPartie.add(mntmEnregistrerSous);
*/
        final JSeparator separator_1 = new JSeparator();
        mnPartie.add(separator_1);

        final JMenuItem mntmQuitter = new JMenuItem("Quitter");
        mntmQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
        mntmQuitter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // preparer les element pour la fermeture

                // demander s'il veut sauvegarder avant (si en local)

                // quitter
                System.exit(0);
            }
        });
        mnPartie.add(mntmQuitter);

        mnAction = new JMenu("Action");
        menuBar.add(mnAction);

        mntmAnnuler = new JMenuItem("Annuler");
        mntmAnnuler.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                new Thread(new Runnable() {
                    
                    @Override
                    public void run() {
                        moteur.annulerCoup();
                    }
                }).start();
            }
        });
        mntmAnnuler.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK));
        mntmAnnuler.setEnabled(false);
        mnAction.add(mntmAnnuler);

        mntmRefaire = new JMenuItem("Refaire");
        mntmRefaire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    
                    @Override
                    public void run() {
                        moteur.refaireCoup();
                    }
                }).start();
            }
        });
        mntmRefaire.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, InputEvent.CTRL_MASK));
        mntmRefaire.setEnabled(false);
        mnAction.add(mntmRefaire);

        final JMenu mnOptions = new JMenu("Options");
        menuBar.add(mnOptions);

        chckbxmntmAideVisuelle = new JCheckBoxMenuItem("Aides visuelles");
        chckbxmntmAideVisuelle.setSelected(true);
        mnOptions.add(chckbxmntmAideVisuelle);

        chckbxmntmAutorisationDuConseil = new JCheckBoxMenuItem("Autorisation du conseil");
        chckbxmntmAutorisationDuConseil.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getPanelControle().activerConseil(chckbxmntmAutorisationDuConseil.isSelected());
            }
        });
        chckbxmntmAutorisationDuConseil.setSelected(true);
        mnOptions.add(chckbxmntmAutorisationDuConseil);

        final JSeparator separator_2 = new JSeparator();
        mnOptions.add(separator_2);

        mnTheme = new JMenu("Thème");
        mnOptions.add(mnTheme);
/*
        chckbxmntmMusique = new JCheckBoxMenuItem("Musique");
        chckbxmntmMusique.setSelected(true);
        mnOptions.add(chckbxmntmMusique);

        final JMenu mnStatistiques = new JMenu("Statistiques");
        menuBar.add(mnStatistiques);
*/
        final JMenu mnAide = new JMenu("Aide");
        mnAide.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        menuBar.add(mnAide);

        final JMenuItem mntmReglesDuJeu = new JMenuItem("Règles du jeu");
        mntmReglesDuJeu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                new ReglesEtManuel(1);
            }
        });
        mnAide.add(mntmReglesDuJeu);

        final JMenuItem mntmManuelDutilisation = new JMenuItem("Manuel d'utilisation");
        mntmManuelDutilisation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                new ReglesEtManuel(2);
            }
        });
        mnAide.add(mntmManuelDutilisation);
/*
        final JMenuItem mntmAPropos = new JMenuItem("A propos");
        mnAide.add(mntmAPropos);
*/
        contentPane = new JPanel();
        contentPane.setBorder(null);
        setContentPane(contentPane);
        final GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[] { 504, 194 };
        gbl_contentPane.rowHeights = new int[] { 26, 504, 70 };
        gbl_contentPane.columnWeights = new double[] { 0.0, 1.0 };
        gbl_contentPane.rowWeights = new double[] { 0.0, 1.0, 1.0 };
        contentPane.setLayout(gbl_contentPane);

        final JPanel panel = new JPanel();
        panel.setBackground(Color.BLACK);
        final GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.gridwidth = 2;
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 0;
        contentPane.add(panel, gbc_panel);
        panel.setLayout(new BorderLayout(0, 0));

        lblInfos = new JLabel("");
        lblInfos.setForeground(Color.WHITE);
        panel.add(lblInfos);

        panelPlateau = new JPanel();
        panelPlateau.setBorder(null);
        panelPlateau.setPreferredSize(new Dimension(504, 504));
        panelPlateau.setSize(new Dimension(504, 504));
        final GridBagConstraints gbc_panelPlateau = new GridBagConstraints();
        gbc_panelPlateau.anchor = GridBagConstraints.NORTHWEST;
        gbc_panelPlateau.gridx = 0;
        gbc_panelPlateau.gridy = 1;
        contentPane.add(panelPlateau, gbc_panelPlateau);
        panelPlateau.setLayout(new BorderLayout(0, 0));

        final JPanel panelJoueurs = new JPanel();
        final GridBagConstraints gbc_panelJoueurs = new GridBagConstraints();
        gbc_panelJoueurs.fill = GridBagConstraints.BOTH;
        gbc_panelJoueurs.gridx = 1;
        gbc_panelJoueurs.gridy = 1;
        contentPane.add(panelJoueurs, gbc_panelJoueurs);
        final GridBagLayout gbl_panelJoueurs = new GridBagLayout();
        gbl_panelJoueurs.columnWidths = new int[] { 194 };
        gbl_panelJoueurs.rowHeights = new int[] { 252, 252 };
        gbl_panelJoueurs.columnWeights = new double[] { 1.0 };
        gbl_panelJoueurs.rowWeights = new double[] { 1.0, 1.0 };
        panelJoueurs.setLayout(gbl_panelJoueurs);

        panelJoueurHaut = new JPanel();
        final GridBagConstraints gbc_panelJoueurHaut = new GridBagConstraints();
        gbc_panelJoueurHaut.fill = GridBagConstraints.BOTH;
        gbc_panelJoueurHaut.gridx = 0;
        gbc_panelJoueurHaut.gridy = 0;
        panelJoueurs.add(panelJoueurHaut, gbc_panelJoueurHaut);
        panelJoueurHaut.setLayout(new BorderLayout(0, 0));

        panelJoueurBas = new JPanel();
        panelJoueurBas.setBackground(Color.BLACK);
        final GridBagConstraints gbc_panelJoueurBas = new GridBagConstraints();
        gbc_panelJoueurBas.fill = GridBagConstraints.BOTH;
        gbc_panelJoueurBas.gridx = 0;
        gbc_panelJoueurBas.gridy = 1;
        panelJoueurs.add(panelJoueurBas, gbc_panelJoueurBas);
        panelJoueurBas.setLayout(new BorderLayout(0, 0));

        panelControle = new JPanel();
        final GridBagConstraints gbc_panelControle = new GridBagConstraints();
        gbc_panelControle.gridwidth = 2;
        gbc_panelControle.fill = GridBagConstraints.BOTH;
        gbc_panelControle.gridx = 0;
        gbc_panelControle.gridy = 2;
        contentPane.add(panelControle, gbc_panelControle);
        panelControle.setLayout(new BoxLayout(panelControle, BoxLayout.X_AXIS));

        setResizable(false);

        fillThemeMenu();
    }

    public boolean aideVisuellesActivees() {
        return chckbxmntmAideVisuelle.isSelected();
    }

    public void ajouterMessageNotification(String titre, String message) {
        final Achievement achievement = new Achievement(titre, message, notificationConfig);
        listeNotification.add(achievement);
    }

    public boolean conseilAutorise() {
        return chckbxmntmAutorisationDuConseil.isSelected();
    }

    private void fillThemeMenu() {
        final ArrayList<String> themes = ChargeurDeRessources.listThemes();
        final ButtonGroup btngrpThemes = new ButtonGroup();
        Collections.sort(themes);
        /*
         * TODO: sort themes with 'default' on top
         */

        final ActionListener actionSelectionTheme = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // ChargeurDeRessources.chargerRessources(((JRadioButtonMenuItem)e.getSource()).getText());
            }
        };

        for (final String theme : themes) {
            final JRadioButtonMenuItem mntmTheme = new JRadioButtonMenuItem(theme);
            mntmTheme.addActionListener(actionSelectionTheme);
            if (theme.equals(ChargeurDeRessources.getThemeName())) {
                mntmTheme.setSelected(true);
            }
            mnTheme.add(mntmTheme);
            btngrpThemes.add(mntmTheme);
        }
    }

    public synchronized RejoindrePartieEnLigne getFenetreRejoindrePartieEnLigne() {
        return fenetreRejoindrePartieEnLigne;
    }

    public AchievementQueue getListeNotification() {
        return listeNotification;
    }

    public MoteurLeger getMoteur() {
        return moteur;
    }

    public void setMessage(String texte, Color couleur) {
        lblInfos.setForeground(couleur);
        lblInfos.setText(texte);
    }

    public void setMoteur(MoteurLeger moteur) {
        if (this.moteur == null) {
            this.moteur = moteur;
        }
    }

    public void setPanelControle(JPanel panelControle) {
        this.panelControle.removeAll();
        this.panelControle.add(panelControle);
        this.panelControle.setMinimumSize(new Dimension(5, 5));
        this.panelControle.setMaximumSize(new Dimension(5, 5));
        realPanelControle = (PanelControle) panelControle;
    }

    public PanelControle getPanelControle() {
        return realPanelControle;
    }

    public void setPanelJoueurBas(JPanel panelJoueurBas) {
        this.panelJoueurBas.removeAll();
        this.panelJoueurBas.add(panelJoueurBas);
    }

    public void setPanelJoueurHaut(JPanel panelJoueurHaut) {
        this.panelJoueurHaut.removeAll();
        this.panelJoueurHaut.add(panelJoueurHaut);
    }

    public void setPanelPlateau(JPanel panelPlateau) {
        this.panelPlateau.removeAll();
        this.panelPlateau.add(panelPlateau);
    }

    @Override
    public void update(Observable o, Object arg) {

        if (moteur.getInfosPartie().getTerrain().finPartie()) {
            final Terrain terrain = moteur.getInfosPartie().getTerrain();
            final Joueur joueurs[] = moteur.getInfosPartie().getJoueurs();
            final Joueur j1 = joueurs[0];
            final Joueur j2 = joueurs[1];

            if (terrain.getNbTours(j1.getCouleur()) > terrain.getNbTours(j2.getCouleur())) {
                    txt = j1.getNom() + " a gagné!";
                color = Color.yellow;
            } else if (terrain.getNbTours(j1.getCouleur()) < terrain.getNbTours(j2.getCouleur())) {
                    txt = j2.getNom() + " a gagné!";
                color = Color.cyan;
            } else {
                txt = "Match nul.";
                color = Color.red;
            }

        } else {
            Joueur j = moteur.getInfosPartie().getJoueurCourant();
            if(!j.getTypeJoueur().equals(TypeJoueur.Humain))
            txt = j.getNom() + " réfléchit...";
            else {
                txt = "";
            }
            color = Color.white;
        }
        setMessage(txt, color);
        lblInfos.setHorizontalAlignment(SwingConstants.CENTER);
        lblInfos.setFont(new Font("Arial Black", Font.PLAIN, 15));
    }

    public void effacerMessageGagnant() {
        txt = "";
        color = Color.white;
        setMessage(txt, color);
    }

    public void activerAnnuler(boolean etat) {
        mntmAnnuler.setEnabled(etat);
    }

    public void activerRefaire(boolean etat) {
        mntmRefaire.setEnabled(etat);
    }

    public boolean isConseilPossible() {
        return chckbxmntmAutorisationDuConseil.isSelected();
    }
}
