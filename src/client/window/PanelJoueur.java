package client.window;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import common.CouleurJoueur;
import common.InfosPartie;
import common.Joueur;
import common.Terrain;
import common.TypeJoueur;

public class PanelJoueur extends JPanel implements Observer {
    private static final long  serialVersionUID = -9112493098317941141L;
    ImageIcon                  imageBg;
    JLabel                     labelI;
    JLabel                     labelJoueur;
    JLabel                     label;
    JLabel                     lblfacile;
    InfosPartie                info;
    Terrain                    terrain;
    private final JLabel       lblScore;
    private final JLayeredPane layeredPane;
    private final boolean      estBlanc;
    private TypeJoueur         type;

    public PanelJoueur(InfosPartie i, boolean estBlanc, Terrain t) {

        info = i;
        this.estBlanc = estBlanc;
        terrain = t;
        final Joueur j = i.getJoueurs()[estBlanc ? 0 : 1];
        if (j != null)
            type = j.getTypeJoueur();
        setAlignmentY(Component.TOP_ALIGNMENT);
        setAlignmentX(Component.LEFT_ALIGNMENT);
        setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        setOpaque(false);
        setMinimumSize(new Dimension(194, 252));
        setMaximumSize(new Dimension(194, 252));
        setSize(194, 252);
        int niveau = 1;
        switch (type) {
            case Facile:
                niveau = 1;
                break;
            case Moyen:
                niveau = 2;
                break;
            case Difficile:
                niveau = 3;
                break;
            case Humain:
                niveau = 0;
                break;
        }
        String image = "", labelNom = j.getNom(), labelNiveau = "";
        Color color = null;
        if (type == TypeJoueur.Humain && estBlanc) { // Humain et pion jaune
            image = "yellowHuman.png";
            //labelNom = j.getNom();
            labelNiveau = "";
            color = Color.yellow;
        } else if (type == TypeJoueur.Humain && !estBlanc) { // Humain et pion bleu
            image = "blueHuman.png";
            //labelNom = j.getNom();
            labelNiveau = "";
            color = Color.cyan;
        } else if (type != TypeJoueur.Humain && estBlanc) { // pion jaune

            image = "yellowRobot" + niveau + ".png";
            //labelNom = "Ordinateur";
            labelNiveau = "Ordinateur (" + j.getTypeJoueur() + ")";
            color = Color.yellow;

        } else if (type != TypeJoueur.Humain && !estBlanc) { // pion bleu
            image = "blueRobot" + niveau + ".png";
            //labelNom = "Ordinateur";
            labelNiveau = "Ordinateur (" + j.getTypeJoueur() + ")";
            color = Color.cyan;
        }
        imageBg = new ImageIcon("themes/default/" + image);
        setLayout(null);
        labelJoueur = new JLabel(labelNom);
        lblfacile = new JLabel(labelNiveau);
        lblfacile.setHorizontalAlignment(SwingConstants.CENTER);
        lblfacile.setFont(new Font("Arial Black", Font.PLAIN, 11));
        lblfacile.setForeground(color);
        labelI = new JLabel(imageBg);
        lblScore = new JLabel("Score : 24 tours");
        lblScore.setForeground(color);
        labelJoueur.setForeground(color);

        layeredPane = new JLayeredPane();
        layeredPane.setBounds(0, 0, 194, 252);
        add(layeredPane);

        labelJoueur.setVerticalTextPosition(SwingConstants.TOP);
        labelJoueur.setFont(new Font("Arial Black", Font.BOLD, 17));
        labelJoueur.setVerticalAlignment(SwingConstants.TOP);
        labelJoueur.setHorizontalTextPosition(SwingConstants.CENTER);
        labelJoueur.setHorizontalAlignment(SwingConstants.CENTER);

        labelI.setBounds(0, 0, 194, 252);
        add(labelI);

        lblScore.setVerticalAlignment(SwingConstants.TOP);
        lblScore.setAlignmentY(Component.TOP_ALIGNMENT);
        lblScore.setFont(new Font("Arial Black", Font.PLAIN, 12));
        lblScore.setHorizontalTextPosition(SwingConstants.LEADING);
        lblScore.setHorizontalAlignment(SwingConstants.CENTER);

        final GroupLayout gl_layeredPane = new GroupLayout(layeredPane);
        gl_layeredPane.setHorizontalGroup(gl_layeredPane.createParallelGroup(Alignment.LEADING)
                                                        .addGroup(gl_layeredPane.createSequentialGroup()
                                                                                .addGroup(gl_layeredPane.createParallelGroup(Alignment.LEADING,
                                                                                                                             false)
                                                                                                        .addComponent(lblScore,
                                                                                                                      Alignment.TRAILING,
                                                                                                                      GroupLayout.DEFAULT_SIZE,
                                                                                                                      194,
                                                                                                                      Short.MAX_VALUE)
                                                                                                        .addComponent(labelJoueur,
                                                                                                                      Alignment.TRAILING,
                                                                                                                      GroupLayout.DEFAULT_SIZE,
                                                                                                                      194,
                                                                                                                      Short.MAX_VALUE)
                                                                                                        .addComponent(lblfacile,
                                                                                                                      Alignment.TRAILING,
                                                                                                                      GroupLayout.DEFAULT_SIZE,
                                                                                                                      GroupLayout.DEFAULT_SIZE,
                                                                                                                      Short.MAX_VALUE))
                                                                                .addContainerGap()));
        gl_layeredPane.setVerticalGroup(gl_layeredPane.createParallelGroup(Alignment.LEADING)
                                                      .addGroup(gl_layeredPane.createSequentialGroup()
                                                                              .addGap(8)
                                                                              .addComponent(labelJoueur,
                                                                                            GroupLayout.PREFERRED_SIZE,
                                                                                            20,
                                                                                            GroupLayout.PREFERRED_SIZE)
                                                                              .addPreferredGap(ComponentPlacement.RELATED)
                                                                              .addComponent(lblfacile)
                                                                              .addGap(25)
                                                                              .addComponent(lblScore,
                                                                                            GroupLayout.PREFERRED_SIZE,
                                                                                            90,
                                                                                            GroupLayout.PREFERRED_SIZE)
                                                                              .addContainerGap(98, Short.MAX_VALUE)));
        layeredPane.setLayout(gl_layeredPane);
    }

    @Override
    public void update(Observable arg0, Object arg1) {
        ImageIcon image;
        int niveau = 1;
        switch (type) {
            case Facile:
                niveau = 1;
                break;
            case Moyen:
                niveau = 2;
                break;
            case Difficile:
                niveau = 3;
                break;
            case Humain:
                niveau = 0;
                break;
        }
        String icon = "";
        if (info.getCouleurJoueurCourant().equals(CouleurJoueur.BLANC) && estBlanc && type == TypeJoueur.Humain) {
            icon = "yellowHumanSelected.png";
        } else if (info.getCouleurJoueurCourant().equals(CouleurJoueur.NOIR) && !estBlanc && type == TypeJoueur.Humain) {
            icon = "blueHumanSelected.png";
        } else if (info.getCouleurJoueurCourant().equals(CouleurJoueur.BLANC) && estBlanc) {
            icon = "yellowRobot" + niveau + "Selected.png";
        } else if (info.getCouleurJoueurCourant().equals(CouleurJoueur.NOIR) && !estBlanc) {
            icon = "blueRobot" + niveau + "Selected.png";
        } else if (type == TypeJoueur.Humain && estBlanc) {// ce n'est pas le panel du joueur courant => remettre image du joueur
            icon = "yellowHuman.png";
        } else if (type == TypeJoueur.Humain && !estBlanc) {
            icon = "blueHuman.png";
        } else if (type != TypeJoueur.Humain && estBlanc) {
            icon = "yellowRobot" + niveau + ".png";
        } else if (type != TypeJoueur.Humain && !estBlanc) {
            icon = "blueRobot" + niveau + ".png";
        }

        image = new ImageIcon("themes/default/" + icon);
        labelI.setIcon(image);

        // changer nb tours
        CouleurJoueur couleurJoueur;
        if (estBlanc) {
            couleurJoueur = CouleurJoueur.BLANC;
        } else {
            couleurJoueur = CouleurJoueur.NOIR;
        }
        final int nbTour = terrain.getNbTours(couleurJoueur);
        lblScore.setText("Score : " + nbTour + " tours");

    }
}
