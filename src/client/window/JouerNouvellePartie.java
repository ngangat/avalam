package client.window;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import utils.RandomSingleton;

import common.TypeJoueur;

public class JouerNouvellePartie extends JDialog {

    /**
     * 
     */
    private static final long serialVersionUID = 1611253926547475230L;

    private final JPanel      contentPane;
    private final JTextField  textFieldNomJoueur1;
    private final JTextField  textFieldNomJoueur2;
    private final ButtonGroup buttonGroupTypeJ2     = new ButtonGroup();

    private final ButtonGroup btnGroupPremierJoueur = new ButtonGroup();
    private TypeJoueur        typeJ2;

    private boolean           j1Commence;

    /**
     * Create the frame.
     */
    public JouerNouvellePartie() {
        setAlwaysOnTop(true);
        setModal(true);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 475, 325);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        final GridBagLayout gbl_contentPane = new GridBagLayout();
        gbl_contentPane.columnWidths = new int[] { 0, 6, 0, 0 };
        gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0 };
        gbl_contentPane.columnWeights = new double[] { 1.0, 0.0, 0.0, Double.MIN_VALUE };
        gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
        contentPane.setLayout(gbl_contentPane);

        final JPanel panel = new JPanel();
        final GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.weighty = 0.5;
        gbc_panel.insets = new Insets(0, 0, 5, 5);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 0;
        contentPane.add(panel, gbc_panel);
        final GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[] { 32, 0, 25, 0, 0 };
        gbl_panel.rowHeights = new int[] { 0, 0, 0, 0 };
        gbl_panel.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
        gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
        panel.setLayout(gbl_panel);

        final JLabel lblJoueur1Titre = new JLabel("Joueur 1");
        lblJoueur1Titre.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
        final GridBagConstraints gbc_lblJoueur1Titre = new GridBagConstraints();
        gbc_lblJoueur1Titre.anchor = GridBagConstraints.NORTH;
        gbc_lblJoueur1Titre.gridheight = 2;
        gbc_lblJoueur1Titre.gridwidth = 4;
        gbc_lblJoueur1Titre.insets = new Insets(0, 0, 5, 0);
        gbc_lblJoueur1Titre.gridx = 0;
        gbc_lblJoueur1Titre.gridy = 0;
        panel.add(lblJoueur1Titre, gbc_lblJoueur1Titre);

        final JLabel lblNomJoueur1 = new JLabel("Nom");
        lblNomJoueur1.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_lblNomJoueur1 = new GridBagConstraints();
        gbc_lblNomJoueur1.insets = new Insets(0, 0, 0, 5);
        gbc_lblNomJoueur1.anchor = GridBagConstraints.WEST;
        gbc_lblNomJoueur1.gridx = 0;
        gbc_lblNomJoueur1.gridy = 2;
        panel.add(lblNomJoueur1, gbc_lblNomJoueur1);

        textFieldNomJoueur1 = new JTextField();
        textFieldNomJoueur1.setText("joueur1");
        final GridBagConstraints gbc_textFieldNomJoueur1 = new GridBagConstraints();
        gbc_textFieldNomJoueur1.anchor = GridBagConstraints.WEST;
        gbc_textFieldNomJoueur1.gridwidth = 3;
        gbc_textFieldNomJoueur1.gridx = 1;
        gbc_textFieldNomJoueur1.gridy = 2;
        panel.add(textFieldNomJoueur1, gbc_textFieldNomJoueur1);
        textFieldNomJoueur1.setColumns(10);

        final JSeparator separator = new JSeparator();
        separator.setOrientation(SwingConstants.VERTICAL);
        final GridBagConstraints gbc_separator = new GridBagConstraints();
        gbc_separator.fill = GridBagConstraints.VERTICAL;
        gbc_separator.insets = new Insets(0, 0, 5, 5);
        gbc_separator.gridx = 1;
        gbc_separator.gridy = 0;
        contentPane.add(separator, gbc_separator);

        final JPanel panel_1 = new JPanel();
        final GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.fill = GridBagConstraints.BOTH;
        gbc_panel_1.insets = new Insets(0, 0, 5, 0);
        gbc_panel_1.gridx = 2;
        gbc_panel_1.gridy = 0;
        contentPane.add(panel_1, gbc_panel_1);
        final GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 52, 0 };
        gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        gbl_panel_1.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
        gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
        panel_1.setLayout(gbl_panel_1);

        final JLabel lblJoueur2Titre = new JLabel("Joueur 2");
        lblJoueur2Titre.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
        final GridBagConstraints gbc_lblJoueur2Titre = new GridBagConstraints();
        gbc_lblJoueur2Titre.anchor = GridBagConstraints.NORTH;
        gbc_lblJoueur2Titre.gridheight = 2;
        gbc_lblJoueur2Titre.gridwidth = 4;
        gbc_lblJoueur2Titre.insets = new Insets(0, 0, 5, 0);
        gbc_lblJoueur2Titre.gridx = 0;
        gbc_lblJoueur2Titre.gridy = 0;
        panel_1.add(lblJoueur2Titre, gbc_lblJoueur2Titre);

        final JLabel lblType = new JLabel("Type");
        lblType.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_lblType = new GridBagConstraints();
        gbc_lblType.insets = new Insets(0, 0, 5, 5);
        gbc_lblType.gridx = 0;
        gbc_lblType.gridy = 2;
        panel_1.add(lblType, gbc_lblType);

        final JRadioButton rdbtnHumain = new JRadioButton("Humain");
        buttonGroupTypeJ2.add(rdbtnHumain);
        rdbtnHumain.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    textFieldNomJoueur2.setEditable(true);
                    textFieldNomJoueur2.setEnabled(true);
                    typeJ2 = TypeJoueur.Humain;
                } else {
                    textFieldNomJoueur2.setEditable(false);
                    textFieldNomJoueur2.setEnabled(false);
                }
            }
        });
        rdbtnHumain.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_rdbtnHumain = new GridBagConstraints();
        gbc_rdbtnHumain.anchor = GridBagConstraints.WEST;
        gbc_rdbtnHumain.gridwidth = 2;
        gbc_rdbtnHumain.insets = new Insets(0, 0, 5, 5);
        gbc_rdbtnHumain.gridx = 1;
        gbc_rdbtnHumain.gridy = 2;
        panel_1.add(rdbtnHumain, gbc_rdbtnHumain);

        textFieldNomJoueur2 = new JTextField();
        textFieldNomJoueur2.setText("joueur2");
        textFieldNomJoueur2.setEnabled(false);
        textFieldNomJoueur2.setEditable(false);
        final GridBagConstraints gbc_textFieldNomJoueur2 = new GridBagConstraints();
        gbc_textFieldNomJoueur2.anchor = GridBagConstraints.WEST;
        gbc_textFieldNomJoueur2.insets = new Insets(0, 0, 5, 0);
        gbc_textFieldNomJoueur2.gridx = 3;
        gbc_textFieldNomJoueur2.gridy = 2;
        panel_1.add(textFieldNomJoueur2, gbc_textFieldNomJoueur2);
        textFieldNomJoueur2.setColumns(10);

        final JRadioButton rdbtnFacile = new JRadioButton("Facile");
        rdbtnFacile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                typeJ2 = TypeJoueur.Facile;
            }
        });
        buttonGroupTypeJ2.add(rdbtnFacile);

        rdbtnFacile.setSelected(true);
        typeJ2 = TypeJoueur.Facile;

        rdbtnFacile.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_rdbtnFacile = new GridBagConstraints();
        gbc_rdbtnFacile.anchor = GridBagConstraints.WEST;
        gbc_rdbtnFacile.gridwidth = 2;
        gbc_rdbtnFacile.insets = new Insets(0, 0, 5, 5);
        gbc_rdbtnFacile.gridx = 1;
        gbc_rdbtnFacile.gridy = 3;
        panel_1.add(rdbtnFacile, gbc_rdbtnFacile);

        final JRadioButton rdbtnMoyen = new JRadioButton("Moyen");
        rdbtnMoyen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                typeJ2 = TypeJoueur.Moyen;
            }
        });
        buttonGroupTypeJ2.add(rdbtnMoyen);

        rdbtnMoyen.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_rdbtnMoyen = new GridBagConstraints();
        gbc_rdbtnMoyen.anchor = GridBagConstraints.WEST;
        gbc_rdbtnMoyen.gridwidth = 2;
        gbc_rdbtnMoyen.insets = new Insets(0, 0, 5, 5);
        gbc_rdbtnMoyen.gridx = 1;
        gbc_rdbtnMoyen.gridy = 4;
        panel_1.add(rdbtnMoyen, gbc_rdbtnMoyen);

        final JRadioButton rdbtnDifficile = new JRadioButton("Difficiie");
        rdbtnDifficile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                typeJ2 = TypeJoueur.Difficile;
            }
        });
        buttonGroupTypeJ2.add(rdbtnDifficile);

        rdbtnDifficile.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_rdbtnDifficile = new GridBagConstraints();
        gbc_rdbtnDifficile.anchor = GridBagConstraints.WEST;
        gbc_rdbtnDifficile.gridwidth = 2;
        gbc_rdbtnDifficile.insets = new Insets(0, 0, 0, 5);
        gbc_rdbtnDifficile.gridx = 1;
        gbc_rdbtnDifficile.gridy = 5;
        panel_1.add(rdbtnDifficile, gbc_rdbtnDifficile);

        final JPanel panel_3 = new JPanel();
        final GridBagConstraints gbc_panel_3 = new GridBagConstraints();
        gbc_panel_3.gridwidth = 3;
        gbc_panel_3.insets = new Insets(0, 0, 5, 0);
        gbc_panel_3.fill = GridBagConstraints.BOTH;
        gbc_panel_3.gridx = 0;
        gbc_panel_3.gridy = 1;
        contentPane.add(panel_3, gbc_panel_3);
        final GridBagLayout gbl_panel_3 = new GridBagLayout();
        gbl_panel_3.columnWidths = new int[] { 0, 0 };
        gbl_panel_3.rowHeights = new int[] { 0, 0, 0, 0 };
        gbl_panel_3.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
        gbl_panel_3.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
        panel_3.setLayout(gbl_panel_3);

        final JRadioButton rdbtnJ1commence = new JRadioButton("Joueur 1 commence");
        rdbtnJ1commence.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                j1Commence = true;
            }
        });
        rdbtnJ1commence.setSelected(true);
        j1Commence = true;
        rdbtnJ1commence.setActionCommand("j1");
        btnGroupPremierJoueur.add(rdbtnJ1commence);
        rdbtnJ1commence.setHorizontalAlignment(SwingConstants.LEFT);
        rdbtnJ1commence.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_rdbtnJ1commence = new GridBagConstraints();
        gbc_rdbtnJ1commence.anchor = GridBagConstraints.WEST;
        gbc_rdbtnJ1commence.insets = new Insets(0, 0, 5, 0);
        gbc_rdbtnJ1commence.gridx = 0;
        gbc_rdbtnJ1commence.gridy = 0;
        panel_3.add(rdbtnJ1commence, gbc_rdbtnJ1commence);

        final JRadioButton rdbtnJ2Commence = new JRadioButton("Joueur 2 commence");
        rdbtnJ2Commence.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                j1Commence = false;
            }
        });
        rdbtnJ2Commence.setActionCommand("j2");
        btnGroupPremierJoueur.add(rdbtnJ2Commence);
        rdbtnJ2Commence.setHorizontalAlignment(SwingConstants.LEFT);
        rdbtnJ2Commence.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_rdbtnJ2Commence = new GridBagConstraints();
        gbc_rdbtnJ2Commence.anchor = GridBagConstraints.WEST;
        gbc_rdbtnJ2Commence.insets = new Insets(0, 0, 5, 0);
        gbc_rdbtnJ2Commence.gridx = 0;
        gbc_rdbtnJ2Commence.gridy = 1;
        panel_3.add(rdbtnJ2Commence, gbc_rdbtnJ2Commence);

        final JRadioButton rdbtnAleatoire = new JRadioButton("Peu importe");
        rdbtnAleatoire.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                j1Commence = RandomSingleton.getInstance().nextBoolean();
            }
        });
        rdbtnAleatoire.setActionCommand("random");
        btnGroupPremierJoueur.add(rdbtnAleatoire);
        rdbtnAleatoire.setHorizontalAlignment(SwingConstants.LEFT);
        rdbtnAleatoire.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_rdbtnAleatoire = new GridBagConstraints();
        gbc_rdbtnAleatoire.anchor = GridBagConstraints.WEST;
        gbc_rdbtnAleatoire.gridx = 0;
        gbc_rdbtnAleatoire.gridy = 2;
        panel_3.add(rdbtnAleatoire, gbc_rdbtnAleatoire);

        final JButton btnAnnuler = new JButton("Annuler");
        btnAnnuler.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnAnnuler.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_btnAnnuler = new GridBagConstraints();
        gbc_btnAnnuler.anchor = GridBagConstraints.SOUTHEAST;
        gbc_btnAnnuler.insets = new Insets(0, 0, 0, 5);
        gbc_btnAnnuler.gridx = 0;
        gbc_btnAnnuler.gridy = 2;
        contentPane.add(btnAnnuler, gbc_btnAnnuler);

        final JButton btnJouer = new JButton("Jouer");
        btnJouer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validerTraiter();
            }
        });
        btnJouer.setFont(new Font("DejaVu Sans", Font.PLAIN, 12));
        final GridBagConstraints gbc_btnJouer = new GridBagConstraints();
        gbc_btnJouer.anchor = GridBagConstraints.SOUTHWEST;
        gbc_btnJouer.gridx = 2;
        gbc_btnJouer.gridy = 2;
        contentPane.add(btnJouer, gbc_btnJouer);
    }

    protected void validerTraiter() {
        // récupération des infos du joueur 1
        final String nomJoueur1 = textFieldNomJoueur1.getText().trim();
        if (nomJoueur1 == null || nomJoueur1.isEmpty()) {
            FenetrePrincipale.getInstance().ajouterMessageNotification("Champ manquant!",
                                                                       "le nom du joueur 1 doit être renseigné !");
            return;
        }

        // récupération des infos du joueur 2
        String nomJoueur2 = null;
        if (typeJ2.equals(TypeJoueur.Humain)) {
            nomJoueur2 = textFieldNomJoueur2.getText().trim();
            if (nomJoueur2 == null || nomJoueur2.isEmpty()) {
                FenetrePrincipale.getInstance().ajouterMessageNotification("Champ manquant!",
                                                                           "le nom du joueur 2 doit être renseigné !");
                return;
            } else if (nomJoueur2.equals(nomJoueur1)) {
                FenetrePrincipale.getInstance()
                                 .ajouterMessageNotification("Joueur 1 et 2 avec le même nom!",
                                                             "le nom des joueurs doivent être different!");
                return;
            }

        } else if (typeJ2.equals(TypeJoueur.Facile)) {
            nomJoueur2 = nomJoueur1.equals("Wall-E") ? "Robocop" : "Wall-E";
        } else if (typeJ2.equals(TypeJoueur.Moyen)) {
            nomJoueur2 = nomJoueur1.equals("Nono") ? "Goldorak" : "Nono";
        } else if (typeJ2.equals(TypeJoueur.Difficile)) {
            nomJoueur2 = nomJoueur1.equals("Astro") ? "Optimus Prime" : "Astro";
        }

        FenetrePrincipale.getInstance().getMoteur().nouvellePartieLocal(nomJoueur1, nomJoueur2, j1Commence, typeJ2);
        FenetrePrincipale.getInstance().getMoteur().getAvalamClient().setPanels();
        FenetrePrincipale.getInstance().revalidate();
        dispose();

    }
}
