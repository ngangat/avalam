package client.reseaux;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.naming.MalformedLinkException;

import utils.File;
import client.MoteurLeger;
import client.window.FenetrePrincipale;
import client.window.RejoindrePartieEnLigne.Mode;

import common.CodeMessage;
import common.Coup;
import common.messages.Message;
import common.messages.MessageAnnonceCouleur;
import common.messages.MessageBienvenue;
import common.messages.MessageBroadcastAnnulationCoup;
import common.messages.MessageBroadcastRefaireCoup;
import common.messages.MessageCoup;
import common.messages.MessageCoupBroadcast;
import common.messages.MessageDemandeConfirmationAnnulationCoup;
import common.messages.MessageInfosPartie;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.TourTropGrandeException;

public final class MoteurReseaux {

    private MoteurLeger         moteurLeger;

    // File des messages à envoyer au serveur
    private final File<Message> messageAEnvoyer;
    private Socket              socket;
    private String              hote;

    private Thread              recepteur;
    private Thread              emetteur;

    public MoteurReseaux() {
        messageAEnvoyer = new File<>(100); // File suffisament grande pour éviter trop d'attente lorsqu'elle est pleine
    }

    /**
     * Ajoute un message à la file d'attente des messages à envoyer au serveur.
     * 
     * @param message
     *        Message à envoyer (requête)
     */
    public void ajouterMessageAEnvoyer(Message message) {
        messageAEnvoyer.inserer(message);
    }

    public Message extraireMessageAEnvoyer() {
        return messageAEnvoyer.extraire();
    }

    public MoteurLeger getMoteurLeger() {
        return moteurLeger;
    }

    public void nouvelleConnexionDistante(String hote, int portHote, String nomJoueur) throws UnknownHostException,
        IOException {
        Socket temp = null;
        System.out.println("Client : MoteurReseaux : Nouvelle connexion : Création du socket à partir de l'hote : "
            + InetAddress.getByName(hote) + " et du port : " + portHote);
        try {
            this.hote = hote;
            temp = new Socket(hote, portHote);
            System.out.println("Client : MoteurReseaux : socket correctement créé");

        } catch (final ConnectException e) {
            System.out.println("Client : Serveur non accessible");
            throw e;
        }

        if (socket != null) {
            System.out.println("fermeture socket jeu distant");
            socket.close();
            try {
                if (recepteur != null) {
                    recepteur.join();
                }
                if (emetteur != null) {
                    emetteur.join();
                }
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
        socket = temp;

        System.out.println("Client : Création des threads");
        recepteur = new Thread(new Recepteur(this, socket.getInputStream()));
        emetteur = new Thread(new Emetteur(this, socket.getOutputStream(), nomJoueur));
        System.out.println("Client : Démarrage des threads de traitements");
        recepteur.start();
        emetteur.start();

    }

    public void setMoteurLeger(MoteurLeger moteurLeger) {
        this.moteurLeger = moteurLeger;
    }

    public int TailleMessagerie() {
        return messageAEnvoyer.Taille();
    }

    /**
     * Traite un message reçu, lors d'une partie à distance
     * 
     * @param message
     */
    public void traiterMessageDistant(Message message) {
        assert message != null;

        final CodeMessage codeMessage = message.getCodeMessage();
        if (codeMessage.equals(CodeMessage.BIENVENUE)) {
            try {
                final String messageBienvenue = ((MessageBienvenue) message).getMessageBienvenue();
                System.out.println(messageBienvenue);
                FenetrePrincipale.getInstance()
                                 .ajouterMessageNotification("Avalam serveur : " + hote, messageBienvenue);
            } catch (final MalformedLinkException e) {
                e.printStackTrace();
            }
        } else if (codeMessage.equals(CodeMessage.ANNONCE_COULEUR)) {
            /*
             * Réception annonce couleur joueur de la part du serveur
             */
            final MessageAnnonceCouleur messageAnnonceCouleur = (MessageAnnonceCouleur) message;
            try {
                moteurLeger.getInfosPartie().setCouleurJoueurClient(messageAnnonceCouleur.getCouleur());;
            } catch (final MalformedLinkException e) {
                e.printStackTrace();
            }

        } else if (codeMessage.equals(CodeMessage.BROADCAST_COUP)) {
            /*
             * Réception d'un coup + joueur suivant
             */
            System.out.println("reception broadcast");
            final MessageCoupBroadcast messageCoupBroadcast = (MessageCoupBroadcast) message;
            try {
                moteurLeger.traiterCoupDistant(messageCoupBroadcast.getCoup(),
                                               messageCoupBroadcast.getCouleurProchainJoueur());
            } catch (final MalformedLinkException e) {
                e.printStackTrace();
            } catch (final CasesNonAdjacentesException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (final CaseSourceVideException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (final AjoutSurCaseVideException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else if (codeMessage.equals(CodeMessage.ERR_COUP)) {
            Coup coupErrone;
            try {
                coupErrone = ((MessageCoup) message).getCoup();
                moteurLeger.traiterCoupErrone(coupErrone);
            } catch (final MalformedLinkException e) {
            }
        } else if (codeMessage.equals(CodeMessage.DEMANDE_CONFIRMATION_ANNULATION)) {
            try {
                moteurLeger.demanderConfirmationAnnulation((MessageDemandeConfirmationAnnulationCoup) message);
            } catch (final MalformedLinkException e) {
            }
        } else if (codeMessage.equals(CodeMessage.BROADCAST_ANNULATION_COUP)) {

            try {
                moteurLeger.validerAnnulerCoup((MessageBroadcastAnnulationCoup) message);
            } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                     | TourTropGrandeException e) {
                e.printStackTrace();
            }

        } else if (codeMessage.equals(CodeMessage.BROADCAST_REFAIRE_COUP)) {
            moteurLeger.validerRefaireCoupDistant((MessageBroadcastRefaireCoup) message);
        } else if (codeMessage.equals(CodeMessage.INFOS_PARTIE)) {
            final MessageInfosPartie messageInfosPartie = (MessageInfosPartie) message;
            moteurLeger.initInfosPartieDistante(messageInfosPartie.getInfosPartie());
            FenetrePrincipale.getInstance().getFenetreRejoindrePartieEnLigne().dispose();
            FenetrePrincipale.getInstance().getMoteur().getAvalamClient().setPanels();
            FenetrePrincipale.getInstance().revalidate();
        } else if (codeMessage.equals(CodeMessage.NOM_DEJA_UTILISE)) {
            FenetrePrincipale.getInstance().getFenetreRejoindrePartieEnLigne().setAffichage(Mode.NomInvalide);
            FenetrePrincipale.getInstance().ajouterMessageNotification("Nom déjà utilisé",
                                                                       "Ce nom est déjà utilisé par l'autre joueur");
        }

    }

}
