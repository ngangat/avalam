package client.reseaux;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.naming.MalformedLinkException;

import common.messages.Message;
import common.messages.MessageNom;

public class Emetteur implements Runnable {

    private final MoteurReseaux moteur;
    private ObjectOutput        oos;
    private final OutputStream  outputStream;
    private boolean             connexionOk;
    private final String        nomTemporaire;

    public Emetteur(MoteurReseaux moteurReseaux, OutputStream outputStream, String nomJoueur) throws IOException {
        super();
        moteur = moteurReseaux;
        this.outputStream = outputStream;
        nomTemporaire = nomJoueur;
        setConnexionOk(true);
    }

    private void envoyer(Message message) throws IOException {
        oos.writeObject(message);
        oos.flush();
    }

    public synchronized boolean isConnexionOk() {
        return connexionOk;
    }

    @Override
    public void run() {
        System.out.println("Client : Emetteur : Démarrage du thread Emetteur");

        int numeroMessage = 1;
        try {
            final BufferedOutputStream bos = new BufferedOutputStream(outputStream);
            oos = new ObjectOutputStream(bos);
        } catch (final IOException e) {
            e.printStackTrace();
            setConnexionOk(false);

            System.out.println("Client : fermeture thread emetteur");
        }
        try {
            final MessageNom messageNom = new MessageNom(nomTemporaire);
            envoyer(messageNom);
            System.out.println("Client : Emetteur : message (nom) numero " + numeroMessage++ + "envoyé");
        } catch (MalformedLinkException | IOException e1) {
            e1.printStackTrace();
            setConnexionOk(false);
        }

        while (isConnexionOk()) {
            // lecture bloquante fileEnvoi
            final Message message = moteur.extraireMessageAEnvoyer();
            // transmission aux clients
            try {
                envoyer(message);
                System.out.println("Client : Emetteur : message numero " + numeroMessage++ + "envoyé");
            } catch (final IOException e) {
                e.printStackTrace();
                setConnexionOk(false);
                System.out.println("Client : fermeture thread emetteur");
            }

        }
    }

    public synchronized void setConnexionOk(boolean connexionOk) {
        this.connexionOk = connexionOk;
    }
}
