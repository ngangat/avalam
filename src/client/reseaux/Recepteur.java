package client.reseaux;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import common.messages.Message;

public class Recepteur implements Runnable { /* CLIENT */

    private final MoteurReseaux moteur;
    private ObjectInput         ois;
    private final InputStream   inputStream;
    private boolean             connexionOk;

    public Recepteur(MoteurReseaux moteur, InputStream inputStream) {
        super();
        this.moteur = moteur;
        this.inputStream = inputStream;
        connexionOk = true;
    }

    public synchronized boolean isConnexionOk() {
        return connexionOk;
    }

    private Message lireMessage() throws ClassNotFoundException, IOException {
        Message message = null;
        if (ois != null) {
            message = (Message) ois.readObject();
            System.out.println("Client : Un message a Ã©tÃ© lu (rÃ©fÃ©rence : " + message + ")");
        } else {
            throw new IOException("Message null reÃ§u.");
        }
        return message;
    }

    @Override
    public void run() {

        System.out.println("Client : Recepteur : DÃ©marrage du thread rÃ©cepteur");

        try {
            final BufferedInputStream bin = new BufferedInputStream(inputStream);
            ois = new ObjectInputStream(bin);
        } catch (final IOException e) {
            e.printStackTrace();
            setConnexionOk(false);
        }

        int numeroMessage = 0;
        while (isConnexionOk()) {
            Message message = null;
            try {
                // lecture bloquante du message provenant du client
                message = lireMessage();
                // traitement message
                if (message != null) {
                    System.out.println("(Client : Recepteur : message numero " + numeroMessage++ + " lu.");
                    moteur.traiterMessageDistant(message);
                } else {
                    throw new IOException("Le message lu est 'null'.");
                }
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
                System.out.println("Client : Recepteur : erreur ; " + e.getMessage());
                System.out.println("Client : fermeture du thread Recepteur");
                setConnexionOk(false);
            }
        }
    }

    public synchronized void setConnexionOk(boolean connexionOk) {
        this.connexionOk = connexionOk;
    }

}
