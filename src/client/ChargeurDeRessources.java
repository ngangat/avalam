package client;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import common.Case;
import common.Coup.TYPE_MVT;

public class ChargeurDeRessources {
    private final static String themesPath    = "themes/";

    private static String       selectedTheme = "default";

    private static Image        plateau;
    private static Image        controlPanel;

    private static Image[]      pionNoir;
    private static Image[]      pionNoirLocked;
    private static Image[]      pionBlanc;
    private static Image[]      pionBlancLocked;
    private static Image        masqueSourcePionSelectionne;
    private static Image        masquePionsAccessibles;
    private static Image[]      direction;
    private static Image[]      conseil;

    private static ImageIcon[]  annuler;
    private static ImageIcon[]  refaire;
    private static ImageIcon[]  aide;
    private static ImageIcon[]  etatConnexion;

    public static void chargerRessources(String themeName) {
        themeName = ChargeurDeRessources.themesPath + themeName;

        try {
            ChargeurDeRessources.plateau = ImageIO.read(new File(themeName + "/background.png"));
            ChargeurDeRessources.controlPanel = ImageIO.read(new File(themeName + "/backgroundPannelControle.png"));

            ChargeurDeRessources.pionBlanc = new Image[4];
            for (int i = 1; i <= ChargeurDeRessources.pionBlanc.length; i++) {
                ChargeurDeRessources.pionBlanc[i - 1] = ImageIO.read(new File(themeName + "/pionblanc" + i + ".png"));
            }

            ChargeurDeRessources.pionBlancLocked = new Image[5];
            for (int i = 1; i <= ChargeurDeRessources.pionBlancLocked.length; i++) {
                ChargeurDeRessources.pionBlancLocked[i - 1] = ImageIO.read(new File(themeName + "/pionblanc" + i
                    + "fini.png"));
            }

            ChargeurDeRessources.pionNoir = new Image[4];
            for (int i = 1; i <= ChargeurDeRessources.pionNoir.length; i++) {
                ChargeurDeRessources.pionNoir[i - 1] = ImageIO.read(new File(themeName + "/pionnoir" + i + ".png"));
            }

            ChargeurDeRessources.pionNoirLocked = new Image[5];
            for (int i = 1; i <= ChargeurDeRessources.pionNoirLocked.length; i++) {
                ChargeurDeRessources.pionNoirLocked[i - 1] = ImageIO.read(new File(themeName + "/pionnoir" + i
                    + "fini.png"));
            }

            ChargeurDeRessources.direction = new Image[8];
            ChargeurDeRessources.direction[0] = ImageIO.read(new File(themeName + "/deplacementHaut.png"));
            ChargeurDeRessources.direction[1] = ImageIO.read(new File(themeName + "/deplacementHautDroite.png"));
            ChargeurDeRessources.direction[2] = ImageIO.read(new File(themeName + "/deplacementDroite.png"));
            ChargeurDeRessources.direction[3] = ImageIO.read(new File(themeName + "/deplacementBasDroite.png"));
            ChargeurDeRessources.direction[4] = ImageIO.read(new File(themeName + "/deplacementBas.png"));
            ChargeurDeRessources.direction[5] = ImageIO.read(new File(themeName + "/deplacementBasGauche.png"));
            ChargeurDeRessources.direction[6] = ImageIO.read(new File(themeName + "/deplacementGauche.png"));
            ChargeurDeRessources.direction[7] = ImageIO.read(new File(themeName + "/deplacementHautGauche.png"));
            
            ChargeurDeRessources.conseil = new Image[8];
            ChargeurDeRessources.conseil[0] = ImageIO.read(new File(themeName + "/conseilHaut.png"));
            ChargeurDeRessources.conseil[1] = ImageIO.read(new File(themeName + "/conseilHautDroite.png"));
            ChargeurDeRessources.conseil[2] = ImageIO.read(new File(themeName + "/conseilDroite.png"));
            ChargeurDeRessources.conseil[3] = ImageIO.read(new File(themeName + "/conseilBasDroite.png"));
            ChargeurDeRessources.conseil[4] = ImageIO.read(new File(themeName + "/conseilBas.png"));
            ChargeurDeRessources.conseil[5] = ImageIO.read(new File(themeName + "/conseilBasGauche.png"));
            ChargeurDeRessources.conseil[6] = ImageIO.read(new File(themeName + "/conseilGauche.png"));
            ChargeurDeRessources.conseil[7] = ImageIO.read(new File(themeName + "/conseilHautGauche.png"));
            
            ChargeurDeRessources.masqueSourcePionSelectionne = ImageIO.read(new File(themeName + "/pionSource.png"));
            ChargeurDeRessources.masquePionsAccessibles = ImageIO.read(new File(themeName + "/pionVoisin.png"));

            ChargeurDeRessources.annuler = new ImageIcon[4];
            ChargeurDeRessources.annuler[0] = new ImageIcon(themeName + "/annulerOff.png");
            ChargeurDeRessources.annuler[1] = new ImageIcon(themeName + "/annulerOn.png");
            ChargeurDeRessources.annuler[2] = new ImageIcon(themeName + "/annulerPressed.png");
            ChargeurDeRessources.annuler[3] = new ImageIcon(themeName + "/annulerSelected.png");

            ChargeurDeRessources.refaire = new ImageIcon[4];
            ChargeurDeRessources.refaire[0] = new ImageIcon(themeName + "/refaireOff.png");
            ChargeurDeRessources.refaire[1] = new ImageIcon(themeName + "/refaireOn.png");
            ChargeurDeRessources.refaire[2] = new ImageIcon(themeName + "/refairePressed.png");
            ChargeurDeRessources.refaire[3] = new ImageIcon(themeName + "/refaireSelected.png");

            ChargeurDeRessources.aide = new ImageIcon[4];
            ChargeurDeRessources.aide[0] = new ImageIcon(themeName + "/aideOff.png");
            ChargeurDeRessources.aide[1] = new ImageIcon(themeName + "/aideOn.png");
            ChargeurDeRessources.aide[2] = new ImageIcon(themeName + "/aidePressed.png");
            ChargeurDeRessources.aide[3] = new ImageIcon(themeName + "/aideSelected.png");

            ChargeurDeRessources.etatConnexion = new ImageIcon[2];
            ChargeurDeRessources.etatConnexion[0] = new ImageIcon(themeName + "/serverOff.png");
            ChargeurDeRessources.etatConnexion[1] = new ImageIcon(themeName + "/serverOn.png");
            
            ChargeurDeRessources.selectedTheme = themeName;
        } catch (final IOException e) {
            e.printStackTrace();
            /*
             * On suppose que default est un thème valide
             * si le chargement de themeName échoue, on charge default
             */
            if (!themeName.equals(ChargeurDeRessources.themesPath + "default"))
                ChargeurDeRessources.chargerRessources("default");
        }
    }

    public static ImageIcon[] getAide() {
        return ChargeurDeRessources.aide;
    }

    public static ImageIcon[] getAnnuler() {
        return ChargeurDeRessources.annuler;
    }

    public static Image getControlPanel() {
        return ChargeurDeRessources.controlPanel;
    }

    public static Image getMasquePionSource() {
        return ChargeurDeRessources.masqueSourcePionSelectionne;
    }

    public static Image getMouvement(TYPE_MVT typeMouvement) {
        switch (typeMouvement) {
            case HAUT:
                return ChargeurDeRessources.direction[0];
            case HAUT_DROIT:
                return ChargeurDeRessources.direction[1];
            case DROIT:
                return ChargeurDeRessources.direction[2];
            case BAS_DROIT:
                return ChargeurDeRessources.direction[3];
            case BAS:
                return ChargeurDeRessources.direction[4];
            case BAS_GAUCHE:
                return ChargeurDeRessources.direction[5];
            case GAUCHE:
                return ChargeurDeRessources.direction[6];
            case HAUT_GAUCHE:
                return ChargeurDeRessources.direction[7];
            default:
                return null;
        }
    }

    public static Image getConseil(TYPE_MVT typeMouvement) {
        switch (typeMouvement) {
            case HAUT:
                return ChargeurDeRessources.conseil[0];
            case HAUT_DROIT:
                return ChargeurDeRessources.conseil[1];
            case DROIT:
                return ChargeurDeRessources.conseil[2];
            case BAS_DROIT:
                return ChargeurDeRessources.conseil[3];
            case BAS:
                return ChargeurDeRessources.conseil[4];
            case BAS_GAUCHE:
                return ChargeurDeRessources.conseil[5];
            case GAUCHE:
                return ChargeurDeRessources.conseil[6];
            case HAUT_GAUCHE:
                return ChargeurDeRessources.conseil[7];
            default:
                return null;
        }
    }

    public static Image getPion(byte couleurCase, int hauteur, boolean locked) {
        return locked ? couleurCase == Case.couleurB ? ChargeurDeRessources.pionBlancLocked[hauteur - 1]
            : ChargeurDeRessources.pionNoirLocked[hauteur - 1] : couleurCase == Case.couleurB
            ? ChargeurDeRessources.pionBlanc[hauteur - 1] : ChargeurDeRessources.pionNoir[hauteur - 1];
    }

    public static Image getPionOverlay() {
        return ChargeurDeRessources.masquePionsAccessibles;
    }

    public static Image getPlateau() {
        return ChargeurDeRessources.plateau;
    }

    public static ImageIcon[] getRefaire() {
        return ChargeurDeRessources.refaire;
    }

    public static ImageIcon[] getServerStatus() {
        return ChargeurDeRessources.etatConnexion;
    }

    public static String getThemeName() {
        return ChargeurDeRessources.selectedTheme;
    }

    public static ArrayList<String> listThemes() {
        final ArrayList<String> themes = new ArrayList<String>();
        final File themeDirectory = new File(ChargeurDeRessources.themesPath);
        final String[] ls = themeDirectory.list();

        for (final String f : ls) {
            if (new File(ChargeurDeRessources.themesPath + f).isDirectory()) {
                themes.add(f);
            }
        }

        return themes;
    }

    private ChargeurDeRessources() {
    }
}
