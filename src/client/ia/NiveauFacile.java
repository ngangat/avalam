package client.ia;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import client.MoteurLeger;
import client.ia.strategies.StrategieNiveauFacile;
import common.CouleurJoueur;
import common.Coup;
import common.Joueur;
import common.TerrainListe;
import common.TerrainMatrice;
import common.TypeJoueur;
import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.MauvaisChoixDeCouleurException;
import exceptions.TourTropGrandeException;

public class NiveauFacile extends Joueur implements Observer, IA {
    private static final long   serialVersionUID  = 5872387645303749556L;
    private Boolean             traitementEnCours = false;
    private TerrainListe        terrainDeSimulation;
    private final CouleurJoueur couleurJoueur;
    private Coup                meilleurCoup;
    private final MoteurLeger   moteur;
    private boolean             suicideDemande;

    public NiveauFacile(CouleurJoueur couleurJoueur, TypeJoueur typeJ2, MoteurLeger moteur)
                                                                                           throws MauvaisChoixDeCouleurException {
        super(TypeJoueur.Facile, "MyLittlePony", couleurJoueur);
        this.couleurJoueur = couleurJoueur;
        meilleurCoup = null;
        this.moteur = moteur;
    }

    private void choisirCoup() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        final LinkedList<Coup> listeCoupPossibles = terrainDeSimulation.getListeCoupsPossible();

        Collections.sort(listeCoupPossibles, new StrategieNiveauFacile(terrainDeSimulation, couleurJoueur));

        if (listeCoupPossibles.size() > 0) {

            final LinkedList<Coup> listeMeilleursCoups = new LinkedList<Coup>();
            listeMeilleursCoups.add(listeCoupPossibles.pollLast());
            final Coup meilleur = listeMeilleursCoups.peekLast();
            int i = listeCoupPossibles.size() - 1;
            while (i > 0
                && new StrategieNiveauFacile(terrainDeSimulation, couleurJoueur).compare(meilleur,
                                                                                         listeCoupPossibles.get(i)) == 0) {
                listeMeilleursCoups.add(listeCoupPossibles.get(i));
                i--;
            }

            /* Mauvais coups à intégrer :
             * On prend le tiers (inférieur) du minimal entre le nombre de bons coups et entre le nombre de mauvais coups
             */
            int mauvaisCoupsAIntegrer = Math.min(listeMeilleursCoups.size(),
                                                 (listeCoupPossibles.size() - listeMeilleursCoups.size()));
            mauvaisCoupsAIntegrer = mauvaisCoupsAIntegrer / 2;
            int offset = listeMeilleursCoups.size();
            for (int j = listeCoupPossibles.size() - offset; j > listeCoupPossibles.size()
                - (offset + mauvaisCoupsAIntegrer); j--) {
                listeMeilleursCoups.add(listeCoupPossibles.get(j));
            }

            // Random depuis les meilleurs
            final Random r = new Random();
            meilleurCoup = listeMeilleursCoups.get(r.nextInt(listeMeilleursCoups.size()));
        }
    }

    public synchronized Boolean getTraitementEnCours() {
        return traitementEnCours;
    }

    public synchronized boolean isSuicideDemande() {
        return suicideDemande;
    }

    public void jouerCoup(TerrainListe terrain) {
        if (!terrain.finPartie()) {
            terrainDeSimulation = terrain;
            meilleurCoup = null;

            choisirCoup();

            if (meilleurCoup != null) {
                System.out.println("meilleur coup : source = " + meilleurCoup.getPrecedenteSource() + ", dest = "
                    + meilleurCoup.getPrescedenteDest());
                System.out.println("meilleur coup : " + meilleurCoup);
                Coup c = null;
                try {
                    // FIXME Ici, exception CaseSourceVide...
                    c = new Coup(meilleurCoup, moteur.getInfosPartie().getTerrain());

                    if (moteur.getInfosPartie().getCouleurJoueurCourant().equals(couleurJoueur)) {
                        moteur.jouerCoup(c);
                    }
                } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                         | TourTropGrandeException | HorsDuTerrainException e1) {
                    e1.printStackTrace();
                }

            } else {
                System.err.println("il n'y a plus aucun coup possible");
            }
        } else {
            System.out.println("La partie est terminé!");
        }
    }

    @Override
    public void seSuicider() {
        setSuicideDemande(true);
    }

    public synchronized void setSuicideDemande(boolean suicideDemande) {
        this.suicideDemande = suicideDemande;
    }

    public synchronized void setTraitementEnCours(Boolean traitementEnCours) {
        this.traitementEnCours = traitementEnCours;
    }

    @Override
    public void update(Observable o, Object arg) {

        new Thread() {

            @Override
            public void run() {
                synchronized (traitementEnCours) {

                    if (!getTraitementEnCours()
                        && moteur.getInfosPartie().getCouleurJoueurCourant().equals(couleurJoueur)
                        && !moteur.getInfosPartie().getTerrain().finPartie()) {

                        setTraitementEnCours(true);

                        System.out.println("Niveau rapide : c'est à moi de jouer ! (" + couleurJoueur.name() + ")");
                        final long depart = System.nanoTime();
                        jouerCoup(new TerrainListe((TerrainMatrice) moteur.getInfosPartie().getTerrain()));
                        final long duree = System.nanoTime() - depart;
                        System.out.println("Coup joué en " + duree / 1000000000.0 + " secondes");

                        traitementEnCours = false;
                    }
                }
            }
        }.start();

    }

    @Override
    public Coup conseillerCoup() {
        terrainDeSimulation = new TerrainListe((TerrainMatrice) moteur.getInfosPartie().getTerrain());
        choisirCoup();
        return meilleurCoup;
    }
}
