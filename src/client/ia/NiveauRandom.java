package client.ia;

import java.awt.Point;
import java.util.LinkedList;
import java.util.Random;

import common.Case;
import common.CouleurJoueur;
import common.Coup;
import common.Joueur;
import common.Terrain;
import common.TypeJoueur;
import exceptions.HorsDuTerrainException;
import exceptions.MauvaisChoixDeCouleurException;

public class NiveauRandom extends Joueur implements IA {

    /**
     * 
     */
    private static final long serialVersionUID = -6910688851629324396L;
    private final Terrain     terrain;

    public NiveauRandom(Terrain terrain, TypeJoueur typeJoueur, String nom, CouleurJoueur couleur)
                                                                                                  throws MauvaisChoixDeCouleurException {
        super(typeJoueur, nom, couleur);
        this.terrain = terrain;
    }

    public void choisirDestination(Point source) throws HorsDuTerrainException {
        final LinkedList<Point> listVoisin = getVoisins(source.x, source.y);
        final Random r = new Random();
        final int caseChoisie = r.nextInt(listVoisin.size());
        listVoisin.get(caseChoisie);
    }

    public Point choisirSource() throws HorsDuTerrainException {
        final Random r = new Random();
        final LinkedList<LinkedList<Point>> list = new LinkedList<LinkedList<Point>>();
        final LinkedList<Point> listSource = new LinkedList<Point>();
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                final Case source = terrain.getCase(i, j);
                final LinkedList<Point> voisin = getVoisins(i, j);
                if (!source.equals(Case.CENTRE) && !source.equals(Case.LIBRE) && !source.equals(Case.NON_DEFINI)
                    && source.getHauteur() < 5 && voisin.size() != 0) {
                    listSource.add(new Point(i, j));
                    list.add(voisin);

                }
            }
        }
        final int caseChoisie = r.nextInt(listSource.size());
        final Point c = listSource.get(caseChoisie);

        return c;

    }

    private LinkedList<Point> getVoisins(int x, int y) throws HorsDuTerrainException {
        final byte hauteur = terrain.getCase(x, y).getHauteur();
        final LinkedList<Point> list = new LinkedList<Point>();
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                final Case dest = terrain.getCase(i, j);
                if (!dest.equals(Case.CENTRE) && !dest.equals(Case.LIBRE) && !dest.equals(Case.NON_DEFINI)
                    && dest.getHauteur() + hauteur <= 5 && i != x && j != y) {
                    list.add(new Point(i, j));
                }
            }

        }
        return list;
    }

    @Override
    public void seSuicider() {
        // TODO Auto-generated method stub

    }

    @Override
    public Coup conseillerCoup() {
        // TODO Auto-generated method stub
        return null;
    }

}
