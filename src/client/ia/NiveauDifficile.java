package client.ia;

import java.awt.Point;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import utils.RandomSingleton;
import client.MoteurLeger;
import client.ia.strategies.StrategieNiveauRapide;

import common.Case;
import common.CouleurJoueur;
import common.Coup;
import common.Joueur;
import common.Terrain;
import common.TerrainListe;
import common.TerrainMatrice;
import common.TypeJoueur;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.MauvaisChoixDeCouleurException;
import exceptions.TourTropGrandeException;

public class NiveauDifficile extends Joueur implements Observer, IA {
    private enum EnteteEval {
        GAGNE(0b011_00000_000_000_0000_0000_00000),
        RECHERCHE_EN_COURS(0b010_00000_000_000_0000_0000_00000),
        NULLE(0b001_00000_000_000_0000_0000_00000),
        PERD(0b000_00000_000_000_0000_0000_00000);

        int valeurEntete;

        private EnteteEval(int valeurEntete) {
            this.valeurEntete = valeurEntete;
        }

        public int getValeur() {
            return valeurEntete;
        }

    }

    private static final long serialVersionUID      = 2315674455547803727L;

    //    private final int         MASQUE_UTILISE        = 0b111_11111_111_111_1111_1111_11111;
    //    private final int         MASQUE_DISPO          = 0b000_11111_111_111_1111_1111_11111;
    //    private final int         MASQUE_PROFONDEUR     = 0b000_00000_111_100_0000_0000_00000;
    private final int         MASQUE_STRATEGIE_FINE = 0b000_00000_000_011_1111_1111_11111;

    private final int         MASQUE_NB_1           = 0b11111;
    private final int         MASQUE_NB_2           = 0b1111;
    private final int         MASQUE_NB_3           = 0b1111;
    private final int         MASQUE_NB_4           = 0b111;
    private final int         MASQUE_NB_5           = 0b111;
    private final int         MASQUE_NB_TOURS_TOT   = 0b11111;

    private final int         DECALAGE_NB_1         = 0;
    private final int         DECALAGE_NB_2         = 5;
    private final int         DECALAGE_NB_3         = 9;
    private final int         DECALAGE_NB_4         = 13;
    private final int         DECALAGE_NB_5         = 16;
    private final int         DECALAGE_NB_TOURS_TOT = 19;

    private final int         DECALAGE_PROF         = 15;

    private int setNb1(int score, int nb) {
        int tmp = score | ~(MASQUE_NB_1 << DECALAGE_NB_1); // reset
        int s = tmp & ((nb & MASQUE_NB_1) << DECALAGE_NB_1);
        return s;
    }

    private int setNb2(int score, int nb) {

        int tmp = score | ~(MASQUE_NB_2 << DECALAGE_NB_2); // reset
        int s = tmp & ((nb & MASQUE_NB_2) << DECALAGE_NB_2);
        return s;
    }

    private int setNb3(int score, int nb) {

        int tmp = score | ~(MASQUE_NB_3 << DECALAGE_NB_3); // reset
        int s = tmp & ((nb & MASQUE_NB_3) << DECALAGE_NB_3);
        return s;
    }

    private int setNb4(int score, int nb) {

        int tmp = score | ~(MASQUE_NB_4 << DECALAGE_NB_4); // reset
        int s = tmp & ((nb & MASQUE_NB_4) << DECALAGE_NB_4);
        return s;
    }

    private int setNb5(int score, int nb) {

        int tmp = score | ~(MASQUE_NB_5 << DECALAGE_NB_5); // reset
        int s = tmp & ((nb & MASQUE_NB_5) << DECALAGE_NB_5);
        return s;
    }

    /**
     * 
     * @param score ancienscore
     * @return nouveau score, avec le le nombre de tours incrémenté (quelque soit la valeur)
     */
    private int incNbToursTot(int score) {

        int tmp = score | ~(MASQUE_NB_TOURS_TOT << DECALAGE_NB_TOURS_TOT); // reset
        int valprec = (score >> DECALAGE_NB_TOURS_TOT) & MASQUE_NB_TOURS_TOT;
        int newVal = valprec + 1;
        int s = tmp & ((newVal & MASQUE_NB_TOURS_TOT) << DECALAGE_NB_TOURS_TOT);
        return s;
    }

    /**
     * 
     * @param score Valeur du score avant la modification
     * @param nbTours Le nombres de tours correspondant à valeurTour
     * @param valeurTour valeur de tour concerné par la modification (1 à 5)
     * @return nouveau score
     */
    private int setNbTours(int score, int nbTours, int valeurTour) {
        switch (valeurTour) {
            case 1:
                return setNb1(score, nbTours);
            case 2:
                return setNb2(score, nbTours);
            case 3:
                return setNb3(score, nbTours);
            case 4:
                return setNb4(score, nbTours);
            case 5:
                return setNb5(score, nbTours);
            default:
                return score;
        }
    }

    private int getNb1(int score) {
        int nb = (score & (MASQUE_NB_1 << DECALAGE_NB_1)) >> DECALAGE_NB_1;
        return nb;
    }

    private int getNb2(int score) {
        int nb = (score & (MASQUE_NB_2 << DECALAGE_NB_2)) >> DECALAGE_NB_2;
        return nb;
    }

    private int getNb3(int score) {
        int nb = (score & (MASQUE_NB_3 << DECALAGE_NB_3)) >> DECALAGE_NB_3;
        return nb;
    }

    private int getNb4(int score) {
        int nb = (score & (MASQUE_NB_4 << DECALAGE_NB_4)) >> DECALAGE_NB_4;
        return nb;
    }

    private int getNb5(int score) {
        int nb = (score & (MASQUE_NB_5 << DECALAGE_NB_5)) >> DECALAGE_NB_5;
        return nb;
    }

    private int getNbTours(int score, int valeurTour) {
        switch (valeurTour) {
            case 1:
                return getNb1(score);
            case 2:
                return getNb2(score);
            case 3:
                return getNb3(score);
            case 4:
                return getNb4(score);
            case 5:
                return getNb5(score);
            default:
                return score;
        }
    }

    /**
     * 
     */
    private Boolean             traitementEnCours = false;
    // private Terrain             terrainDeSimulation;
    private final CouleurJoueur couleurJoueur;
    private Coup                meilleurCoup;
    //private int                 profondeurMaxi;
    private final MoteurLeger   moteur;
    private int                 nbPionsDeplace;
    private boolean             suicideDemande    = false;

    public NiveauDifficile(CouleurJoueur couleurJoueur, TypeJoueur difficulte, MoteurLeger moteur)
                                                                                                  throws MauvaisChoixDeCouleurException {
        super(difficulte, "Nono", couleurJoueur);
        this.couleurJoueur = couleurJoueur;
        meilleurCoup = null;

        this.moteur = moteur;
    }

    private LinkedList<Coup> getMeilleurCoups(CouleurJoueur couleurNoeudCourant, Terrain current) {
        final LinkedList<Coup> listeCoupPossibles = current.getListeCoupsPossible();
        final LinkedList<Coup> listeMeilleursCoups = new LinkedList<Coup>();
        //System.out.println("nb Coups possibles : " + listeCoupPossibles.size());

        Collections.sort(listeCoupPossibles, new StrategieNiveauRapide(current, couleurNoeudCourant));

        // Parcous de la liste des coups possibles
        // On va prendre le 1er de la liste: Le meilleur après les tris stratégiques
        if (listeCoupPossibles.size() > 0) {

            listeMeilleursCoups.add(listeCoupPossibles.pollLast());
            final Coup meilleur = listeMeilleursCoups.peekLast();
            int i = listeCoupPossibles.size() - 1;
            while (i > 0
                && new StrategieNiveauRapide(current, couleurNoeudCourant).compare(meilleur, listeCoupPossibles.get(i)) == 0) {
                listeMeilleursCoups.add(listeCoupPossibles.get(i));
                i--;
            }
        }
        return listeMeilleursCoups;

    }

    private int alphabeta(int profondeur, int profondeurMaxi, int alpha, int beta, Coup coup, TerrainListe current)
        throws InterruptedException {

        if (isSuicideDemande()) {
            throw new InterruptedException("l'IA se suicide");
        }

        LinkedList<Coup> listeCoupPossibles = current.getListeCoupsPossible();

        // System.out.println("nb Coups possibles : " + listeCoupPossibles.size());

        if (listeCoupPossibles == null || listeCoupPossibles.isEmpty()) {
            return evaluerDernierCoup(profondeur, profondeurMaxi, true, coup, current);
        } else if (profondeur >= profondeurMaxi) {
            return evaluerDernierCoup(profondeur, profondeurMaxi, false, coup, current);
        }

        final CouleurJoueur couleurNoeudCourant = getCouleurNoeudCourant(profondeur);
        Collections.sort(listeCoupPossibles, new StrategieNiveauRapide(current, couleurNoeudCourant));
        LinkedList<Coup> meilleurCoups = getMeilleurCoups(couleurNoeudCourant, current);
        int score = -Integer.MAX_VALUE;

        // Parcous de la liste des coups possibles
        if (meilleurCoups != null && !meilleurCoups.isEmpty()) {
            Collections.shuffle(meilleurCoups); // FIXME : RANDOM : A enlever si tests
            for (Coup c : meilleurCoups) {
                try {
                    current.effectuerCoup(c, false);
                } catch (TourTropGrandeException | HorsDuTerrainException | CasesNonAdjacentesException
                         | CaseSourceVideException | AjoutSurCaseVideException e) {
                    e.printStackTrace();
                }

                score = -alphabeta(profondeur + 1, profondeurMaxi, -beta, -alpha, c, current);

                try {
                    current.annulerCoup(c);
                } catch (HorsDuTerrainException e) {
                    e.printStackTrace();
                }
                if (score >= alpha) {
                    alpha = score;
                    meilleurCoup = c;
                    if (alpha >= beta) {
                        break;
                    }
                }
            }
        }
        return alpha;

    }

    private CouleurJoueur getCouleurNoeudCourant(int profondeur) {
        CouleurJoueur couleurNoeudCourant = null;
        try {
            couleurNoeudCourant = profondeur % 2 == 0 ? couleurJoueur : couleurJoueur.getCouleurAdversaire();
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }
        return couleurNoeudCourant;
    }

    /**
     * Fonction d'évaluation du dernier coup (soit un coup bloquant, soit un noeud max) l'entête définit la priorité des
     * critères : plus le critère est "décalé" en poids fort, plus il est important
     * On ignore le premier bit pour ne pas avoir de problème de complément à 2
     * 
     * Entête : 1+2 premiers bits de poids fort
     * <ul>
     *     <li>0b011 -> le dernier coup est bloquant (plus de coup possible) et le coup est gagnant</li>
     *     <li>0b010 -> le dernier coup n'est pas bloquant (la profondeur max a été atteinte)</li>
     *     <li>0b001 -> le dernier coup est bloquant (plus de coup possible) et le coup est nulle</li>
     *     <li>0b000 -> le dernier coup est bloquant (plus de coup possible) et le coup est perdant</li>
     * </ul>
     * 
     * Valeurs :
     * <ul>
     *     <li>profondeur : 6 bits suivants</li>
     *     <li>autres : stratégie fine => comment évaluer le noeud courant si on a pas terminé ? (TODO: à définir)</li>
     * </ul>
     * 
     * @param profondeur Profondeur du noeud terminal (inférieur ou égale à la profondeur max défini par l'utilisateur)
     * @param noeudFinal true si le noeud correspond à un coup bloquant (on ne peut plus jouer
     * @param couleurJoueur Correspond à la couleur du noeud en cours de parcours
     * @return une valeur entière (short -> 16 bits) représentant l'évaluation du noeud
     */
    private int
                    evaluerDernierCoup(int profondeur, int profondeurMaxi, boolean noeudFinal, Coup dernier,
                                       Terrain current) {

        int entete;
        int eval;
        int deltaNbTours = 0;
        CouleurJoueur couleurNoeudCourant = getCouleurNoeudCourant(profondeur);
        deltaNbTours = current.getDiffNbTours(couleurNoeudCourant);

        final int prof = ((profondeurMaxi - profondeur) << DECALAGE_PROF);

        int stratfine = (deltaNbTours & MASQUE_STRATEGIE_FINE);
        stratfine = deltaNbTours + 24;
        if (!noeudFinal) { // Noeud interne : on peut toujours joué, mais on est arrivé au niveau de profondeur donné
            entete = EnteteEval.RECHERCHE_EN_COURS.getValeur();
        } else if (deltaNbTours > 0) {
            entete = EnteteEval.GAGNE.getValeur();
        } else if (deltaNbTours == 0) {
            entete = EnteteEval.NULLE.getValeur();
        } else {
            entete = EnteteEval.PERD.getValeur();
        }
        //System.out.println("prof : " + profondeur);
        eval = (entete | prof | stratfine);
        return eval;
    }

    public synchronized Boolean getTraitementEnCours() {
        return traitementEnCours;
    }

    public synchronized boolean isSuicideDemande() {
        return suicideDemande;
    }

    public void jouerCoup(Terrain t) throws InterruptedException {

        if (!t.finPartie()) {
            TerrainListe terrainDeSimulation = new TerrainListe((TerrainMatrice) t);
            nbPionsDeplace++;
            int profondeurMaxi = 4;
            
            meilleurCoup = null;
            //            LinkedList<Coup> bestCoups = new LinkedList<>();
            //            ArrayList<HashMap<Point, ArrayList<Point>>> listeSousTerrain = getSousTerrain((TerrainMatrice) t);
            //            int score = -Integer.MAX_VALUE;
            //            for (HashMap<Point, ArrayList<Point>> pointsTerrain : listeSousTerrain) {
            //                TerrainListe current = new TerrainListe((TerrainMatrice) t, pointsTerrain);
            //                System.out.println();
            //                score = alphabeta(0, profondeurMaxi, score, Integer.MAX_VALUE, null, current);
            //                System.out.println("Score : " + score);
            //                System.out.println("meilleur Coup : " + meilleurCoup.getSourceCoord());
            //                if (meilleurCoup != null)
            //                    try {
            //                        bestCoups.add(new Coup(meilleurCoup, terrainDeSimulation));
            //                    } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
            //                             | TourTropGrandeException | HorsDuTerrainException e) {
            //                        e.printStackTrace();
            //                    }
            //            }

            if (nbPionsDeplace < 8)
                choisirCoup(terrainDeSimulation);
            else
                alphabeta(0, profondeurMaxi, -Integer.MAX_VALUE, Integer.MAX_VALUE, meilleurCoup, terrainDeSimulation);
            // System.out.println("Nb coups possible : " + bestCoups.size());
            if (meilleurCoup != null) {
                //                System.out.println("meilleur coup : source = " + meilleurCoup.getPrecedenteSource() + ", dest = "
                //                    + meilleurCoup.getPrescedenteDest());
                Coup c = null;
                try {
                    c = new Coup(meilleurCoup, moteur.getInfosPartie().getTerrain());
                    if (couleurJoueur.equals(moteur.getInfosPartie().getCouleurJoueurCourant())) {
                        moteur.jouerCoup(c);
                    }
                } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                         | TourTropGrandeException | HorsDuTerrainException e1) {
                    e1.printStackTrace();
                }

            } else {
                nbPionsDeplace--;
                System.err.println("il n'y a plus aucun coup possible");
            }
        }

    }

//    private ArrayList<HashMap<Point, ArrayList<Point>>> getSousTerrain(TerrainMatrice t) {
//        TerrainListe temp = new TerrainListe(t);
//        Integer couleurCourant = 0;
//        Integer[][] tabcouleur = new Integer[9][9];
//
//        for (int y = 0; y < 9; y++) {
//            for (int x = 0; x < 9; x++) {
//                tabcouleur[x][y] = -2;
//                try {
//                    if (temp.getCase(x, y).estUneTour()) {
//                        tabcouleur[x][y] = -1;
//                    }
//                } catch (HorsDuTerrainException e) {
//                }
//            }
//        }
//
//        for (int y = 0; y < 9; y++) {
//            for (int x = 0; x < 9; x++) {
//                try {
//                    if (t.getCase(x, y).estUneTour()) {
//                        if (tabcouleur[x][y] == -1) {
//                            couleurCourant++;
//                            //System.out.println("coulCourant = " + couleurCourant);
//                            colorierSommet(temp, tabcouleur, new Point(x, y), couleurCourant);
//                        }
//                    }
//                } catch (HorsDuTerrainException e) {
//                }
//            }
//        }
//
//        // System.out.println("-----------------nb terrain : " + couleurCourant);
//
//        ArrayList<HashMap<Point, ArrayList<Point>>> listeTerrainsPoints = new ArrayList<>();
//        for (int i = 0; i < couleurCourant; i++) {
//            listeTerrainsPoints.add(new HashMap<Point, ArrayList<Point>>());
//        }
//        for (int y = 0; y < 9; y++) {
//            for (int x = 0; x < 9; x++) {
//                if (tabcouleur[x][y] >= 1) {
//                    listeTerrainsPoints.get(tabcouleur[x][y] - 1).put(new Point(x, y), new ArrayList<Point>());
//                    //System.out.println(listeTerrainsPoints.get(tabcouleur[x][y] - 1));
//                }
//            }
//        }
//        return listeTerrainsPoints;
//    }

    void colorierSommet(TerrainListe t, Integer[][] couls, Point p, Integer coul) {
        couls[p.x][p.y] = coul;
        for (Point adj : t.getTerrain().keySet()) {
            if (couls[adj.x][adj.y] == -1) {
                colorierSommet(t, couls, adj, coul);
            }
        }
    }

        private LinkedList<Coup> getMeilleurCoups(Terrain current) {
            final LinkedList<Coup> listeCoupPossibles = current.getListeCoupsPossible();
            final LinkedList<Coup> listeMeilleursCoups = new LinkedList<Coup>();
            System.out.println("nb Coups possibles : " + listeCoupPossibles.size());
    
            Collections.sort(listeCoupPossibles, new StrategieNiveauRapide(current, couleurJoueur));
    
            // Parcous de la liste des coups possibles
            // On va prendre le 1er de la liste: Le meilleur après les tris stratégiques
            if (listeCoupPossibles.size() > 0) {
    
                listeMeilleursCoups.add(listeCoupPossibles.pollLast());
                final Coup meilleur = listeMeilleursCoups.peekLast();
                int i = listeCoupPossibles.size() - 1;
                while (i > 0
                    && new StrategieNiveauRapide(current, couleurJoueur).compare(meilleur, listeCoupPossibles.get(i)) == 0) {
                    listeMeilleursCoups.add(listeCoupPossibles.get(i));
                    i--;
                }
            }
            return listeMeilleursCoups;
    
        }

        private void choisirCoup(Terrain current) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            LinkedList<Coup> listeMeilleursCoups = getMeilleurCoups(current);
            if (!listeMeilleursCoups.isEmpty())
                meilleurCoup = listeMeilleursCoups.get(RandomSingleton.getInstance().nextInt(listeMeilleursCoups.size()));
            else
                meilleurCoup = null;
    
        }

    //    private int differencePondere(CouleurJoueur couleur, Terrain current) {
    //        int nbCouleurJoueurCourant = couleur.getValeur();
    //        int nbCouleurJoueurAdverse = 0;
    //        try {
    //            nbCouleurJoueurAdverse = couleur.getCouleurAdversaire().getValeur();
    //        } catch (IllegalAccessException e1) {
    //        }
    //        for (int y = 0; y < 9; y++) {
    //            for (int x = 0; x < 9; x++) {
    //                Case c;
    //                try {
    //                    c = current.getCase(x, y);
    //                    if (c.getCouleur() == couleurJoueur.getValeur() && c.getHauteur() > 0 && c.getHauteur() <= 5) {
    //                        nbCouleurJoueurCourant = setNbTours(nbCouleurJoueurCourant,
    //                                                            getNbTours(nbCouleurJoueurCourant, c.getHauteur()),
    //                                                            c.getHauteur());
    //                        nbCouleurJoueurCourant = incNbToursTot(nbCouleurJoueurCourant);
    //                    } else if (c.getCouleur() != couleurJoueur.getValeur() && c.getHauteur() > 0 && c.getHauteur() <= 5) {
    //                        nbCouleurJoueurAdverse = setNbTours(nbCouleurJoueurAdverse,
    //                                                            getNbTours(nbCouleurJoueurAdverse, c.getHauteur()),
    //                                                            c.getHauteur());
    //                        nbCouleurJoueurAdverse = incNbToursTot(nbCouleurJoueurAdverse);
    //                    }
    //                } catch (HorsDuTerrainException e) {
    //                }
    //
    //            }
    //        }
    //        return (nbCouleurJoueurCourant - nbCouleurJoueurAdverse);
    //    }

    public int getNbToursPondere(CouleurJoueur couleur, Terrain current) {
        int nbCouleurJoueurCourant = couleur.getValeur();

        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                Case c;
                try {
                    c = current.getCase(x, y);
                    if (c.getCouleur() == couleurJoueur.getValeur() && c.getHauteur() > 0 && c.getHauteur() <= 5) {
                        nbCouleurJoueurCourant = setNbTours(nbCouleurJoueurCourant,
                                                            getNbTours(nbCouleurJoueurCourant, c.getHauteur()),
                                                            c.getHauteur());

                        nbCouleurJoueurCourant = incNbToursTot(nbCouleurJoueurCourant);
                    }
                } catch (HorsDuTerrainException e) {
                }

            }
        }
        return (nbCouleurJoueurCourant);
    }

    //    private short noterCoup(Coup dernierCoup, CouleurJoueur couleurNoeudCourant) {
    //        short val;
    //
    //        if (couleurNoeudCourant.getValeur() == dernierCoup.getPrecedenteSource().getCouleur()) {
    //            if (couleurNoeudCourant.getValeur() != dernierCoup.getPrescedenteDest().getCouleur()) {
    //                val = 4;
    //            } else {
    //                val = 2;
    //            }
    //        } else {
    //            if (couleurNoeudCourant.getValeur() != dernierCoup.getPrescedenteDest().getCouleur()) {
    //                val = 3;
    //            } else {
    //                val = 1;
    //            }
    //        }
    //
    //        return val;
    //    }

    @Override
    public void seSuicider() {
        setSuicideDemande(true);
    }

    private synchronized void setSuicideDemande(boolean suicideDemande) {
        this.suicideDemande = suicideDemande;
    }

    public synchronized void setTraitementEnCours(Boolean traitementEnCours) {
        this.traitementEnCours = traitementEnCours;
    }

    @Override
    public void update(Observable o, Object arg) {
        // System.out.println("Couleur ia : " + couleurJoueur);
        new Thread() {
            @Override
            public void run() {
                synchronized (traitementEnCours) {
                    if (!getTraitementEnCours()
                        && moteur.getInfosPartie().getCouleurJoueurCourant().equals(couleurJoueur)
                        && !moteur.getInfosPartie().getTerrain().finPartie()) {

                        setTraitementEnCours(true);
                        final long depart = System.nanoTime();

                        try {
                            jouerCoup(moteur.getInfosPartie().getTerrain());
                        } catch (InterruptedException e) {
                            System.out.println("Exception levée : " + e.getMessage());
                            e.printStackTrace();
                        }

                        final long duree = System.nanoTime() - depart;
                        System.out.println("Ordinateur " + couleurJoueur.name() + ", nb Coup joué : " + nbPionsDeplace);
                        System.out.println("Coup joué en " + duree / 1000000000.0 + " secondes");

                        setTraitementEnCours(false);
                    }

                }
            }

        }.start();
    }

    @Override
    public Coup conseillerCoup() {
        return null;
    }
}
