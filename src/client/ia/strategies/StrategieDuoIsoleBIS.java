package client.ia.strategies;

import java.util.ArrayList;
import java.util.Comparator;

import common.CaseCoord;
import common.CouleurJoueur;
import common.Coup;
import common.Terrain;

public class StrategieDuoIsoleBIS implements Comparator<Coup> {

    private final Terrain terrain;
    private final int     couleur;

    public StrategieDuoIsoleBIS(Terrain t, CouleurJoueur c) {
        terrain = t;
        couleur = c.getValeur();
    }

    @Override
    public int compare(Coup c1, Coup c2) {
        final int colorSrcC1 = c1.getPrecedenteSource().getCouleur();
        final int colorDestC1 = c1.getPrescedenteDest().getCouleur();
        final int colorSrcC2 = c2.getPrecedenteSource().getCouleur();
        final int colorDestC2 = c2.getPrescedenteDest().getCouleur();
        int resC1 = 0;
        int resC2 = 0;

        /* Pour le coup C1
         * On regard d'abord si les deux cases sont isolées
         * 				- Si non, on renvoie 0 : la stratégie ne s'applique pas pour ce cas
         * 				- Si oui, on s'intéresse aux couleurs
         */
        if (terrain.getCasesVoisinesJouables(c1.getSourceCoord().x, c1.getSourceCoord().y).size() == 1
            && terrain.getCasesVoisinesJouables(c1.getDestCoord().x, c1.getDestCoord().y).size() == 1) {
            // Si les deux couleurs sont égales, on renvoie 0 : coup non prioritaire car n'apporte rien au joueur
            // Sinon :
            if (colorSrcC1 != colorDestC1) {
                if (colorSrcC1 == couleur) {
                    // La source est notre couleur, le coup nous crée une tour !
                    resC1 = 1;
                } else {
                    // La source est la couleur de l'adversaire : Le coup lui crée une tour !
                    resC1 = -1;
                }
            }
        } else {
            int valeurNouvelleDest = c1.getDestination().getValeurCase();
            byte couleurDest = c1.getDestination().getCouleur();

            if (couleur == couleurDest) {
                if (valeurNouvelleDest == 5)
                    resC1 = 1;
                else {
                    ArrayList<CaseCoord> casesVoisinesJouables = terrain.getCasesVoisinesJouables(c1.getSourceCoord().x,
                                                                                                  c1.getSourceCoord().y);

                    resC1 = -1;
                    for (CaseCoord c : casesVoisinesJouables) {
                        if (c.getX() == c1.getSourceCoord().x && c.getY() == c1.getSourceCoord().y)
                            continue;
                        if (c.getCouleur() != couleur && valeurNouvelleDest + c.getHauteur() > 5) {
                            resC1 = 1;
                            break;
                        }
                    }
                }
            }
        }

        /* Pour le coup C2
         * On regard d'abord si les deux cases sont isolées
         * 				- Si non, on renvoie 0 : la stratégie ne s'applique pas pour ce cas
         * 				- Si oui, on s'intéresse aux couleurs
         */
        if (terrain.getCasesVoisinesJouables(c2.getSourceCoord().x, c2.getSourceCoord().y).size() == 1
            && terrain.getCasesVoisinesJouables(c2.getDestCoord().x, c2.getDestCoord().y).size() == 1) {
            // Si les deux couleurs sont égales, on renvoie 0 : coup non prioritaire car n'apporte rien au joueur
            // Sinon :
            if (colorSrcC2 != colorDestC2) {
                if (colorSrcC2 == couleur) {
                    // La source est notre couleur, le coup nous crée une tour !
                    resC2 = 1;
                } else {
                    // La source est la couleur de l'adversaire : Le coup lui crée une tour !
                    resC2 = -1;
                }
            }
        } else {
            int valeurNouvelleDest = c2.getDestination().getValeurCase();
            byte couleurDest = c2.getDestination().getCouleur();

            if (couleur == couleurDest) {
                if (valeurNouvelleDest == 5)
                    resC2 = 1;
                else {
                    ArrayList<CaseCoord> casesVoisinesJouables = terrain.getCasesVoisinesJouables(c2.getSourceCoord().x,
                                                                                                  c2.getSourceCoord().y);

                    resC2 = -1;
                    for (CaseCoord c : casesVoisinesJouables) {
                        if (c.getX() == c2.getSourceCoord().x && c.getY() == c2.getSourceCoord().y)
                            continue;
                        if (c.getCouleur() != couleur && valeurNouvelleDest + c.getHauteur() > 5) {
                            resC2 = 1;
                            break;
                        }
                    }
                }
            }
        }

        /*
         * Après avoir évalué les deux coups séparemment, on compare les
         * résulats obtenus pour chacun et on renvoie le résultat final
         */

        int res;
        if (resC1 == resC2) {
            res = 0;
        } else if (resC1 > resC2) {
            res = 1;
        } else {
            res = -1;
        }

        return res;
    }
}
