package client.ia.strategies;

import java.util.Comparator;

import common.Coup;

/**
 * Cette stratégie permet de trier les coups en fonction des couleurs uniquement
 * on sait que jouer une tour à soi sur un adversaire ou encore jouer une tour
 * de l'adversaire sur une autre de ses tours fait perdre à l'adversaire une tour,
 * dans les autres cas, c'est le joueur qui perd un tour...
 */
public class StrategieOptimiste implements Comparator<Coup> {
    private final int couleur;

    public StrategieOptimiste(int couleur) {
        this.couleur = couleur;
    }

    @Override
    public int compare(Coup c1, Coup c2) {
        final byte couleurSourceC1 = c1.getPrecedenteSource().getCouleur();
        final byte couleurSourceC2 = c2.getPrecedenteSource().getCouleur();
        final byte couleurDestinationC1 = c1.getPrescedenteDest().getCouleur();
        final byte couleurDestinationC2 = c2.getPrescedenteDest().getCouleur();
        int scoreC1 = 0;
        int scoreC2 = 0;

        // Pour C1
        // Si le pion source est à moi
        if (couleurSourceC1 != couleur) {
            // Si le pion destination est adverse
            if (couleur != couleurDestinationC1) {
                scoreC1 = 1;
            } else {
                scoreC1 = -1;
            }
        }
        // Si le pion source est à l'adversaire
        else {
            // Si le pion destination est adverse
            if (couleurDestinationC1 != couleur) {
                scoreC1 = 1;
            } else {
                scoreC1 = -1;
            }
        }

        // Pour C2
        // Si le pion source est à moi
        if (couleurSourceC2 != couleur) {
            // Si le pion destination est adverse
            if (couleur != couleurDestinationC2) {
                scoreC2 = 1;
            } else {
                scoreC2 = -1;
            }
        }
        // Si le pion source est à l'adversaire
        else {
            // Si le pion destination est adverse
            if (couleurDestinationC2 != couleur) {
                scoreC2 = 1;
            } else {
                scoreC2 = -1;
            }
        }

        if (scoreC1 < scoreC2) {
            return -1;
        } else if (scoreC1 == scoreC2) {
            return 0;
        } else {
            return 1;
        }
    }
}
