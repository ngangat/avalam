package client.ia.strategies;

import java.util.ArrayList;
import java.util.Comparator;

import common.Case;
import common.CaseCoord;
import common.CouleurJoueur;
import common.Coup;
import common.Terrain;

public class StrategieDeplacementPrudent implements Comparator<Coup> {

    private final Terrain       terrain;
    private final CouleurJoueur couleur;

    public StrategieDeplacementPrudent(Terrain terrain, CouleurJoueur couleur) {
        this.terrain = terrain;
        this.couleur = couleur;
    }

    @Override
    public int compare(Coup c1, Coup c2) {
        final int hauteurSourceC1 = c1.getPrecedenteSource().getHauteur();
        final int hauteurSourceC2 = c2.getPrecedenteSource().getHauteur();
        final int hauteurDestinationC1 = c1.getPrescedenteDest().getHauteur();
        final int hauteurDestinationC2 = c2.getPrescedenteDest().getHauteur();
        final byte couleurSourceC1 = c1.getPrecedenteSource().getCouleur();
        final byte couleurSourceC2 = c2.getPrecedenteSource().getCouleur();
        final byte couleurDestinationC1 = c1.getPrescedenteDest().getCouleur();
        final byte couleurDestinationC2 = c2.getPrescedenteDest().getCouleur();
        int scoreC1 = 0;
        int scoreC2 = 0;
        int x = 0;

        // Pour C1
        // Si je joue un pion à moi
        if (couleurSourceC1 == couleur.getValeur()) {
            x = hauteurSourceC1 + hauteurDestinationC1;
            final ArrayList<CaseCoord> voisinsDest = terrain.getCasesVoisinesJouables(c1.getDestCoord().x,
                                                                                      c1.getDestCoord().y);
            for (int i = 0; i < voisinsDest.size(); i++) {
                final Case courante = voisinsDest.get(i);
                if (courante.getCouleur() != couleur.getValeur() && courante.getHauteur() == 5 - x) {
                    scoreC1 = -1;
                }
            }
            if (scoreC1 != -1) {
                if (couleurDestinationC1 == couleur.getValeur()) {
                    scoreC1 = 0;
                } else {
                    scoreC1 = 1;
                }
            }
            // Si je joue un pion de l'adversaire
        } else {
            scoreC1 = -1;
        }

        // Pour C2
        // Si je joue un pion à moi
        if (couleurSourceC2 == couleur.getValeur()) {
            x = hauteurSourceC2 + hauteurDestinationC2;
            final ArrayList<CaseCoord> voisinsDest = terrain.getCasesVoisinesJouables(c2.getDestCoord().x,
                                                                                      c2.getDestCoord().y);
            for (int i = 0; i < voisinsDest.size(); i++) {
                final Case courante = voisinsDest.get(i);
                if (courante.getCouleur() != couleur.getValeur() && courante.getHauteur() == 5 - x) {
                    scoreC2 = -1;
                }
            }
            if (scoreC2 != -1) {
                if (couleurDestinationC2 == couleur.getValeur()) {
                    scoreC2 = 0;
                } else {
                    scoreC2 = 1;
                }
            }
            // Si je joue un pion de l'adversaire
        } else {
            scoreC2 = -1;
        }

        if (scoreC1 < scoreC2) {
            return -1;
        } else if (scoreC1 == scoreC2) {
            return 0;
        } else {
            return 1;
        }
    }
}
