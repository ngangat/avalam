package client.ia.strategies;

import java.util.Comparator;

import common.Coup;
import common.Terrain;

/**
 * Cette stratégie classe les coups en différentes catégories:
 * 	1-Les coups qui finissent des tours à l'adversaire avec ses pions
 *  2-Les coups qui ne finissent pas de tours
 *  3-Les coups qui finissent des tours à soi avec les pions de l'adversaire
 */
public class StrategieCreationTourFinie implements Comparator<Coup> {
    private final Terrain terrain;
    private final int     couleur;

    public StrategieCreationTourFinie(Terrain terrain, int couleur) {
        this.terrain = terrain;
        this.couleur = couleur;
    }

    @Override
    public int compare(Coup c1, Coup c2) {
        final int hauteurSourceC1 = c1.getPrecedenteSource().getHauteur();
        final int hauteurSourceC2 = c2.getPrecedenteSource().getHauteur();
        final int hauteurDestinationC1 = c1.getPrescedenteDest().getHauteur();
        final int hauteurDestinationC2 = c2.getPrescedenteDest().getHauteur();
        final byte couleurSourceC1 = c1.getPrecedenteSource().getCouleur();
        final byte couleurSourceC2 = c2.getPrecedenteSource().getCouleur();
        final byte couleurDestinationC1 = c1.getPrescedenteDest().getCouleur();
        final byte couleurDestinationC2 = c2.getPrescedenteDest().getCouleur();
        int scoreC1 = 0;
        int scoreC2 = 0;

        // Pour C1
        // Si les pions sont adverses
        if (couleurSourceC1 != couleurDestinationC1) {
            // Si c'est moi qui suis gagnant
            if (couleurSourceC1 == couleur) {
                // Si je fais une tour de 5
                if (hauteurSourceC1 + hauteurDestinationC1 == 5) {
                    scoreC1 = 1;
                }
                // Si je fais une tour isolée
                else if (terrain.getCasesVoisinesJouables(c1.getDestCoord().x, c1.getDestCoord().y).size() == 0) {
                    scoreC1 = 1;
                }
                // Si je ne fais pas de tour à 5 ou de tour isolée
                else {
                    scoreC1 = 0;
                }
                // Si c'est l'adversaire qui est gagnant
            } else {
                // Si il fait une tour de 5
                if (hauteurSourceC1 + hauteurDestinationC1 == 5) {
                    scoreC1 = -1;
                }
                // Si il fait une tour isolée
                else if (terrain.getCasesVoisinesJouables(c1.getDestCoord().x, c1.getDestCoord().y).size() == 0) {
                    scoreC1 = -1;
                }
                // Si il ne fait pas de tour à 5 ou de tour isolée
                else {
                    scoreC1 = 0;
                }
            }
        } else {
            scoreC1 = 0;
        }

        // Pour C2
        // Si les pions sont adverses
        if (couleurSourceC2 != couleurDestinationC2) {
            // Si c'est moi qui suis gagnant
            if (couleurSourceC2 == couleur) {
                // Si je fais une tour de 5
                if (hauteurSourceC2 + hauteurDestinationC2 == 5) {
                    scoreC2 = 1;
                }
                // Si je fais une tour isolée
                else if (terrain.getCasesVoisinesJouables(c2.getDestCoord().x, c2.getDestCoord().y).size() == 0) {
                    scoreC2 = 1;
                }
                // Si je ne fais pas de tour à 5 ou de tour isolée
                else {
                    scoreC2 = 0;
                }
                // Si c'est l'adversaire qui est gagnant
            } else {
                // Si il fait une tour de 5
                if (hauteurSourceC2 + hauteurDestinationC2 == 5) {
                    scoreC2 = -1;
                }
                // Si il fait une tour isolée
                else if (terrain.getCasesVoisinesJouables(c2.getDestCoord().x, c2.getDestCoord().y).size() == 0) {
                    scoreC2 = -1;
                }
                // Si il ne fait pas de tour à 5 ou de tour isolée
                else {
                    scoreC2 = 0;
                }
            }
        } else {
            scoreC2 = 0;
        }

        if (scoreC1 < scoreC2) {
            return -1;
        } else if (scoreC1 == scoreC2) {
            return 0;
        } else {
            return 1;
        }
    }
}
