package client.ia.strategies;

import java.util.Comparator;

import common.CouleurJoueur;
import common.Coup;
import common.Terrain;

public class StrategieNiveauRapideBis implements Comparator<Coup> {
    private final Comparator<Coup> strat1;
    private final Comparator<Coup> strat2;
    private final Comparator<Coup> strat3;
    private final Comparator<Coup> strat4;

    public StrategieNiveauRapideBis(Terrain terrain, CouleurJoueur couleur) {
        // Déclaration des différentes stratégies par ordre d'importance
        // Pour changer l'ordre : Effectuer changements ici

        strat4 = new StrategieOptimiste(couleur.getValeur());
        strat3 = new StrategieCreationTourFinie(terrain, couleur.getValeur());
        strat2 = new StrategieDeplacementPrudent(terrain, couleur);
        strat1 = new StrategieDuoIsole(terrain, couleur);
    }

    /* On utilise les compare de chaque stratégie :
     * 			- Si le résultat est 0, on appelle la stratégie suivante et ainsi de suite
     * 			- Sinon, on renvoie le résultat (donc 1 ou -1) 
     */
    @Override
    public int compare(Coup c1, Coup c2) {
        int tempScore = strat1.compare(c1, c2);
        if (tempScore != 0) {
            return tempScore;
        } else {
            tempScore = strat2.compare(c1, c2);
            if (tempScore != 0) {
                return tempScore;
            } else {
                tempScore = strat3.compare(c1, c2);
                if (tempScore != 0) {
                    return tempScore;
                } else {
                    tempScore = strat4.compare(c1, c2);
                    if (tempScore != 0) {
                        return tempScore;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }
}
