/**
 * 
 */
package client.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import client.reseaux.MoteurReseaux;

import common.messages.Message;

/**
 * @author ngangat
 *
 */
public class TestMoteurReseaux {
    private MoteurReseaux m;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        m = new MoteurReseaux();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test method for {@link client.reseaux.MoteurReseaux#initConnexion(java.lang.String, int)}.
     * @throws IOException 
     * @throws ConnectException 
     */

    /**
     * Test method for {@link client.reseaux.MoteurReseaux#extraireMessageAEnvoyer()}.
     */
    @Test
    public void testExtraireMessageAEnvoyerCorrect() {
        final int t = m.TailleMessagerie();
        //Message mes = m.extraireMessageAEnvoyer(); 
        Assert.assertEquals(m.TailleMessagerie(), t - 1);
        //assertEquals(m.extraireMessageAEnvoyer(), mes);
    }

    @Test
    public void testExtraireMessageAEnvoyerNull() {
        final int t = m.TailleMessagerie();
        final Message mes = m.extraireMessageAEnvoyer();
        Assert.assertEquals(m.TailleMessagerie(), t - 1);
        Assert.assertEquals(mes, null);
    }

    /*@Test
    public void testFermerConnexion() throws IOException {
        boolean fermeture = false;
        m.fermerConnexion();
        assertEquals(m.getSocket().isClosed(), fermeture);
    }
    */
    /**
     * Test method for {@link client.reseaux.MoteurReseaux#traiterMessage(common.Message)}.
     */
    @Test
    public void testTraiterMessage() {
        Assert.fail("Not yet implemented");
    }

}
