package client.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import client.ia.strategies.StrategieDuoIsole;

import common.Case;
import common.CouleurJoueur;
import common.Coup;
import common.TerrainMatrice;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public class TestStrategieDuoIsole {

    StrategieDuoIsole s1;
    StrategieDuoIsole s2;
    StrategieDuoIsole s3;

    TerrainMatrice    t1;
    TerrainMatrice    t2;
    TerrainMatrice    t3;

    byte[][]          ex1 = { { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.B, Case.L, Case.N, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.N, Case.L, Case.B, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L } };

    byte[][]          ex2 = { { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.B, Case.L, Case.B, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.N, Case.L, Case.N, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.B, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L } };

    byte[][]          ex3 = { { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.B, Case.L, Case.N, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.N, Case.L, Case.B, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.B, Case.L, Case.N, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L } };

    @Before
    public void setUp() throws Exception {
        // Initialisation des TerrainMatrice
        t1 = new TerrainMatrice();
        t2 = new TerrainMatrice();
        t3 = new TerrainMatrice();

        // Remplacement des matrices par les exemples d�finis pr�c�demment
        t1.setNouvelleConfiguration(ex1);
        t2.setNouvelleConfiguration(ex2);
        t3.setNouvelleConfiguration(ex3);

        // Ajout des exemples dans les strat�gies
        s1 = new StrategieDuoIsole(t1, CouleurJoueur.BLANC);
        s2 = new StrategieDuoIsole(t2, CouleurJoueur.BLANC);
        s3 = new StrategieDuoIsole(t3, CouleurJoueur.BLANC);
    }

    @After
    public void tearDown() throws Exception {
        t1 = null;
        t2 = null;
        t3 = null;

        s1 = null;
        s2 = null;
        s3 = null;
    }

    @Test
    public void testCasNumeroDeux() throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {
        final Coup c1 = new Coup(1, 2, 1, 3, t2);
        final Coup c2 = new Coup(3, 1, 3, 2, t2);

        Assert.assertEquals(s2.compare(c1, c2), -1);
    }

    @Test
    public void testCasNumeroTrois() throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {
        final Coup c1 = new Coup(1, 1, 1, 2, t3);
        final Coup c2 = new Coup(3, 1, 3, 2, t3);

        System.out.println("Nb voisins Src c1 -> " + t3.getCasesVoisinesJouables(1, 1).size());
        System.out.println("Nb voisins Dest c1 -> " + t3.getCasesVoisinesJouables(1, 2).size());
        System.out.println("Nb voisins Src c2 -> " + t3.getCasesVoisinesJouables(3, 1).size());
        System.out.println("Nb voisins Dest c2 -> " + t3.getCasesVoisinesJouables(3, 2).size());

        Assert.assertEquals(s3.compare(c1, c2), 0);
    }

    @Test
    public void testCasNumeroUn() throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException, TourTropGrandeException, HorsDuTerrainException {

        final Coup c1 = new Coup(1, 1, 1, 2, t1);
        final Coup c2 = new Coup(3, 1, 3, 2, t1);

        Assert.assertEquals(s1.compare(c1, c2), 1);
    }

}
