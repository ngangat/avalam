package client.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import client.ia.strategies.StrategieDeplacementPrudent;

import common.Case;
import common.CouleurJoueur;
import common.Coup;
import common.Terrain;
import common.TerrainMatrice;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

/**
 * TEST OK
 */
public class TestStrategieDeplacementPrudent {

    StrategieDeplacementPrudent s1;
    StrategieDeplacementPrudent s2;

    Terrain                     ter1;
    Terrain                     ter2;

    byte[][]                    t1 = { { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L } };

    byte[][]                    t2 = { { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L, Case.L } };

    @Before
    public void setUp() throws Exception {
        ter1 = new TerrainMatrice();
        ter1.setNouvelleConfiguration(t1);

        ter2 = new TerrainMatrice();
        ter2.setNouvelleConfiguration(t2);

        final Case blanc1 = new Case(Case.B);
        final Case blanc2 = new Case(Case.B);
        final Case noir1 = new Case(Case.N);
        final Case noir2 = new Case(Case.N);

        noir2.augmenterHauteur(1);
        blanc2.augmenterHauteur(1);

        ter1.setCase(1, 1, blanc2);
        ter1.setCase(1, 2, blanc1);
        ter1.setCase(2, 1, blanc2);
        ter1.setCase(2, 2, noir2);
        ter1.setCase(3, 2, noir2);
        ter1.setCase(4, 2, noir1);
        ter1.setCase(3, 3, blanc2);
        ter1.setCase(4, 3, noir2);

        ter2.setCase(0, 0, blanc1);
        ter2.setCase(0, 1, blanc2);
        ter2.setCase(1, 1, noir2);
        ter2.setCase(1, 2, noir2);

        s1 = new StrategieDeplacementPrudent(ter1, CouleurJoueur.BLANC);
        s2 = new StrategieDeplacementPrudent(ter2, CouleurJoueur.NOIR);
    }

    @After
    public void tearDown() throws Exception {
        s1 = null;
        s2 = null;
        ter1 = null;
        ter2 = null;
    }

    @Test
    public void testTerrain1() {
        Coup meilleurCoup1 = null;
        Coup meilleurCoup2 = null;
        Coup moyenCoup1 = null;
        Coup moyenCoup2 = null;
        Coup mauvaisCoup1 = null;
        Coup mauvaisCoup2 = null;
        Coup mauvaisCoup3 = null;
        Coup mauvaisCoup4 = null;
        try {
            meilleurCoup1 = new Coup(2, 1, 2, 2, ter1);
            meilleurCoup2 = new Coup(3, 3, 2, 2, ter1);
            moyenCoup1 = new Coup(2, 1, 1, 1, ter1);
            moyenCoup2 = new Coup(1, 1, 2, 1, ter1);
            mauvaisCoup1 = new Coup(2, 1, 3, 2, ter1);
            mauvaisCoup2 = new Coup(3, 2, 4, 2, ter1);
            mauvaisCoup3 = new Coup(3, 3, 4, 2, ter1);
            mauvaisCoup4 = new Coup(3, 3, 4, 3, ter1);
        } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                 | TourTropGrandeException | HorsDuTerrainException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(1, s1.compare(meilleurCoup1, moyenCoup1));
        Assert.assertEquals(1, s1.compare(meilleurCoup1, moyenCoup2));
        Assert.assertEquals(-1, s1.compare(moyenCoup1, meilleurCoup1));
        Assert.assertEquals(0, s1.compare(meilleurCoup1, meilleurCoup2));
        Assert.assertEquals(1, s1.compare(moyenCoup1, mauvaisCoup1));
        Assert.assertEquals(1, s1.compare(moyenCoup2, mauvaisCoup3));
        Assert.assertEquals(-1, s1.compare(mauvaisCoup4, meilleurCoup1));
        Assert.assertEquals(1, s1.compare(moyenCoup1, mauvaisCoup4));
        Assert.assertEquals(1, s1.compare(meilleurCoup2, mauvaisCoup2));
        Assert.assertEquals(0, s1.compare(mauvaisCoup1, mauvaisCoup4));

    }

    @Test
    public void testTerrain2() {
        Coup meilleur = null;
        Coup mauvais = null;
        try {
            meilleur = new Coup(1, 1, 1, 2, ter2);
            mauvais = new Coup(1, 1, 0, 0, ter2);
        } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                 | TourTropGrandeException | HorsDuTerrainException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(1, s2.compare(meilleur, mauvais));
    }
}
