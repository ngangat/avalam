package client.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import client.ia.strategies.StrategieCreationTourFinie;

import common.Case;
import common.CouleurJoueur;
import common.Coup;
import common.Terrain;
import common.TerrainMatrice;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

/**
 * TEST OK
 */
public class TestStrategieCreationTourFinie {

    StrategieCreationTourFinie s1;
    StrategieCreationTourFinie s2;

    Terrain                    ter1;
    Terrain                    ter2;

    byte[][]                   t1 = { { Case.N, Case.B, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L } };

    byte[][]                   t2 = { { Case.N, Case.B, Case.L, Case.L, Case.L },
        { Case.N, Case.B, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L } };

    @Before
    public void setUp() throws Exception {
        ter1 = new TerrainMatrice();
        ter2 = new TerrainMatrice();
        ter1.setNouvelleConfiguration(t1);
        ter2.setNouvelleConfiguration(t2);
        final Case caseNoir4 = new Case(Case.N);
        final Case caseNoir3 = new Case(Case.N);
        caseNoir3.augmenterHauteur(2);
        caseNoir4.augmenterHauteur(3);
        ter2.setCase(0, 0, caseNoir4);
        ter2.setCase(0, 1, caseNoir3);
        ter1.setCase(0, 0, caseNoir4);

        s1 = new StrategieCreationTourFinie(ter1, CouleurJoueur.BLANC.getValeur());
        s2 = new StrategieCreationTourFinie(ter2, CouleurJoueur.BLANC.getValeur());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testTerrain1() {
        Coup c1 = null;
        Coup c2 = null;
        try {
            c1 = new Coup(0, 0, 1, 0, ter1);
        } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                 | TourTropGrandeException | HorsDuTerrainException e) {
            e.printStackTrace();
        }
        try {
            c2 = new Coup(1, 0, 0, 0, ter1);
        } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                 | TourTropGrandeException | HorsDuTerrainException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(-1, s1.compare(c1, c2));
        Assert.assertEquals(1, s1.compare(c2, c1));

    }

    @Test
    public void testTerrain2() {
        Coup c1 = null;
        Coup c2 = null;
        Coup c3 = null;
        Coup c4 = null;
        try {
            c1 = new Coup(1, 0, 0, 1, ter2); // Blanc-Noir pas tour de 5
            c2 = new Coup(1, 0, 0, 0, ter2); // Blanc-Noir tour de 5
            c3 = new Coup(0, 0, 1, 0, ter2); // Noir-Blanc tour de 5
            c4 = new Coup(1, 0, 1, 1, ter2); // Blanc-Blanc tour de 5
        } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                 | TourTropGrandeException | HorsDuTerrainException e) {
            e.printStackTrace();
        }

        System.out.println("Source:");
        Case source = null;
        try {
            source = ter2.getCase(1, 0);
        } catch (final HorsDuTerrainException e) {
        }
        System.out.println("couleur: " + source.getCouleur() + " hauteur: " + source.getHauteur());

        System.out.println("Noir4:");
        Case noir4 = null;
        try {
            noir4 = ter2.getCase(0, 0);
        } catch (final HorsDuTerrainException e) {
        }
        System.out.println("couleur: " + noir4.getCouleur() + " hauteur: " + noir4.getHauteur());

        System.out.println("Noir3:");
        Case noir3 = null;
        try {
            noir3 = ter2.getCase(0, 1);
        } catch (final HorsDuTerrainException e) {
        }
        System.out.println("couleur: " + noir3.getCouleur() + " hauteur: " + noir3.getHauteur());

        Assert.assertEquals(-1, s2.compare(c1, c2));
        Assert.assertEquals(1, s2.compare(c2, c1));
        Assert.assertEquals(1, s2.compare(c4, c3));

    }

}
