package client.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import client.ia.strategies.StrategieOptimiste;

import common.Case;
import common.CouleurJoueur;
import common.Coup;
import common.Terrain;
import common.TerrainMatrice;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

/**
 * TEST OK
 */
public class TestStrategieOptimiste {

    StrategieOptimiste s1;

    Terrain            ter1;

    byte[][]           t1 = { { Case.B, Case.N, Case.L, Case.L, Case.L }, { Case.N, Case.B, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L }, { Case.L, Case.L, Case.L, Case.L, Case.L },
        { Case.L, Case.L, Case.L, Case.L, Case.L } };

    @Before
    public void setUp() throws Exception {
        ter1 = new TerrainMatrice();
        ter1.setNouvelleConfiguration(t1);

        s1 = new StrategieOptimiste(CouleurJoueur.BLANC.getValeur());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testTerrain1() {
        Coup c1 = null;
        Coup c2 = null;
        Coup c3 = null;
        Coup c4 = null;

        try {
            c1 = new Coup(0, 0, 1, 0, ter1); //Blanc-Noir
            c2 = new Coup(0, 0, 1, 1, ter1); //Blanc-Blanc
            c3 = new Coup(1, 0, 1, 1, ter1); //Noir-Blanc
            c4 = new Coup(1, 0, 0, 1, ter1); //Noir-Noir
        } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                 | TourTropGrandeException | HorsDuTerrainException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(1, s1.compare(c1, c2));
        Assert.assertEquals(0, s1.compare(c2, c3));
        Assert.assertEquals(-1, s1.compare(c3, c4));
        Assert.assertEquals(-1, s1.compare(c2, c4));

    }
}
