package client.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import client.MoteurLeger;

import common.Coup;
import common.Terrain;
import common.TerrainMatrice;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public class TestMoteurLeger {
    private MoteurLeger ml;
    private Terrain     t;

    @Before
    public void setUp() throws Exception {
        t = new TerrainMatrice();
        ml = new MoteurLeger();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testJouerCoup() throws TourTropGrandeException, CaseSourceVideException, AjoutSurCaseVideException,
        CasesNonAdjacentesException, HorsDuTerrainException {
    }

    @Test(expected = CasesNonAdjacentesException.class)
    public void testJouerCoupCasesNonAdjacentes() throws TourTropGrandeException, CaseSourceVideException,
        AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final int srcX = 1;
        final int srcY = 1;
        final int destX = 3;
        final int destY = 3;

        final Coup coup = new Coup(srcX, srcY, destX, destY, t);
        ml.jouerCoup(coup);
    }

    @Test(expected = HorsDuTerrainException.class)
    public void testJouerCoupHorsTerrain() throws NullPointerException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final int srcX = -5;
        final int srcY = -5;
        final int destX = -4;
        final int destY = -4;

        final Coup coup = new Coup(srcX, srcY, destX, destY, t);
        ml.jouerCoup(coup);
    }

    @Test(expected = NullPointerException.class)
    public void testJouerCoupTerrainNull() throws NullPointerException, TourTropGrandeException,
        CaseSourceVideException, AjoutSurCaseVideException, CasesNonAdjacentesException, HorsDuTerrainException {
        final Coup coup = new Coup(5, 5, 4, 4, null);
        ml.jouerCoup(coup);
    }

}
