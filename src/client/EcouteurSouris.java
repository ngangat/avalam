package client;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Observable;

import common.Case;
import common.Coup;
import common.Terrain;
import common.TerrainGraphique;
import common.TypeJoueur;
import common.TypePartie;

import exceptions.AjoutSurCaseVideException;
import exceptions.CaseIsoleeException;
import exceptions.CaseSourcePleineException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.TourTropGrandeException;

public class EcouteurSouris extends Observable implements MouseListener, MouseMotionListener {
    private final TerrainGraphique plateau;
    private final Terrain          terrain;
    private final MoteurLeger      moteurLeger;
    private Point                  source;
    boolean                        selected;

    public EcouteurSouris(Terrain t, TerrainGraphique p, MoteurLeger m) {
        terrain = t;
        plateau = p;
        moteurLeger = m;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (moteurLeger.getInfosPartie().getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain)) {
            if (!selected) {
                return;
            }
            plateau.setPositionSouris(e.getPoint());
            plateau.repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("ecouteur souris : joueur humain ? "
            + moteurLeger.getInfosPartie().getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain));

        final boolean peutJouerLocal = moteurLeger.getInfosPartie().getTypePartie().equals(TypePartie.LOCAL)
            && moteurLeger.getInfosPartie().getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain);
        final boolean peutJouerDistant = moteurLeger.getInfosPartie().getTypePartie().equals(TypePartie.DISTANT)
            && moteurLeger.getInfosPartie().getCouleurJoueurCourant()
                          .equals(moteurLeger.getInfosPartie().getCouleurJoueurClient());
        if (peutJouerLocal || peutJouerDistant) {
            final int x = e.getPoint().x / plateau.getLargeurCase();
            final int y = e.getPoint().y / plateau.getHauteurCase();

            try {
                final Case caseSelectionnee = terrain.getCase(x, y);

                if (caseSelectionnee.getHauteur() == 0) {
                    throw new CaseSourceVideException();
                }

                if (caseSelectionnee.getHauteur() == 5) {
                    throw new CaseSourcePleineException();
                }

                if (terrain.getCasesVoisinesJouables(x, y).size() == 0) {
                    throw new CaseIsoleeException();
                }

                source = new Point(x, y);

                plateau.setSource(source);
                plateau.setPositionSouris(e.getPoint());
                plateau.setSelected(true);
                selected = true;
                plateau.repaint();
            } catch (CaseSourceVideException | HorsDuTerrainException | CaseSourcePleineException | CaseIsoleeException ex) {
                System.out.println("pas de Tours jouables ici ! ");
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        final boolean peutJouerLocal = moteurLeger.getInfosPartie().getTypePartie().equals(TypePartie.LOCAL)
            && moteurLeger.getInfosPartie().getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain);
        final boolean peutJouerDistant = moteurLeger.getInfosPartie().getTypePartie().equals(TypePartie.DISTANT)
            && moteurLeger.getInfosPartie().getCouleurJoueurCourant()
                          .equals(moteurLeger.getInfosPartie().getCouleurJoueurClient());
        if (peutJouerLocal || peutJouerDistant) {
            if (!selected) {
                return;
            }
            final int x = e.getPoint().x / plateau.getLargeurCase();
            final int y = e.getPoint().y / plateau.getHauteurCase();

            try {
                final Case caseSelectionnee = terrain.getCase(x, y);

                if (caseSelectionnee.getHauteur() == 0) {
                    throw new CaseSourceVideException();
                }

                moteurLeger.jouerCoup(new Coup(source.x, source.y, x, y, terrain));
                System.out.println("Ecouteur souris : coup joué !");

            } catch (CaseSourceVideException | HorsDuTerrainException | CasesNonAdjacentesException
                     | AjoutSurCaseVideException | TourTropGrandeException ex) {
                System.out.println("Coup non jouable ! ");
            }

            selected = false;
            plateau.setSelected(false);
            plateau.repaint();
        }
    }

}
