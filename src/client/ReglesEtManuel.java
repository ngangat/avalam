package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ReglesEtManuel extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = -4171183958908469542L;
    private JEditorPane       jep;

    public ReglesEtManuel(int i) {

        JPanel panel = new JPanel();

        try {

            File file = new File("html/reglesdujeu.html");
            URL u = file.toURI().toURL();
            URL url = new URL(u, "#manuel");
            jep = new JEditorPane(url);
            jep.setContentType("text/html");

            jep.setEditable(false);
            panel.add(jep);

            /*jep.addHyperlinkListener(new HyperlinkListener() {
            	public void hyperlinkUpdate(HyperlinkEvent hev) {
            	
            	
            	try {
            		jep.setPage(hev.getURL());
            	} catch (IOException e) {
            		// TODO Auto-generated catch block
            		e.printStackTrace();
            	}
            	
            }});*/

        } catch (Exception e1) {
            e1.printStackTrace();
        }

        JScrollPane scroll = new JScrollPane(jep);
        this.setSize(600, 700);
        this.getContentPane().add(scroll, BorderLayout.CENTER);
        setVisible(true);
        jep.setBackground(Color.black);
        setTitle("Règle du jeu");
    }

}
