package client;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.naming.MalformedLinkException;
import javax.swing.JOptionPane;

import client.ia.IA;
import client.ia.NiveauDifficile;
import client.ia.NiveauFacile;
import client.ia.NiveauMoyen;
import client.reseaux.MoteurReseaux;
import client.window.FenetrePrincipale;
import common.CouleurJoueur;
import common.Coup;
import common.Historique;
import common.InfosPartie;
import common.Joueur;
import common.TerrainGraphique;
import common.TerrainMatrice;
import common.TypeJoueur;
import common.TypePartie;
import common.messages.Message;
import common.messages.MessageBroadcastAnnulationCoup;
import common.messages.MessageBroadcastRefaireCoup;
import common.messages.MessageConfirmationAnnulationCoup;
import common.messages.MessageCoup;
import common.messages.MessageDemandeAnnulationCoup;
import common.messages.MessageDemandeConfirmationAnnulationCoup;
import common.messages.MessageNom;
import common.messages.MessageRefaireCoup;
import exceptions.AjoutSurCaseVideException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.HorsDuTerrainException;
import exceptions.MauvaisChoixDeCouleurException;
import exceptions.TourTropGrandeException;

public class MoteurLeger {

    private MoteurReseaux    moteurReseau;

    private InfosPartie      infosPartie;

    private AvalamClient     avalamClient;

    private TerrainGraphique plateau;

    private boolean isCoupNouveau = false;
    
    public MoteurLeger() {
    }

    public void annulerCoup() {
        if (infosPartie.getTypePartie().equals(TypePartie.DISTANT)) {
            annulerCoupDistant();
        } else {
            annulerCoupLocal();
        }
    }

    private void annulerCoupDistant() {
        final Message messageDemandeAnnulationCoup = new MessageDemandeAnnulationCoup(infosPartie.getTypePartie());
        moteurReseau.ajouterMessageAEnvoyer(messageDemandeAnnulationCoup);
    }

    private void annulerCoupLocal() {
        try {
            lockBoutonsControle();
            Coup coup = infosPartie.getHistorique().getDernierCoup();

            plateau.animerCoup(coup, true);
            while (plateau.isAnim());

            infosPartie.getTerrain().annulerCoup(coup);
            infosPartie.getHistorique().annulerCoup();

            boolean auMoinsUneIA = !infosPartie.getJoueurs()[0].getTypeJoueur().equals(TypeJoueur.Humain)
                || !infosPartie.getJoueurs()[1].getTypeJoueur().equals(TypeJoueur.Humain);

            if (auMoinsUneIA) {
                coup = infosPartie.getHistorique().getDernierCoup();

                plateau.animerCoup(coup, true);
                while (plateau.isAnim());

                infosPartie.getTerrain().annulerCoup(coup);
                infosPartie.getHistorique().annulerCoup();
            } else
                infosPartie.changerJoueurCourant();

            updateBoutonsControle();
        } catch (final Exception e) {
        }
    }

    private void updateBoutonsControle() {
        if (infosPartie.getTypePartie().equals(TypePartie.LOCAL))
            updateBoutonsConstroleLocal();
        else
            updateBoutonsControleDistant();
    }

    private void updateBoutonsConstroleLocal() {
        boolean annulerPossible = infosPartie.getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain)
            && infosPartie.getHistorique().annulerPossible();
        boolean refairePossible = infosPartie.getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain)
            && infosPartie.getHistorique().refairePossible();
        boolean conseilPossible = infosPartie.getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain)
            && FenetrePrincipale.getInstance().isConseilPossible();

        annulerPossible = annulerPossible
            && ((!infosPartie.getJoueurs()[0].getTypeJoueur().equals(infosPartie.getJoueurs()[1].getTypeJoueur()) && infosPartie.getHistorique()
                                                                                                                                .getIt()
                                                                                                                                .nextIndex() >= 2) || infosPartie.getJoueurs()[0].getTypeJoueur()
                                                                                                                                                                                 .equals(infosPartie.getJoueurs()[1].getTypeJoueur()));

        FenetrePrincipale.getInstance().getPanelControle().activerAnnuler(annulerPossible);
        FenetrePrincipale.getInstance().getPanelControle().activerRefaire(refairePossible);
        FenetrePrincipale.getInstance().getPanelControle().activerConseil(conseilPossible);
        FenetrePrincipale.getInstance().activerAnnuler(annulerPossible);
        FenetrePrincipale.getInstance().activerRefaire(refairePossible);
    }

    private void updateBoutonsControleDistant() {
        boolean annulerPossible = !infosPartie.getCouleurJoueurClient().equals(infosPartie.getJoueurCourant().getCouleur());
        boolean refairePossible = infosPartie.getCouleurJoueurClient().equals(infosPartie.getJoueurCourant().getCouleur()) && !isCoupNouveau;

        FenetrePrincipale.getInstance().getPanelControle().activerConseil(false);
        FenetrePrincipale.getInstance().getPanelControle().activerAnnuler(annulerPossible);
        FenetrePrincipale.getInstance().getPanelControle().activerRefaire(refairePossible);
    }

    private void lockBoutonsControle() {
        FenetrePrincipale.getInstance().getPanelControle().activerAnnuler(false);
        FenetrePrincipale.getInstance().getPanelControle().activerRefaire(false);
        FenetrePrincipale.getInstance().getPanelControle().activerConseil(false);
        FenetrePrincipale.getInstance().activerAnnuler(false);
        FenetrePrincipale.getInstance().activerRefaire(false);
    }

    public void demanderConfirmationAnnulation(MessageDemandeConfirmationAnnulationCoup message)
        throws MalformedLinkException {
        MessageConfirmationAnnulationCoup reponse;
        boolean isOk = true;

        if (infosPartie.getTypePartie().equals(TypePartie.DISTANT)) {
            /*
             * Dialogue de demande pour le jeu en réseau
             */

            final int res = JOptionPane.showConfirmDialog(FenetrePrincipale.getInstance(),
                                                          "Votre adversaire veut annuler son coup. L'autorisez vous ?",
                                                          "Demande de confirmation d'annulation",
                                                          JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            isOk = res == JOptionPane.YES_OPTION;
        }

        reponse = new MessageConfirmationAnnulationCoup(message.getCoup(), isOk);
        moteurReseau.ajouterMessageAEnvoyer(reponse);
    }

    public void envoyerNom(String j1) {
        try {
            moteurReseau.ajouterMessageAEnvoyer(new MessageNom(j1));
        } catch (final MalformedLinkException e) {
            e.printStackTrace();
        }

    }

    public AvalamClient getAvalamClient() {
        return avalamClient;
    }

    public InfosPartie getInfosPartie() {
        return infosPartie;
    }

    public MoteurReseaux getMoteurReseau() {
        return moteurReseau;
    }

    public void initInfosPartieDistante(InfosPartie infosPartieDistante) {
        //        System.out.println("ajout du terrain");
        infosPartie.setTerrain(infosPartieDistante.getTerrain());
        //        System.out.println("ajout du type de partie");
        infosPartie.setTypePartie(TypePartie.DISTANT);
        //        System.out.println("suppressions des anciens joueurs");
        infosPartie.detruireJoueur();
        //        System.out.println("ajout des nouveaux joueurs");
        infosPartie.setJoueurs(infosPartieDistante.getJoueurs());
        //        System.out.println("ajout de l'historique");
        infosPartie.setHistorique(infosPartieDistante.getHistorique());
        avalamClient.initEvent();
        //        System.out.println("ajout du joueur courant");
        infosPartie.setCouleurJoueurCourant(infosPartieDistante.getCouleurJoueurCourant(), true);
    }

    public void jouerCoup(Coup coup) throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException {
        if (infosPartie.getTypePartie().equals(TypePartie.LOCAL)) {
            jouerCoupLocal(coup);
        } else {
            jouerCoupDistant(coup);
        }

        updateBoutonsControle();
    }

    private void jouerCoupDistant(Coup coup) throws CasesNonAdjacentesException, CaseSourceVideException,
        AjoutSurCaseVideException {
        try {
            infosPartie.getTerrain().effectuerCoup(coup, true);
            infosPartie.changerJoueurCourant(); // change la couleur du joueur courant...
            final Message messageCoup = new MessageCoup(coup);
            moteurReseau.ajouterMessageAEnvoyer(messageCoup);
        } catch (TourTropGrandeException | HorsDuTerrainException e) {

        }

    }

    private void jouerCoupLocal(Coup coup) {
        if (coup == null) {
            throw new NullPointerException("Un coup ne peut être envoyé s'il n'est pas d'abord instancié");
        }
        //System.out.println("Coup à jouer :  " + coup);
        try {
            if (!infosPartie.getJoueurCourant().getTypeJoueur().equals(TypeJoueur.Humain)) {
                boolean deuxRobots = !infosPartie.getJoueurs()[0].getTypeJoueur().equals(TypeJoueur.Humain)
                    && !infosPartie.getJoueurs()[1].getTypeJoueur().equals(TypeJoueur.Humain);
                if (!deuxRobots) {
                    plateau.animerCoup(coup, false);
                    while (plateau.isAnim());
                }
                infosPartie.getTerrain().effectuerCoup(coup, false);
            } else {
                infosPartie.getTerrain().effectuerCoup(coup, true);
            }

            infosPartie.getHistorique().ajouterNouveauCoup(coup);
        } catch (TourTropGrandeException | HorsDuTerrainException | CasesNonAdjacentesException
                 | CaseSourceVideException | AjoutSurCaseVideException e) {
            e.printStackTrace();
        }
        infosPartie.changerJoueurCourant(); // change la couleur du joueur courant...

    }

    public void nouvellePartieDistante(String hote, int port, String nomJoueur) throws UnknownHostException,
        IOException {
        moteurReseau.nouvelleConnexionDistante(hote, port, nomJoueur);
        lockBoutonsControle();
    }

    public void nouvellePartieLocal(String nomJoueur1, String nomJoueur2, boolean joueur1Commence, TypeJoueur typeJ2) {

        if (infosPartie == null) {
            infosPartie = new InfosPartie();
        } else {
            infosPartie.deleteObservers();
            infosPartie = new InfosPartie();
        }

        final Joueur[] joueurs = new Joueur[2];
        try {
            final int indiceJ1 = joueur1Commence ? 0 : 1;
            final int indiceJ2 = !joueur1Commence ? 0 : 1;
            final CouleurJoueur couleurJ1 = joueur1Commence ? CouleurJoueur.BLANC : CouleurJoueur.NOIR;
            final CouleurJoueur couleurJ2 = !joueur1Commence ? CouleurJoueur.BLANC : CouleurJoueur.NOIR;

            joueurs[indiceJ1] = new Joueur(TypeJoueur.Humain, nomJoueur1, couleurJ1);
            if (typeJ2.equals(TypeJoueur.Humain)) {
                joueurs[indiceJ2] = new Joueur(typeJ2, nomJoueur2, couleurJ2);
            } else if (typeJ2.equals(TypeJoueur.Facile)) {
                joueurs[indiceJ2] = new NiveauFacile(couleurJ2, typeJ2, this);
                joueurs[indiceJ2].setNom(nomJoueur2);
            } else if (typeJ2.equals(TypeJoueur.Moyen)) {
                joueurs[indiceJ2] = new NiveauMoyen(couleurJ2, typeJ2, this);
                joueurs[indiceJ2].setNom(nomJoueur2);
            } else {
                joueurs[indiceJ2] = new NiveauDifficile(couleurJ2, typeJ2, this);
                joueurs[indiceJ2].setNom(nomJoueur2);
            }

            // TESTS IA ///////////////////////////////////////////////////////////////

            //            joueurs[indiceJ1] = new NiveauDifficile(couleurJ1, TypeJoueur.Difficile, this);
            //            joueurs[indiceJ1].setNom("Wall-E");
            //
            //            joueurs[indiceJ2] = new NiveauMoyen(couleurJ2, TypeJoueur.Difficile, this);
            //            joueurs[indiceJ2].setNom("Nono");

            ///////////////////////////////////////////////////////////////////////////
        } catch (final MauvaisChoixDeCouleurException e) {
            e.printStackTrace();
        }

        infosPartie.setTerrain(new TerrainMatrice());
        infosPartie.setTypePartie(TypePartie.LOCAL);

        infosPartie.detruireJoueur();
        infosPartie.setJoueurs(joueurs);
        infosPartie.setHistorique(new Historique());

        avalamClient.initEvent();
    }

    public void refaireCoup() {
        if (infosPartie.getTypePartie().equals(TypePartie.DISTANT))
            refaireCoupDistant();
        else
            refaireCoupLocal();
    }

    public void refaireCoupLocal() {
        try {
            lockBoutonsControle();

            boolean auMoinsUneIA = !infosPartie.getJoueurs()[0].getTypeJoueur().equals(TypeJoueur.Humain)
                || !infosPartie.getJoueurs()[1].getTypeJoueur().equals(TypeJoueur.Humain);

            Coup coupARefaire = infosPartie.getHistorique().getProchainCoup();

            plateau.animerCoup(coupARefaire, false);
            while (plateau.isAnim());

            infosPartie.getTerrain().effectuerCoup(coupARefaire, true);
            infosPartie.getHistorique().refaireCoup();

            if (auMoinsUneIA) {
                coupARefaire = infosPartie.getHistorique().getProchainCoup();

                plateau.animerCoup(coupARefaire, false);
                while (plateau.isAnim());

                infosPartie.getTerrain().effectuerCoup(coupARefaire, true);
                infosPartie.getHistorique().refaireCoup();
            } else
                infosPartie.changerJoueurCourant();

            updateBoutonsControle();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refaireCoupDistant() {
        final MessageRefaireCoup messageRefaireCoup = new MessageRefaireCoup();
        moteurReseau.ajouterMessageAEnvoyer(messageRefaireCoup);
    }

    public void setAvalamClient(AvalamClient avalamClient) {
        this.avalamClient = avalamClient;

    }

    /**
     * Ne pas utiliser cette méthode pour changer la couleur lors d'un nouveau coup
     * @param couleurJoueur
     */
    @Deprecated
    public void setCouleur(CouleurJoueur couleurJoueur) {
        infosPartie.setCouleurJoueurCourant(couleurJoueur, true);
    }

    public void setInfosPartie(InfosPartie infosPartie) {
        this.infosPartie = infosPartie;
    }

    public void setMoteurReseau(MoteurReseaux moteurReseau) {
        this.moteurReseau = moteurReseau;
    }

    public synchronized void setPlateau(TerrainGraphique plateau) {
        this.plateau = plateau;
    }

    /**
     * Traite la réception d'un coup depuis le serveur
     * @param coup
     * @param couleurProchainJoueur
     * @throws CasesNonAdjacentesException
     * @throws CaseSourceVideException
     * @throws AjoutSurCaseVideException
     */
    public void traiterCoupDistant(Coup coup, CouleurJoueur couleurProchainJoueur) throws CasesNonAdjacentesException,
        CaseSourceVideException, AjoutSurCaseVideException {
        /*
         * Le joueur doit afficher le coup de son adversaire
         */
        try {
            System.out.print("Moteur léger : Coup reçu depuis le serveur...");
            if (infosPartie.getCouleurJoueurClient().equals(couleurProchainJoueur)) {
                lockBoutonsControle();
                plateau.animerCoup(coup, false);
                while (plateau.isAnim());
                infosPartie.getTerrain().effectuerCoup(coup, true);
            }
            
            isCoupNouveau = true;
            
            infosPartie.setCouleurJoueurCourant(couleurProchainJoueur, false);
            if (!infosPartie.getCouleurJoueurCourant().equals(couleurProchainJoueur)) {
                System.err.println("Triche ! Un joueur a joué plus d'une fois !");
            }
        } catch (TourTropGrandeException | HorsDuTerrainException e) {

        }
    }

    public void traiterCoupErrone(Coup coupErrone) {
        try {
            System.out.print("AnnulationCoup...");
            infosPartie.getTerrain().annulerCoup(coupErrone);
            System.out.println(" OK");
        } catch (HorsDuTerrainException | CasesNonAdjacentesException | CaseSourceVideException
                 | AjoutSurCaseVideException | TourTropGrandeException e) {
        }
    }

    public void validerAnnulerCoup(MessageBroadcastAnnulationCoup message) throws CasesNonAdjacentesException,
        CaseSourceVideException, AjoutSurCaseVideException, TourTropGrandeException {
        try {
            plateau.animerCoup(message.getCoup(), true);
            while(plateau.isAnim());
            infosPartie.getTerrain().annulerCoup(message.getCoup());
            if (message.getNbAnnulationsPossibles() > 0) {
                infosPartie.getTerrain().setCoupAvantAnnule(message.getPrecedentCoup());
            }
            isCoupNouveau = false;
            
            infosPartie.changerJoueurCourant();

            if (message.getNbAnnulationsPossibles() > 0)
                updateBoutonsControle();
            else
                lockBoutonsControle();
        } catch (final MalformedLinkException e) {
            e.printStackTrace();
        } catch (final HorsDuTerrainException e) {
        }
    }

    public void validerRefaireCoupDistant(MessageBroadcastRefaireCoup message) {

        try {
            plateau.animerCoup(message.getCoup(), false);
            while(plateau.isAnim());
            infosPartie.getTerrain().effectuerCoup(message.getCoup(), true);

            if (message.getNbRefaireRestant()==0)
                isCoupNouveau = true;

            infosPartie.changerJoueurCourant();
            updateBoutonsControle();

        } catch (MalformedLinkException | TourTropGrandeException | HorsDuTerrainException
                 | CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException e) {

        }
    }

    public void conseillerCoup() {
        try {
            IA conseiller = new NiveauMoyen(infosPartie.getCouleurJoueurCourant(), infosPartie.getJoueurCourant()
                                                                                              .getTypeJoueur(), this);
            Coup conseil = conseiller.conseillerCoup();
            //System.out.println("Conseil: " + conseil.getSourceCoord() + " >> " + conseil.getDestCoord());
            if (conseil != null)
                plateau.montrerConseil(conseil);
        } catch (MauvaisChoixDeCouleurException e) {
        }

    }
}
