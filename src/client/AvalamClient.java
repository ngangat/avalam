package client;

import java.util.Observer;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import client.reseaux.MoteurReseaux;
import client.window.FenetrePrincipale;
import client.window.PanelControle;
import client.window.PanelJoueur;

import common.CouleurJoueur;
import common.InfosPartie;
import common.Joueur;
import common.Terrain;
import common.TerrainGraphique;
import common.TerrainMatrice;
import common.TypeJoueur;
import common.TypePartie;

public class AvalamClient implements Runnable {

    public static void main(String[] args) {
        // Look and Feel
        try {
            for (final LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (final Exception e) {
        }
        try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
            //UIManager.setLookAndFeel(ch.randelshofer.quaqua.QuaquaManager.getLookAndFeel());
        } catch (final Exception e) {
            e.printStackTrace();
        }

        if (args.length == 0) { // mode joueur (graphique)
            FenetrePrincipale.getInstance(); // permet d'appeler le constructeur de FenetrePrincipale (pour pouvoir
            // utiliser les notifications)

            // Démarrage du client
            final AvalamClient avc = new AvalamClient(0);

            // Démarrage du GUI avec la partie rapide
            SwingUtilities.invokeLater(avc);

        } else {
            // récupérer l'adresse du serveur et le port

            // récupérer le niveau de l'ia Ã  partir de la ligne de commande

            // initialiser l'ia et jouer

        }

    }

    private final MoteurLeger moteurLeger;
    private PanelJoueur       panelJoueurNoir;
    private PanelJoueur       panelJoueurBlanc;
    private TerrainGraphique  plateau;
    private PanelControle     panelControle;

    private EcouteurSouris    ecouteurSouris;

    /**
     * Constructeur
     * @param typeParti Type de la parti : la connexion au serveur est gÃ©rÃ© diffÃ©rement en fonction de si on est 
     * en local (une seule fenetre) ou Ã  distance (2 fenÃªtres)
     * @param portLocal Port local. 0 pour prendre le premier port de libre
     */
    public AvalamClient(int portLocal) {

        moteurLeger = new MoteurLeger();
        final MoteurReseaux moteurReseau = new MoteurReseaux();
        moteurLeger.setMoteurReseau(moteurReseau);
        moteurLeger.setAvalamClient(this);
        moteurReseau.setMoteurLeger(moteurLeger);
        ChargeurDeRessources.chargerRessources(ChargeurDeRessources.getThemeName());
        moteurLeger.nouvellePartieLocal("Joueur 1", "Joueur 2", true, TypeJoueur.Facile);

    }

    public void initEvent() {
        final InfosPartie i = moteurLeger.getInfosPartie();
        System.out.println("Suppression des observateurs d'infosPartie");
        i.deleteObservers();
        final Terrain t = i.getTerrain();
        System.out.println("Suppression des observateurs du terrain");
        ((TerrainMatrice) t).deleteObservers();

        System.out.println("Ajout du plateau");
        plateau = new TerrainGraphique(t);
        moteurLeger.setPlateau(plateau);
        ((TerrainMatrice) t).addObserver(plateau);
        i.addObserver(plateau);

        if (ecouteurSouris != null) {
            System.out.println("Suppresion des anciens listeners souris");
            plateau.removeMouseListener(ecouteurSouris);
            plateau.removeMouseMotionListener(ecouteurSouris);
        }
        System.out.println("Ajout du listener souris sur le plateau");
        ecouteurSouris = new EcouteurSouris(t, plateau, moteurLeger);
        plateau.addMouseListener(ecouteurSouris);
        plateau.addMouseMotionListener(ecouteurSouris);

        System.out.println("Ajout des joueurs en tant qu'observateur d'infosPartie");
        for(Joueur j : i.getJoueurs())
        if (j != null && !j.getTypeJoueur().equals(TypeJoueur.Humain) && i.getTypePartie().equals(TypePartie.LOCAL)) {
            i.addObserver((Observer) j);
        }

        System.out.println("Ajout de la fenetre principale en tant qu'observateur d'infosPartie");

        System.out.println("Ajout du panel joueur blanc");
        panelJoueurBlanc = new PanelJoueur(moteurLeger.getInfosPartie(), true, t);
        i.addObserver(panelJoueurBlanc);

        System.out.println("Ajout du panel joueur blanc");
        panelJoueurNoir = new PanelJoueur(moteurLeger.getInfosPartie(), false, t);
        i.addObserver(panelJoueurNoir);

        System.out.println("Ajout du panneau de contrôle");
        panelControle = new PanelControle(moteurLeger);
        
        i.setCouleurJoueurCourant(CouleurJoueur.BLANC, true);
        i.addObserver(FenetrePrincipale.getInstance());
    }

    @Override
    public void run() {
        FenetrePrincipale.getInstance().setMoteur(moteurLeger);

        setPanels();

        FenetrePrincipale.getInstance().setVisible(true);
        FenetrePrincipale.getInstance().pack();
        FenetrePrincipale.getInstance().setLocationRelativeTo(null); // doit Ãªtre aprÃ¨s le pack() pour centrer la
                                                                     // fenÃªtre
    }

    /**
     * 
     */
    public void setPanels() {
        FenetrePrincipale.getInstance().setPanelJoueurHaut(panelJoueurNoir);
        FenetrePrincipale.getInstance().setPanelJoueurBas(panelJoueurBlanc);
        FenetrePrincipale.getInstance().setPanelPlateau(plateau);
        FenetrePrincipale.getInstance().setPanelControle(panelControle);
        FenetrePrincipale.getInstance().effacerMessageGagnant();
    }
}
