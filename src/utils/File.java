package utils;

public class File<T> {
    T[] tab;
    int tete, nombre;

    @SuppressWarnings("unchecked")
    public File(int taille) {
        tab = (T[]) new Object[taille];
        tete = 0;
        nombre = 0;
    }

    public synchronized T extraire() {
        T resultat;
        while (nombre == 0) {
            try {
                wait();
            } catch (final Exception e) {
            }
        }
        resultat = tab[tete];
        tete = (tete + 1) % tab.length;
        nombre--;
        notifyAll();
        return resultat;
    }

    public synchronized void inserer(T o) {
        while (nombre == tab.length) {
            try {
                wait();
            } catch (final Exception e) {
            }
        }
        tab[(tete + nombre) % tab.length] = o;
        nombre++;
        notifyAll();
    }

    public synchronized int Taille() {
        return this.tab.length;
    }
}
