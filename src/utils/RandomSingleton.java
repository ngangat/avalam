package utils;

import java.util.Random;

public class RandomSingleton {
    private static RandomSingleton instance;

    public static RandomSingleton getInstance() {
        if (RandomSingleton.instance == null) {
            RandomSingleton.instance = new RandomSingleton();
        }
        return RandomSingleton.instance;
    }

    private final Random rnd;

    private RandomSingleton() {
        rnd = new Random();
    }

    public boolean nextBoolean() {
        return rnd.nextBoolean();
    }

    public int nextInt() {
        return rnd.nextInt();
    }

    public int nextInt(int x) {
        return rnd.nextInt(x);
    }
}
