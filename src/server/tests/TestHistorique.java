package server.tests;

import java.util.Random;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import common.Coup;
import common.Historique;
import common.Terrain;
import common.TerrainMatrice;

public class TestHistorique {
    protected Historique h;
    protected Terrain    t;
    protected Random     r;

    @Before
    public void setUp() throws Exception {
        h = new Historique();
        t = new TerrainMatrice();
        r = new Random();
    }

    @After
    public void tearDown() throws Exception {
        h = null;
        t = null;
        r = null;
    }

    @Test
    public void testAjouterNouveauCoupSimpleApresAnnuler() throws Exception {
        final Coup c1 = new Coup(3, 6, Coup.TYPE_MVT.BAS, t);
        final Coup c2 = new Coup(2, 4, Coup.TYPE_MVT.BAS_DROIT, t);
        final Coup c3 = new Coup(5, 5, Coup.TYPE_MVT.DROIT, t);
        final Coup c4 = new Coup(3, 3, Coup.TYPE_MVT.HAUT_GAUCHE, t);

        h.ajouterNouveauCoup(c1);

        Assert.assertEquals(h.getListCoupJoues().size(), 1);
        Assert.assertEquals(h.getListCoupJoues().get(0), c1);

        h.annulerCoup();

        Assert.assertEquals(h.getListCoupJoues().size(), 1);
        Assert.assertEquals(h.getListCoupJoues().get(0), c1);

        h.ajouterNouveauCoup(c2);
        h.annulerCoup();

        Assert.assertEquals(h.getListCoupJoues().size(), 1);
        Assert.assertEquals(h.getListCoupJoues().get(0), c2);

        h.ajouterNouveauCoup(c3);
        h.ajouterNouveauCoup(c4);

        Assert.assertEquals(h.getListCoupJoues().size(), 2);
        Assert.assertEquals(h.getListCoupJoues().get(0), c4);
        Assert.assertEquals(h.getListCoupJoues().get(1), c3);
    }

    @Test
    public void testAjouterNouveauCoupSimpleApresRefaire() throws Exception {

        final Coup c1 = new Coup(3, 6, Coup.TYPE_MVT.BAS, t);
        final Coup c2 = new Coup(2, 4, Coup.TYPE_MVT.BAS_DROIT, t);
        final Coup c3 = new Coup(5, 5, Coup.TYPE_MVT.DROIT, t);
        final Coup c4 = new Coup(3, 2, Coup.TYPE_MVT.HAUT_GAUCHE, t);
        final Coup c5 = new Coup(4, 2, Coup.TYPE_MVT.BAS_GAUCHE, t);
        final Coup c6 = new Coup(6, 6, Coup.TYPE_MVT.DROIT, t);

        h.ajouterNouveauCoup(c1);
        h.annulerCoup();

        h.ajouterNouveauCoup(c3);
        Assert.assertEquals(h.getListCoupJoues().size(), 1);
        Assert.assertEquals(h.getListCoupJoues().get(0), c3);

        h.ajouterNouveauCoup(c2);
        h.annulerCoup();
        h.refaireCoup();

        Assert.assertEquals(h.getListCoupJoues().size(), 2);
        Assert.assertEquals(h.getListCoupJoues().get(0), c2);
        Assert.assertEquals(h.getListCoupJoues().get(1), c3);

        h.ajouterNouveauCoup(c1);

        Assert.assertEquals(h.getListCoupJoues().size(), 3);
        Assert.assertEquals(h.getListCoupJoues().get(0), c1);
        Assert.assertEquals(h.getListCoupJoues().get(1), c2);
        Assert.assertEquals(h.getListCoupJoues().get(2), c3);

        h.ajouterNouveauCoup(c4);
        h.ajouterNouveauCoup(c5);
        h.ajouterNouveauCoup(c6);

        Assert.assertEquals(h.getListCoupJoues().size(), 6);
        Assert.assertEquals(h.getListCoupJoues().get(0), c6);
        Assert.assertEquals(h.getListCoupJoues().get(1), c5);
        Assert.assertEquals(h.getListCoupJoues().get(2), c4);
        Assert.assertEquals(h.getListCoupJoues().get(3), c1);
        Assert.assertEquals(h.getListCoupJoues().get(4), c2);
        Assert.assertEquals(h.getListCoupJoues().get(5), c3);

        h.annulerCoup();
        h.annulerCoup();
        h.annulerCoup();

        Assert.assertEquals(h.getListCoupJoues().size(), 6);
        Assert.assertEquals(h.getListCoupJoues().get(0), c6);
        Assert.assertEquals(h.getListCoupJoues().get(1), c5);
        Assert.assertEquals(h.getListCoupJoues().get(2), c4);
        Assert.assertEquals(h.getListCoupJoues().get(3), c1);
        Assert.assertEquals(h.getListCoupJoues().get(4), c2);
        Assert.assertEquals(h.getListCoupJoues().get(5), c3);

        h.refaireCoup();

        Assert.assertEquals(h.getListCoupJoues().size(), 6);
        Assert.assertEquals(h.getListCoupJoues().get(0), c6);
        Assert.assertEquals(h.getListCoupJoues().get(1), c5);
        Assert.assertEquals(h.getListCoupJoues().get(2), c4);
        Assert.assertEquals(h.getListCoupJoues().get(3), c1);
        Assert.assertEquals(h.getListCoupJoues().get(4), c2);
        Assert.assertEquals(h.getListCoupJoues().get(5), c3);

        h.refaireCoup();

        Assert.assertEquals(h.getListCoupJoues().size(), 6);
        Assert.assertEquals(h.getListCoupJoues().get(0), c6);
        Assert.assertEquals(h.getListCoupJoues().get(1), c5);
        Assert.assertEquals(h.getListCoupJoues().get(2), c4);
        Assert.assertEquals(h.getListCoupJoues().get(3), c1);
        Assert.assertEquals(h.getListCoupJoues().get(4), c2);
        Assert.assertEquals(h.getListCoupJoues().get(5), c3);

        h.ajouterNouveauCoup(c2);

        Assert.assertEquals(h.getListCoupJoues().size(), 6);
        Assert.assertEquals(h.getListCoupJoues().get(0), c2);
        Assert.assertEquals(h.getListCoupJoues().get(1), c5);
        Assert.assertEquals(h.getListCoupJoues().get(2), c4);
        Assert.assertEquals(h.getListCoupJoues().get(3), c1);
        Assert.assertEquals(h.getListCoupJoues().get(4), c2);
        Assert.assertEquals(h.getListCoupJoues().get(5), c3);
    }

    @Test(expected = Exception.class)
    public void testAnnulerCoupBloquantPlusDeCoup() throws Exception {
        try {
            final Coup c1 = new Coup(4, 1, Coup.TYPE_MVT.HAUT, t);
            final Coup c2 = new Coup(5, 3, Coup.TYPE_MVT.HAUT, t);
            h.ajouterNouveauCoup(c1);
            h.ajouterNouveauCoup(c2);
            h.annulerCoup();
            h.annulerCoup();
        } catch (final Exception e) {
            Assert.fail("Erreur inattendue !");
        }
        h.annulerCoup();
    }

    /* TESTS pour la fonction annulerCoup() */
    @Test(expected = Exception.class)
    public void testAnnulerCoupBloquantVide() throws Exception {
        h.annulerCoup();
    }

    /* TESTS : Exécution de plusieurs fonctions */

    /* 1) Ajout d'un nouveau coup après un annulerCoup */

    @Test(expected = Exception.class)
    public void testRefaireCoupBloquantPlusDeCoup() throws Exception {
        final Coup c1 = new Coup(6, 5, Coup.TYPE_MVT.GAUCHE, t);
        final Coup c2 = new Coup(8, 5, Coup.TYPE_MVT.DROIT, t);
        h.ajouterNouveauCoup(c1);
        h.ajouterNouveauCoup(c2);
        h.annulerCoup();
        h.annulerCoup();
        h.refaireCoup();
        h.refaireCoup();
        h.refaireCoup();
    }

    /* 2) Ajout d'un nouveau coup après un refaireCoup */

    /* TESTS pour la fonction refaireCoup() */
    @Test(expected = Exception.class)
    public void testRefaireCoupBloquantVide() throws Exception {
        h.refaireCoup();
    }
}
