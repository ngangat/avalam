package server;

import javax.naming.MalformedLinkException;

import server.reseaux.MoteurReseaux;

import common.CouleurJoueur;
import common.Coup;
import common.InfosPartie;
import common.Terrain;
import common.TypePartie;
import common.messages.MessageBroadcastAnnulationCoup;
import common.messages.MessageBroadcastRefaireCoup;
import common.messages.MessageConfirmationAnnulationCoup;
import common.messages.MessageCoup;
import common.messages.MessageCoupBroadcast;
import common.messages.MessageDemandeConfirmationAnnulationCoup;

import exceptions.AjoutSurCaseVideException;
import exceptions.AucunCoupAAnnulerException;
import exceptions.CaseSourceVideException;
import exceptions.CasesNonAdjacentesException;
import exceptions.ConfirmationPourUneAutreAnnulationException;
import exceptions.ConfirmationSansDemandeDAnnulationException;
import exceptions.HorsDuTerrainException;
import exceptions.MauvaisJoueurException;
import exceptions.TourTropGrandeException;

public class Moteur {
    Terrain               terrain;

    private MoteurReseaux moteurReseaux;
    private InfosPartie   infosPartie;

    /*
     * Données utiles à l'annulation de coup
     */
    private Coup          coupAAnnuler;
    private boolean       demandeAnnulation;

    public Moteur(Terrain t) {
        terrain = t;
    }

    public synchronized void confirmerAnnulation(CouleurJoueur confirmeur, MessageConfirmationAnnulationCoup message)
        throws Exception {
        if (!demandeAnnulation) {
            throw new ConfirmationSansDemandeDAnnulationException();
        }

        if (!message.getCoup().getSourceCoord().equals(getCoupAAnnuler().getSourceCoord())
            || !message.getCoup().getDestCoord().equals(getCoupAAnnuler().getDestCoord())) {
            throw new ConfirmationPourUneAutreAnnulationException();
        }

        if (message.isOkPourAnnulation()) {
            final int nbRestant = infosPartie.getHistorique().getIt().previousIndex();
            MessageBroadcastAnnulationCoup messageBroadcastAnnulationCoup;

            terrain.annulerCoup(getCoupAAnnuler());
            infosPartie.getHistorique().annulerCoup();

            if (nbRestant == 0) {
                messageBroadcastAnnulationCoup = new MessageBroadcastAnnulationCoup(getCoupAAnnuler(), nbRestant);
            } else {
                messageBroadcastAnnulationCoup = new MessageBroadcastAnnulationCoup(
                                                                                    getCoupAAnnuler(),
                                                                                    nbRestant,
                                                                                    infosPartie.getHistorique()
                                                                                               .getListCoupJoues()
                                                                                               .get(infosPartie.getHistorique()
                                                                                                               .getIt()
                                                                                                               .previousIndex()));
            }

            moteurReseaux.ajouterMessageAEnvoyer(CouleurJoueur.TOUS, messageBroadcastAnnulationCoup);
        }

        demandeAnnulation = false;
    }

    public synchronized void demanderConfirmationAnnulation(CouleurJoueur demandeur) {
        try {
            setCoupAAnnuler();
            demandeAnnulation = true;

            final MessageDemandeConfirmationAnnulationCoup message = new MessageDemandeConfirmationAnnulationCoup(
                                                                                                                  getCoupAAnnuler());

            CouleurJoueur adversaire;
            if (infosPartie.getTypePartie().equals(TypePartie.DISTANT)) {
                adversaire = demandeur.equals(CouleurJoueur.BLANC) ? CouleurJoueur.NOIR : CouleurJoueur.BLANC;
            } else {
                adversaire = demandeur;
            }

            moteurReseaux.ajouterMessageAEnvoyer(adversaire, message);
        } catch (final Exception e) {
        }
    }

    private Coup getCoupAAnnuler() {
        return coupAAnnuler;
    }

    public synchronized InfosPartie getInfosPartie() {
        return infosPartie;
    }

    public void jouerCoup(MessageCoup messageCoupRecu, CouleurJoueur joueur) throws MauvaisJoueurException,
        MalformedLinkException {
        if (!joueur.equals(infosPartie.getCouleurJoueurCourant())) {
            throw new MauvaisJoueurException();
        }
        try {
            final Coup coupJoue = new Coup(messageCoupRecu.getCoup(), terrain);
            if (infosPartie.getTypePartie().equals(TypePartie.DISTANT)) {
                System.out.println("Coup reçu de joueur " + joueur.name());
            } else {
                System.out.println("Coup reçu de joueur " + infosPartie.getCouleurJoueurCourant().name());
            }
            /*
             * Réalisation du coup, broadcast et changement joueur courant si la reconstruction du coup a réussi
             */
            terrain.effectuerCoup(coupJoue, false);
            infosPartie.getHistorique().ajouterNouveauCoup(coupJoue);
            System.out.println("Coup joué");
            infosPartie.changerJoueurCourant();

            System.out.println("Ajout msg a envoyer...");
            moteurReseaux.ajouterMessageAEnvoyer(CouleurJoueur.TOUS,
                                                 new MessageCoupBroadcast(messageCoupRecu,
                                                                          infosPartie.getCouleurJoueurCourant()));
        } catch (CasesNonAdjacentesException | CaseSourceVideException | AjoutSurCaseVideException
                 | TourTropGrandeException | HorsDuTerrainException e) {
            e.printStackTrace();
            /*
             * Notifier client que son coup est erroné : on change le code message et on le renvoie au bon joueur
             */
            messageCoupRecu.setCodeErreur();
            moteurReseaux.ajouterMessageAEnvoyer(infosPartie.getCouleurJoueurCourant(), messageCoupRecu);
        }
    }

    public synchronized void refaireCoup() {
        try {
            infosPartie.getHistorique().refaireCoup();
            final Coup coupRefait = infosPartie.getHistorique().getDernierCoup();
            moteurReseaux.ajouterMessageAEnvoyer(CouleurJoueur.TOUS,
                                                 new MessageBroadcastRefaireCoup(coupRefait,
                                                                                 infosPartie.getHistorique()
                                                                                            .getListCoupJoues().size()
                                                                                     - infosPartie.getHistorique()
                                                                                                  .getIt().nextIndex()));
        } catch (final Exception e) {
        }
    }

    private void setCoupAAnnuler() throws AucunCoupAAnnulerException {
        if (infosPartie.getHistorique().getIt().nextIndex() == 0) {
            throw new AucunCoupAAnnulerException();
        }
        coupAAnnuler = infosPartie.getHistorique().getDernierCoup();
    }

    public synchronized void setInfosPartie(InfosPartie infosPartie) {
        this.infosPartie = infosPartie;
    }

    public void setMoteurReseaux(MoteurReseaux moteurReseaux) {
        this.moteurReseaux = moteurReseaux;

    }

    public boolean verifierCoup(CouleurJoueur joueur, Coup nouveauCoup) {
        if (joueur.equals(infosPartie.getCouleurJoueurCourant())) {
            // TODO : vérifier si le coup est possible
            return true;
        } else {

            return false;
        }

    }
}
