package server.reseaux;

import java.util.ArrayList;

import server.AvalamServeur;
import utils.File;

import common.CouleurJoueur;
import common.messages.Message;

public class ListeEnvoi {
    private final ArrayList<File<Message>> listeEnvoiJoueurs;

    public ListeEnvoi() {
        this(AvalamServeur.NOMBREMAXJOUEUR);
    }

    /* Constructeurs */
    public ListeEnvoi(int nbJoueur) {
        listeEnvoiJoueurs = new ArrayList<>();
        for (int i = 0; i < nbJoueur; i++) {
            final File<Message> fm = new File<>(100);
            listeEnvoiJoueurs.add(fm);
        }

    }

    // public ListeEnvoi(ArrayList<File<Message>> listeEnvoiJoueurs) {
    // if (listeEnvoiJoueurs == null) {
    // this.listeEnvoiJoueurs = new ArrayList<>(2);
    // } else {
    // this.listeEnvoiJoueurs = listeEnvoiJoueurs;
    // }
    // }

    /* MÃ©thodes public */

    /**
     * Permet d'extraire le message le plus vieux (fifo) de la file du joueur de couleur donnÃ©e en paramÃ©tre
     * 
     * @param couleur
     *        Couleur du joueur demandant le message (ne peut Ãªtre Ã©gal Ã  CouleurJoueur.TOUS
     * @return Le message le plus vieux
     */
    public Message extraire(CouleurJoueur couleur) {
        if (couleur.equals(CouleurJoueur.TOUS)) {
            throw new IllegalArgumentException(
                                               "Impossible d'extraire plusieurs Ã©lements en mÃªme temps. lors d'une partie distante");
        } else {
            return listeEnvoiJoueurs.get(couleur.getValeur()).extraire();
        }
    }

    /**
     * Insere un message donnÃ© dans la file du joueur de couleur donnÃ©e
     * 
     * @param couleur
     *        Couleur du joueur voulant inserer l'Ã©lement
     * @param message
     *        Message du joueur
     */
    public void inserer(CouleurJoueur couleur, Message message) {
        if (couleur.equals(CouleurJoueur.TOUS)) {
            System.out.println("Ajout dans les deux files...");
            for (final File<Message> fm : listeEnvoiJoueurs) {
                fm.inserer(message);
            }
            System.out.println("Message ajoutÃ© dans les deux files");
        } else {
            listeEnvoiJoueurs.get(couleur.getValeur()).inserer(message);
        }
    }
}
