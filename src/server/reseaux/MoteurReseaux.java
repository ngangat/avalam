package server.reseaux;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.naming.MalformedLinkException;

import server.AvalamServeur;
import server.Moteur;
import common.CodeMessage;
import common.CouleurJoueur;
import common.Historique;
import common.InfosPartie;
import common.Joueur;
import common.TerrainMatrice;
import common.TypeJoueur;
import common.TypePartie;
import common.messages.Message;
import common.messages.MessageAnnonceCouleur;
import common.messages.MessageConfirmationAnnulationCoup;
import common.messages.MessageCoup;
import common.messages.MessageInfosPartie;
import common.messages.MessageNom;
import exceptions.MauvaisChoixDeCouleurException;
import exceptions.MauvaisJoueurException;

public class MoteurReseaux {
    private int              nbClientConnecte;
    private ServerSocket     serverSocket;
    private final Socket[]   sockets;
    private final ListeEnvoi listeMessagesAEnvoyer;
    private Moteur           moteur;
    private int              portServeur;

    // Constructeurs
    public MoteurReseaux() {

        listeMessagesAEnvoyer = new ListeEnvoi(AvalamServeur.NOMBREMAXJOUEUR);
        sockets = new Socket[AvalamServeur.NOMBREMAXJOUEUR];

        nbClientConnecte = 0;
        portServeur = -1;
    }

    // Méthodes privées
    /**
     * Ajoute un message à la file d'attente des messages à envoyer.
     * 
     * @param couleur
     *        Couleur du/des joueurs destinataire (BLANC, NOIR, TOUS) @see CouleurJoueur
     * @param message
     *        Message à envoyer (requÃªte)
     */
    public void ajouterMessageAEnvoyer(CouleurJoueur couleur, Message message) {
        listeMessagesAEnvoyer.inserer(couleur, message);
    }

    // Méthodes publiques

    /**
     * Création du socket serveur. La création doit se faire avant le reste de la connexion afin de pouvoir
     * gérer les cas ou le port est déjà utilisé par une autre application (entre autre)
     * @param port
     * @throws IOException
     */
    public void creerSocketServeur(int port) throws IOException {
        System.err.print("Serveur : Démarrage de la connexion...");
        serverSocket = new ServerSocket();
        serverSocket.bind(new InetSocketAddress("0.0.0.0", port));
        portServeur = port;
        System.err.println("OK");

    }

    private void demarrerThreadClient(CouleurJoueur couleur) throws IOException {
        final Thread recepteur = new Thread(new Recepteur(this, sockets[couleur.getValeur()].getInputStream(), couleur));
        final Thread emetteur = new Thread(new Emetteur(this, sockets[couleur.getValeur()].getOutputStream(), couleur));
        recepteur.start();
        emetteur.start();
    }

    /**
     * Fermeture des sockets de connexion
     * 
     * @throws IOException
     *         s'il y a un problème avec le socket au moment de la fermeture
     */
    //    public void fermerConnexion() throws IOException {
    //        for (int i = 0; i < AvalamServeur.NOMBREMAXJOUEUR; i++) {
    //            sockets[i].close(); // TODO: A modifier
    //        }
    //    }

    /**
     * Permet de lire et enlever un message de la liste à envoyer. Méthode appelé par les threads Emetteur. Chaque
     * Thread Emetteur étant résponsable que d'une couleur de joueur, il ne peut donc extraire les message se rapportant
     * à l'autre joueur.
     * 
     * @param couleur
     *        Couleur du thread Emetteur (correspond aux envois vers un joueur)
     * @return le message à envoyer au joueur de couleur donné
     */
    public Message extraireMessageAEnvoyer(CouleurJoueur couleur) {
        if (couleur.equals(CouleurJoueur.TOUS)) {
            throw new IllegalArgumentException("Impossible d'extraire le message d'un autre utilisateur");
        }
        return listeMessagesAEnvoyer.extraire(couleur);
    }

    /**
     * Ouverture de connexion vers le serveur et créations thread lecture / écriture pour chaque client. Un client est
     * identifié (cÃ´té serveur) par une couleur @see CouleurJoueur
     * 
     * @param port
     *        Port du serveur
     * @throws IOException
     *         Le socket peut renvoyer une exception en cas d'erreur lors de l'attente de connexion
     */
    public void initConnexion() throws IOException { // TODO: faire en sorte que si un joueur est temporairement
                                                     // déco, il puisse se reco (tableau de taille 3 circulaire)
        if (portServeur == -1) {
            throw new IOException("Le socket serveur doit d'abord Ãªtre créé à partir d'un port disponible");
        }

        initInfosPartie();

        while (nbClientConnecte < AvalamServeur.NOMBREMAXJOUEUR) {
            final CouleurJoueur couleur = trouverCouleurClientLibre();

            System.out.println("Serveur : MoteurReseaux : connexion avec le client numero " + nbClientConnecte + "...");
            sockets[couleur.getValeur()] = serverSocket.accept(); // bloquant : attente d'une connexion
            System.out.println("Serveur : MoteurReseaux : connecté");

            System.out.println("Serveur : MoteurReseaux : Création des threads...");
            demarrerThreadClient(couleur);
            System.out.println("Serveur : MoteurReseaux : thread du client " + couleur + " créé");

            /*
             * AUCUN ENVOI NE DOIT ETRE REALISE A CE NIVEAU
             */

            nbClientConnecte++; // A décrémenter si problème avec le socket
        }
    }

    private void initInfosPartie() {
        moteur.setInfosPartie(new InfosPartie());
        moteur.getInfosPartie().setCouleurJoueurCourant(CouleurJoueur.BLANC, false);
        moteur.getInfosPartie().setJoueurs(new Joueur[2]);
        moteur.getInfosPartie().setTypePartie(TypePartie.DISTANT);
        moteur.getInfosPartie().setHistorique(new Historique());
        moteur.getInfosPartie().setTerrain(new TerrainMatrice());

    }

    public void setMoteur(Moteur moteur) {
        this.moteur = moteur;

    }

    /**
     * Méthode principale permettant de traiter les differents message arrivant des joueurs (par le biais des classes
     * Recepteur)
     * 
     * @param couleur
     *        Couleur du joueur emmetteur du message (méthode appellé depuis les threads Recepteur)
     * @param message
     *        Message du joueur (requÃªte)
     */
    public void traiterMessage(CouleurJoueur couleur, Message message) {
        if (couleur.equals(CouleurJoueur.TOUS)) {
            throw new IllegalArgumentException("Le message ne peut provenir de plusieurs sources differentes");
        }

        /*
         * Réception d'un message de type COUP
         */
        if (message.getCodeMessage().equals(CodeMessage.COUP)) {
            traiterMessageCoup(couleur, message);
        } else if (message.getCodeMessage().equals(CodeMessage.NOMJOUEUR)) {
            traiterMessageNom(couleur, message);
        } else if (message.getCodeMessage().equals(CodeMessage.DEMANDE_ANNULATION_COUP)) {
            // Réception d'une demande d'annulation de coup
            moteur.demanderConfirmationAnnulation(couleur);
        } else if (message.getCodeMessage().equals(CodeMessage.CONFIRMATION_ANNULATION)) {
            // Réception d'une confirmation d'annulation de coup
            try {
                moteur.confirmerAnnulation(couleur, (MessageConfirmationAnnulationCoup) message);
            } catch (final Exception e) {
            }
        } else if (message.getCodeMessage().equals(CodeMessage.DEMANDE_REFAIRE_COUP)) {
            // Réception d'une demande de refaire un coup
            moteur.refaireCoup();
        }
    }

    //    public void setPortServeur(int portServeur) {
    //        this.portServeur = portServeur;
    //    }

    private void traiterMessageCoup(CouleurJoueur couleur, Message message) {
        try {
            final MessageCoup messageCoup = (MessageCoup) message;
            moteur.jouerCoup(messageCoup, couleur);
        } catch (final MauvaisJoueurException e) {
            e.printStackTrace();
        } catch (final MalformedLinkException e) {
            e.printStackTrace();
        }
    }

    private void traiterMessageNom(CouleurJoueur couleur, Message message) {
        final MessageNom messageNom = (MessageNom) message;
        boolean nomDejaUtilise = false;
        Joueur j = null;
        try {
            j = moteur.getInfosPartie().getJoueurs()[couleur.getCouleurAdversaire().getValeur()];
        } catch (final IllegalAccessException e1) {
        }
        if (j != null && j.getNom().equals(messageNom.getNomJoueur())) {
            messageNom.setCodeErreur();
            ajouterMessageAEnvoyer(couleur, messageNom); // On réenvoit le nom avec un code d'erreur
            nomDejaUtilise = true;
        }
        if (!nomDejaUtilise) {
            try {
                moteur.getInfosPartie().ajouterNouveauJoueur(new Joueur(TypeJoueur.Humain, messageNom.getNomJoueur(),
                                                                        couleur));
            } catch (final MauvaisChoixDeCouleurException e) {
            }

            try {
                System.out.println("Annonce couleur, joueur " + couleur);
                ajouterMessageAEnvoyer(couleur, new MessageAnnonceCouleur(couleur, messageNom.getNomJoueur()));
            } catch (final MalformedLinkException e) {
            }
            if (moteur.getInfosPartie().getJoueurs()[0] != null && moteur.getInfosPartie().getJoueurs()[1] != null) {
                ajouterMessageAEnvoyer(CouleurJoueur.TOUS, new MessageInfosPartie(moteur.getInfosPartie()));
            }
        }
    }

    private CouleurJoueur trouverCouleurClientLibre() throws IOException {
        CouleurJoueur couleur;
        if (sockets[CouleurJoueur.BLANC.getValeur()] == null) {
            couleur = CouleurJoueur.BLANC;
        } else if (sockets[CouleurJoueur.NOIR.getValeur()] == null) {
            couleur = CouleurJoueur.NOIR;
        } else {
            throw new IOException("tous les sockets sont pris !");
        }
        return couleur;
    }

    /**
     * Vérifie la comformité d'un message donné provenant d'un joueur donné
     * 
     * @param couleur
     *        Couleur du joueur emetteur du message
     * @param message
     *        Message envoyé par le joueur
     * @return true si le message est conforme, false sinon
     */
    public boolean verifierMessage(CouleurJoueur couleur, Message message) {
        throw new UnsupportedOperationException("pas encore implémenté");
    }

}
