package server.reseaux;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

import common.CouleurJoueur;
import common.messages.Message;

// Producteur : ajoute un message (ou pas) dans la file d'envoi
public class Recepteur implements Runnable {
    private final MoteurReseaux moteur;
    private final CouleurJoueur couleur;
    private ObjectInputStream   ois;
    private final InputStream   inputStream;
    private boolean             connexionOk;

    public Recepteur(MoteurReseaux moteurReseaux, InputStream inputStream, CouleurJoueur couleur) throws IOException {
        super();
        moteur = moteurReseaux;
        this.couleur = couleur;
        this.inputStream = inputStream;
        connexionOk = true;
    }

    private void ajouterMessageATraiter(Message message) {
        moteur.traiterMessage(couleur, message); // /!\ ne jamais mettre une autre couleur que celui du thread !
    }

    public synchronized boolean isConnexionOk() {
        return connexionOk;
    }

    private Message lireMessage() throws ClassNotFoundException, IOException {
        Message message = null;
        if (ois != null) {
            message = (Message) ois.readObject();
            System.out.println("Serveur : Un message a Ã©tÃ© lu (rÃ©fÃ©rence : " + message + ")");
        } else {
            throw new IOException("Le buffer ne peut pas Ãªtre 'null'");
        }
        return message;
    }

    @Override
    public void run() {

        System.out.println("Serveur : RÃ©cepteur : dÃ©marrage du thread");
        try {
            final BufferedInputStream bin = new BufferedInputStream(inputStream);
            ois = new ObjectInputStream(bin);

        } catch (final IOException e) {
            e.printStackTrace();
            System.out.println("Serveur : Recepteur : erreur : " + e.getMessage());
            System.out.println("Une erreur de lecture est survenue sur le thread Recepteur du client de couleur "
                + couleur.name() + "lors de la crÃ©ation du buffer d'entrÃ©e");
            setConnexionOk(false);
        }

        while (isConnexionOk()) {

            Message message = null;
            try {
                // lecture bloquante du message provenant du client
                System.out.println("Serveur : Recepteur : lecture message...");
                message = lireMessage();
                System.out.println("Serveur : Recepteur : message lu.");
                // traitement message
                if (message != null) {
                    System.out.println("Serveur : Recepteur : demande de traitement du message lu...");
                    ajouterMessageATraiter(message);
                    System.out.println("Serveur : Recepteur : demande correctement effectuÃ©");
                } else {
                    throw new IOException("Le message ne peut pas Ãªtre 'null'");
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                System.out.println("Serveur : Recepteur : erreur : " + e.getMessage());
                System.out.println("DÃ©connexion du client de couleur " + couleur.name() + ".");
                setConnexionOk(false);
            }

        }

    }

    public synchronized void setConnexionOk(boolean connexionOk) {
        this.connexionOk = connexionOk;
    }

}
