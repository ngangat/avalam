package server.reseaux;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import common.CouleurJoueur;
import common.messages.Message;
import common.messages.MessageBienvenue;

public class Emetteur implements Runnable {
    private final MoteurReseaux moteur;
    private ObjectOutput        oos;
    private final CouleurJoueur couleur;
    private final OutputStream  outputStream;
    private boolean             connexionOk;

    public Emetteur(MoteurReseaux moteur, OutputStream outputStream, CouleurJoueur couleur) throws IOException {
        super();
        this.moteur = moteur;
        this.couleur = couleur;
        this.outputStream = outputStream;
        connexionOk = true;
    }

    private void envoyer(Message message) throws IOException {

        if (message != null) {
            oos.writeObject(message);
            oos.flush();
            System.out.println("Serveur : Emetteur : message envoyÃ© : " + message);
        } else {
            throw new IOException("Le message ne peut pas Ãªtre 'null'");
        }
    }

    public synchronized boolean isConnexionOk() {
        return connexionOk;
    }

    private Message prochainMessageAEnvoyer() {
        return moteur.extraireMessageAEnvoyer(couleur);
    }

    @Override
    public void run() {
        System.out.println("Serveur : Emetteur : dÃ©marrage du thread");
        // Init du buffer d'Ã©criture
        try {
            final BufferedOutputStream bos = new BufferedOutputStream(outputStream);
            oos = new ObjectOutputStream(bos);
        } catch (final IOException e) {
            e.printStackTrace();
            System.out.println("Serveur : Emetteur : erreur : " + e.getMessage());
            System.out.println("Serveur : Une erreur est survenue sur le thread Emetteur du client de couleur "
                + couleur.name() + "lors de la crÃ©ation du buffer de sortie.");
            setConnexionOk(false);
        }

        // Envoi d'un messsage de bienvenue la premiÃ¨re fois
        final MessageBienvenue messageBienvenue = new MessageBienvenue();
        try {
            envoyer(messageBienvenue);
        } catch (final IOException e1) {
            e1.printStackTrace();
            System.out.println("Serveur : Emetteur : erreur : " + e1.getMessage());
            System.out.println("Serveur : Une erreur est survenue sur le thread Emetteur du client de couleur "
                + couleur.name() + "lors de la crÃ©ation du buffer de sortie.");
            setConnexionOk(false);
        }
        System.out.println("Serveur : Emetteur : Envoi message de bienvenue effectuÃ©");
        while (isConnexionOk()) {
            // lecture bloquante fileEnvoi
            final Message message = prochainMessageAEnvoyer();
            // transmission aux clients
            try {
                envoyer(message);
                System.out.println("Serveur : Emetteur : message envoyÃ©");
            } catch (final IOException e) {
                e.printStackTrace();
                System.out.println("Serveur : Emetteur : erreur : " + e.getMessage());
                System.out.println("Serveur : Une erreur est survenue sur le thread Emetteur du client de couleur "
                    + couleur.name() + "lors de l'envoi de message.");
                setConnexionOk(false);
            }
        }
    }

    public synchronized void setConnexionOk(boolean connexionOk) {
        this.connexionOk = connexionOk;
    }
}
