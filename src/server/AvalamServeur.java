package server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import server.reseaux.MoteurReseaux;
import common.Terrain;
import common.TerrainMatrice;

public class AvalamServeur extends Thread {

    public final static int portParDefaut   = 1980; // port par dÃ©faut si jeu Ã  distance
    public static int       NOMBREMAXJOUEUR = 2;

    private static void infosHote() {
        InetAddress iAddress = null;
        try {
            iAddress = InetAddress.getLocalHost();
        } catch (final UnknownHostException e1) {
            e1.printStackTrace();
        }
        final String hostName = iAddress.getHostName();
        final String canonicalHostName = iAddress.getCanonicalHostName();
        System.out.println("Adresse locale = " + iAddress.getHostAddress());
        System.out.println("HostName:" + hostName);
        System.out.println("Canonical Host Name:" + canonicalHostName);
    }

    public static void main(String[] args) {
        AvalamServeur.infosHote();

        int port = AvalamServeur.portParDefaut;
        final AvalamServeur avalamServeur = new AvalamServeur();
        if (args.length == 0) {
            System.out.println("Utilisation du port par défaut : " + port);
        } else if (args.length == 1) {
            port = Integer.valueOf(args[0]);
        } else {
            System.out.println("Usage :\njava -jar serveur.jar [port]");
        }
        try {
            final String nomHote = InetAddress.getLocalHost().getHostName();
            System.out.println("Le nom d'hote est : " + nomHote);
        } catch (final UnknownHostException e) {
            System.out.println("Nom d'hote inconnu !");
            e.printStackTrace();
        }

        try {
            avalamServeur.moteurReseaux.creerSocketServeur(port);
            
        } catch (final IOException e) {
            System.out.println("Impossible de créer le serveur, vérifier le port.");
            e.printStackTrace();
        }
        
        try {
            avalamServeur.moteurReseaux.initConnexion();
        } catch (IOException e) {
            System.out.println("Impossible d'initialiser la connexion");
            e.printStackTrace();
        }

    }

    private final Moteur        moteur;
    private final MoteurReseaux moteurReseaux;
    private final Terrain       terrain;

    private int                 port;

    public AvalamServeur() {
        terrain = new TerrainMatrice();
        // TODO: distinguer le traitement pour le serveur local du traitement pour les serveur distant
        moteur = new Moteur(terrain);

        moteurReseaux = new MoteurReseaux();
        moteur.setMoteurReseaux(moteurReseaux);
        moteurReseaux.setMoteur(moteur);
    }

    public synchronized int getPort() {
        return port;
    }

    public synchronized void setPort(int port) {
        this.port = port;
    }
}
