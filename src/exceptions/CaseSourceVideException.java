package exceptions;

public class CaseSourceVideException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 113589430291335754L;

    public CaseSourceVideException() {
        super("Une case vide ne peut pas être la source d'un coup");
    }
}
