package exceptions;

public class MauvaisChoixDeCouleurException extends Exception {

    private static final long serialVersionUID = 1874079680172839074L;

    public MauvaisChoixDeCouleurException(String string) {
        super(string);
    }

}
