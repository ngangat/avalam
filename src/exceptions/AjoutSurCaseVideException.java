package exceptions;

public class AjoutSurCaseVideException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -7097538796075956437L;

    public AjoutSurCaseVideException() {
        super("Interdiction d'ajouter des pions sur une case vide");
    }
}
