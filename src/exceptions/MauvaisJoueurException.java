package exceptions;

public class MauvaisJoueurException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -7349822897295412565L;

    public MauvaisJoueurException() {
        super("Un joueur a joué alors que ça n'était pas son tour");
    }
}
