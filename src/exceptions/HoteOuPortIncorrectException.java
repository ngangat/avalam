package exceptions;

public class HoteOuPortIncorrectException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 6514609625890322632L;

    public HoteOuPortIncorrectException() {
        System.out.println("L'adresse du serveur ou Numero de port est incorrect");
    }

}
