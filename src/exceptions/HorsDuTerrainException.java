package exceptions;

public class HorsDuTerrainException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -8471516211162321165L;

    public HorsDuTerrainException() {
        super("la case ne fait pas partie du terrain");
    }
}
