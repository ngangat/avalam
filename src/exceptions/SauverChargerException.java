package exceptions;

public class SauverChargerException extends Exception {

    private static final long serialVersionUID = 3570931526669705210L;

    public SauverChargerException(String message) {
        super(message);
    }
}
