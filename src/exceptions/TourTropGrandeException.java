package exceptions;

public class TourTropGrandeException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1460527917401632995L;

    public TourTropGrandeException() {
        super("La tour ne doit pas dépassé 5 pions");
    }
}
