package exceptions;

public class TerrainNonChargeException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 2903071933369510450L;

    public TerrainNonChargeException() {
        System.out.println("Le terrain n'est pas encore chargé");
    }
}
