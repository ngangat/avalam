package exceptions;

public class CasesNonAdjacentesException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 3330115434646446975L;

    public CasesNonAdjacentesException() {
        super("Les cases jouées doivent être adjacentes");
    }
}
